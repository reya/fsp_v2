module init
  use types
  use utils
  use basis_set
  use rtlib
  public :: init_runtime
  private
contains


  subroutine init_runtime()
    integer :: basis_size

    ! # Read job input, set the rt variable
    call initialize_rt()
    write (*,*) "  [INIT] Runtime library is initialized. "
    ! # Generate the N-particle basis set
    ! # First call computes size of the arrays, second call fills the basis datastructure
    call initialize_basis_set(basis_size, compute=.FALSE., print_output=.FALSE.)
    call initialize_basis_set(basis_size, compute=.TRUE., print_output=.FALSE.)
    !call write_basis_set()
    call check_basis_completeness()
    write(*,*) " "
    write (*,"(A)") "  [INIT] Basis set is initialized. "
    write (*,"(A)") "  [INIT] ============================================= "
    write (*,"(A,I8)") "  [INIT] Size of the basis set: ", basis_set_size
    write (*,"(A)") "  [INIT] ============================================= "
    write(*,*) " "
  end subroutine init_runtime
  
end module init
