module lorentzian_profile

contains

  function lorentzian(x,x0,width)
    integer, parameter :: dp=kind(1.0d00)
    real(kind=dp),intent(in) :: x, x0, width
    real(kind=dp) :: lorentzian
    real(kind=dp), parameter :: pi=atan(1.0_dp)
    !lorentzian = width/(2.0d00*pi*((x-x0)**2 + (width/2.0d00)**2))
    !lorentzian = exp(-(x-x0)**2/(width**2))
    lorentzian = 1.0d0/(((x-x0)**2 + (width/2.0d00)**2))
  end function lorentzian

  subroutine apply_lorentzian(y,xvector,xvecsize,x0,width)
    integer, parameter :: dp=kind(1.0d00)
    integer, intent(in) :: xvecsize
    real(kind=dp), dimension(xvecsize),intent(in) :: xvector
    real(kind=dp), dimension(xvecsize),intent(out) :: y
    real(kind=dp),intent(in) :: width, x0
    integer :: i

    do i=1,xvecsize
       y(i) = lorentzian(xvector(i),x0,width)
    end do
  end subroutine apply_lorentzian


  subroutine profile(profile_out, Energy, Intensity, Erange, size_E, size_Erange, width)
    integer, parameter :: dp=kind(1.0d00)
    integer,intent(in)  :: size_E, size_Erange
    real(kind=dp), dimension(size_E),intent(in)       :: Energy, Intensity
    real(kind=dp), dimension(size_Erange),intent(in)  :: Erange
    real(kind=dp),intent(in)  :: width
    real(kind=dp), dimension(size_Erange), intent(out) :: profile_out
    integer :: i,j

    profile_out(:) = 0.0d00
    do i=1,size_E
       do j=1,size_Erange
          profile_out(j) = profile_out(j) + Intensity(i)*lorentzian(Erange(j),Energy(i),width)
       end do
    end do
  end subroutine profile

end module lorentzian_profile
