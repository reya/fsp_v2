module io
  use utils
  use types
  use hdf5
  use h5lt
  
  implicit none 

  public :: readHDF5dataset_size, readHDF5int, readHDF5dataset2D, readHDF5dataset1D, readHDF5dataset2D_INT
  public :: writeHDF5dataset2D, writeHDF5dataset1D, writeHDF5dataset1D_new, writeHDF5dataset2D_new
  public ::  writeHDF5dataset1D_int, readHDF5dataset1D_INT
  
  private
  
contains
    subroutine readHDF5dataset_size(size_out,location,filename,halt,problem)
    integer, intent(out), dimension(2) :: size_out
    integer(HSIZE_T), dimension(2) :: dims
    integer(HID_T) :: file_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr
    logical, intent(out), optional :: problem
    logical, intent(in), optional :: halt

    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    call h5ltget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);
    size_out = dims
    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);
  end subroutine readHDF5dataset_size

  subroutine datasetExists(dataset_exists, location, filename)
    character(*), intent(in) :: location, filename
    logical, intent(out) :: dataset_exists
    integer(HID_T) :: file_id
    integer :: hdferr

    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    dataset_exists=.FALSE.
    call h5lexists_f(file_id, location, dataset_exists, hdferr); call chkhdferr(hdferr);
    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);
  end subroutine datasetExists

  subroutine readHDF5int(data_out,location,filename)
    integer, intent(out) :: data_out
    integer, dimension(1) :: data
    integer(HSIZE_T), dimension(2) :: dims
    integer(HID_T) :: file_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr
    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    call h5ltget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr);
    call chkhdferr(hdferr);

    call h5ltread_dataset_int_f(file_id, location, data, &
         dims, hdferr)
    data_out = data(1)
    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);

  end subroutine readHDF5int

  subroutine readHDF5dataset2D(data,size_in,location,filename)
    integer, intent(in), dimension(2) :: size_in
    real(kind=dp), dimension(size_in(1),size_in(2)), intent(out) :: data
    integer(HSIZE_T), dimension(2) :: dims
    integer(HID_T) :: file_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5LTget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);
    if ((dims(1).ne.size_in(1)).or.(dims(2).ne.size_in(2))) then
       write(*,*) "Error in readHDF5dataset: dimensions are wrong"
       write(*,*) "   dims = ", dims
       write(*,*) "   size = ", size_in
       stop
    end if
    data(:,:) = 0.0d00
    call H5LTread_dataset_double_f(file_id, location, data, dims, hdferr); call chkhdferr(hdferr);
    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);
  end subroutine readHDF5dataset2D
  
  
  subroutine readHDF5dataset2D_INT(data,size_in,location,filename)
    integer, intent(in), dimension(2) :: size_in
    integer, dimension(size_in(1),size_in(2)), intent(out) :: data
    integer(HSIZE_T), dimension(2) :: dims
    integer(HID_T) :: file_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5LTget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);
    if ((dims(1).ne.size_in(1)).or.(dims(2).ne.size_in(2))) then
       write(*,*) "Error in readHDF5dataset: dimensions are wrong"
       write(*,*) "   dims = ", dims
       write(*,*) "   size = ", size_in
       stop
    end if

    data(:,:) = 0
    call H5LTread_dataset_int_f(file_id, location, data, dims, hdferr); call chkhdferr(hdferr);

    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);
  end subroutine readHDF5dataset2D_INT

  subroutine readHDF5dataset1D_INT(data,size_in,location,filename)
    integer, intent(in) :: size_in
    integer, dimension(size_in), intent(out) :: data
    integer(HSIZE_T), dimension(1) :: dims
    integer(HID_T) :: file_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5LTget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);

    if ((size_in).ne.dims(1)) then
       write(*,*) " [HDF5] Error: expected size_in .ne. actual dataset dimensions: ", size_in, dims
       stop
    end if
    data(:) = 0
    call H5LTread_dataset_int_f(file_id, location, data, dims, hdferr); call chkhdferr(hdferr);

    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);
  end subroutine readHDF5dataset1D_INT
  
  subroutine readHDF5dataset1D(data,size_in,location,filename)
    integer, intent(in) :: size_in
    real(kind=dp), dimension(size_in), intent(out) :: data
    integer(HSIZE_T), dimension(1) :: dims
    integer(HID_T) :: file_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5FOpen_f(filename, H5F_ACC_RDONLY_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5LTget_dataset_info_f(file_id, location, dims, type_class, type_size, hdferr); call chkhdferr(hdferr);
    if (size_in.ne.dims(1)) then
       write(*,*) "Error in readHDF5dataset: dimensions are wrong"
       write(*,*) "   dims = ", dims
       write(*,*) "   size = ", size_in
       stop
    end if
    data(:) = 0.0d00
    call H5LTread_dataset_double_f(file_id, location, data, dims, hdferr); call chkhdferr(hdferr);
    call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    call h5close_f(hdferr); call chkhdferr(hdferr);
  end subroutine readHDF5dataset1D
  
  subroutine writeHDF5dataset2D(data,size_in,location,filename)
    integer, intent(in), dimension(2) :: size_in
    real(kind=dp), dimension(size_in(1),size_in(2)), intent(in) :: data
    integer(HSIZE_T), dimension(2) :: dims
    integer(HID_T) :: file_id, dset_id, dspace_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    dims = size_in
    call h5open_f(hdferr); call chkhdferr(hdferr);

    call H5Fopen_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5Screate_simple_f(2, dims, dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Dcreate_f(file_id, location, H5T_NATIVE_DOUBLE, dspace_id, dset_id, hdferr);
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dims, hdferr); call chkhdferr(hdferr);
    call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
    call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
    call H5close_f(hdferr);
  end subroutine writeHDF5dataset2D

  subroutine writeHDF5dataset1D(data,size_in,location,filename)
    integer, intent(in) :: size_in
    real(kind=dp), dimension(size_in), intent(in) :: data
    integer(HSIZE_T), dimension(1) :: dims
    integer(HID_T) :: file_id, dset_id, dspace_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    dims(1) = size_in
    call h5open_f(hdferr); call chkhdferr(hdferr);

    call H5Fopen_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5Screate_simple_f(1, dims, dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Dcreate_f(file_id, location, H5T_NATIVE_DOUBLE, dspace_id, dset_id, hdferr);
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dims, hdferr); call chkhdferr(hdferr);
    call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
    call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
    call H5close_f(hdferr);
  end subroutine writeHDF5dataset1D


    subroutine writeHDF5dataset1D_int(data,size_in,location,filename)
    integer, intent(in) :: size_in
    integer, dimension(size_in), intent(in) :: data
    integer(HSIZE_T), dimension(1) :: dims
    integer(HID_T) :: file_id, dset_id, dspace_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    dims(1) = size_in
    call h5open_f(hdferr); call chkhdferr(hdferr);

    call H5Fopen_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5Screate_simple_f(1, dims, dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Dcreate_f(file_id, location, H5T_NATIVE_INTEGER, dspace_id, dset_id, hdferr);
    CALL h5dwrite_f(dset_id, H5T_NATIVE_INTEGER, data, dims, hdferr); call chkhdferr(hdferr);
    call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
    call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
    call H5close_f(hdferr);
  end subroutine writeHDF5dataset1D_int


  subroutine writeHDF5dataset1D_new(data,size_in,location,filename)
    integer, intent(in) :: size_in
    real(kind=dp), dimension(size_in), intent(in) :: data
    integer(HSIZE_T), dimension(1) :: dims
    integer(HID_T) :: file_id, dset_id, dspace_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    dims(1) = size_in
    call h5open_f(hdferr); call chkhdferr(hdferr);

    call H5Fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5Screate_simple_f(1, dims, dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Dcreate_f(file_id, location, H5T_NATIVE_DOUBLE, dspace_id, dset_id, hdferr);
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dims, hdferr); call chkhdferr(hdferr);
    call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
    call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
    call H5close_f(hdferr);
  end subroutine writeHDF5dataset1D_new
  
  
  subroutine writeHDF5dataset2D_new(data,size_in,location,filename)
    integer, intent(in), dimension(2) :: size_in
    real(kind=dp), dimension(size_in(1),size_in(2)), intent(in) :: data
    integer(HSIZE_T), dimension(2) :: dims
    integer(HID_T) :: file_id, dset_id, dspace_id
    integer(SIZE_T) :: i, j
    integer :: type_class
    integer(SIZE_T) :: type_size
    character(*), intent(in) :: location, filename
    integer :: hdferr

    dims = size_in
    ! call h5open_f(hdferr); call chkhdferr(hdferr);
    ! call H5FCreate_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); call chkhdferr(hdferr)
    ! call H5FOpen_f(filename, H5F_ACC_RDWR_F, file_id, hdferr); 
    ! call H5LTmake_dataset_double_f(file_id, location, 2, dims, &
    !      data, hdferr); call chkhdferr(hdferr);
    ! call H5FClose_f(file_id,hdferr); call chkhdferr(hdferr);
    ! call h5close_f(hdferr); call chkhdferr(hdferr);
    call h5open_f(hdferr); call chkhdferr(hdferr);
    call H5Fcreate_f(filename, H5F_ACC_TRUNC_F, file_id, hdferr); call chkhdferr(hdferr);
    call H5Screate_simple_f(2, dims, dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Dcreate_f(file_id, location, H5T_NATIVE_DOUBLE, dspace_id, dset_id, hdferr);
    CALL h5dwrite_f(dset_id, H5T_NATIVE_DOUBLE, data, dims, hdferr); call chkhdferr(hdferr);
    call H5Dclose_f(dset_id, hdferr); call chkhdferr(hdferr);
    call H5Sclose_f(dspace_id, hdferr); call chkhdferr(hdferr);
    call H5Fclose_f(file_id, hdferr); call chkhdferr(hdferr);
    call H5close_f(hdferr);
  end subroutine writeHDF5dataset2D_new

end module io
