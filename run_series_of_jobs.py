#!/usr/bin/python
from __future__ import division
import sys
import os, shutil
import argparse
### HDF5 I/O ###
import h5py
### NumPy module ###
import numpy as np

from scipy.optimize import minimize, fsolve, root
from subprocess import call

def write_input_file(filename,modes,HRs,T,magnitude,positions,E0,Ng,Ne):
    f = h5py.File(filename, "w")
    # More or less constant parameters #
    f.create_dataset("dense_run",(1,),data=0, dtype='i')
    f.create_dataset("compute_eigenstates",(1,),data=1, dtype='i')
    f.create_dataset("compute_absorption",(1,),data=0, dtype='i')
    f.create_dataset("compute_emission",(1,),data=1, dtype='i')
    f.create_dataset("only_nearest_neighbors",(1,),data=0, dtype='i')
    f.create_dataset("full_output",(1,),data=0, dtype='i')
    f.create_dataset("number_of_eig",(1,),data=-1, dtype='i')
    f.create_dataset("Nparticle",(1,),data=Nparticle, dtype='i')
    
    # Variable inputs #
    f.create_dataset("modes",(np.shape(modes)),data=modes, dtype='f')
    f.create_dataset("HRs",(np.shape(HRs)),data=HRs, dtype='f')
    f.create_dataset("Nmers",(1,),data=np.size(positions[:,0]), dtype='i')
    f.create_dataset("Nmodes",(1,),data=np.size(modes), dtype='i')
    Nmers = np.size(positions[:,0])
    f.create_dataset("T",(np.shape(T)),data=T, dtype='f')
    
    assert np.shape(magnitude)==np.shape(positions) # Possible catch
    assert np.shape(HRs)==np.shape(modes)
    assert np.size(E0)==Nmers
    
    f.create_dataset("magnitude",(np.shape(magnitude)),data=magnitude, dtype='f')
    f.create_dataset("positions",(np.shape(positions)),data=positions, dtype='f')
    f.create_dataset("E0",(np.shape(E0)),data=E0, dtype='f')

    f.create_dataset("Ng",(np.shape(Ng)),data=Ng, dtype='f')
    f.create_dataset("Ne",(np.shape(Ne)),data=Ne, dtype='f')

    f.create_dataset("ntemp",(1,),data=np.size(T), dtype='f')
    # Closing & flushing #
    f.close()


def J_to_distance(J):
    if (J>=0):
        mi = np.array([0, 1, 0])
        mj = np.array([0, 1, 0])
    else:
        mi = np.array([0, 0, 1])
        mj = np.array([0, 0, 1])
    rij0 = 5.0
    func = lambda(rij): J-(4.0*np.pi/(np.linalg.norm(np.array([0,0,rij],dtype=np.float64))**3))*(np.dot(mi,mj)-3.0*(np.dot(mi,np.array([0,0,rij],dtype=np.float64))*np.dot(mj,np.array([0,0,rij],dtype=np.float64))/(np.linalg.norm((np.array([0,0,rij],dtype=np.float64)))**2)))*219474.63;
    sol = root(func, rij0,method='lm',tol=1.0e-15,options={'xtol':1.0e-15,'ftol':1.0e-15})
    rij = sol.x
    rij = rij*0.5291772109217 # Convert to angstroms
    return np.linalg.norm(rij)


def read_eigenstates(filename,state_idx=0):
    f = h5py.File(filename,'r')
    # pr_1p(:)    = 0.0d00;       pr_2p(:) = 0.0d00;        pr_3p(:) = 0.0d00
    # c_1p(:)     = 0.0d00;       c_2p(:)  = 0.0d00;        c_3p(:)  = 0.0d00
    # c_1p_vib(:) = 0.0d00;    c_2p_vib(:) = 0.0d00;     c_3p_vib(:) = 0.0d00
    # c_1p_el(:)  = 0.0d00;     c_2p_el(:) = 0.0d00;      c_3p_el(:) = 0.0d00
    try:
        w = f.get('/w')
        w = np.array(w,dtype=np.float64)
        c_1p = f.get('/c_1p')
        c_1p_el = f.get('/c_1p_el')
        c_1p_vib = f.get('/c_1p_vib')
        c_2p = f.get('/c_2p')
        c_2p_el = f.get('/c_2p_el')
        c_2p_vib = f.get('/c_2p_vib')
        pr_1p = f.get('/pr_1p')
        pr_2p = f.get('/pr_2p')
        ipr_total = f.get('/ipr_total')
    except:
        print " ERROR: Can't get w"
        
    # Subtract E00 and divide by omega_vib
    #Erange = (w*219474.631370515-E00)/omega_vib
    #     Erange = (w*219474.631370515-E00)/omega_vib
    c_1p = np.array(c_1p); pr_1p = np.array(pr_1p); ipr_total = np.array(ipr_total)
    if Nparticle==2:
        c_2p = np.array(c_2p); pr_2p = np.array(pr_2p)
    else:
        c_2p = np.array([0])
        pr_2p = np.array([0])
    return w, c_1p[state_idx], c_2p[state_idx], pr_1p[state_idx], pr_2p[state_idx], ipr_total[state_idx]


def read_ISUM(filename,take_iw):
    f = h5py.File(filename,'r')
    for key in f.keys():
        if "ISUM" in key:
            key_fine = False
            if (take_iw==1):
                if ("ISUM_w" in key):
                    key_fine=True
#                    pass
            else:
                if ("ISUM_w" not in key):
                    key_fine=True
                    #pass
            if (key_fine):
                try:
                    print " * Reading key: ", key
                    ISUM = f.get(key)
                    ISUM = np.array(ISUM,dtype=np.float64)
                except:
                    pass        #print " * Skipping key: ", key

    return ISUM


def gen_pos_and_dipole(J,nmers):
    positions = []
    magnitude = []
    E0 = []
    rij = J_to_distance(J)
    for i in range(nmers):
        positions.append([0, 0, i*rij])
        if (J>=0):
            magnitude.append([0, 1, 0])
        else:
            magnitude.append([0, 0, 1])
        E0.append(3)
    return np.array(positions), np.array(magnitude), np.array(E0)


def prepare_job(Jmin=-3000, Jmax=3000, Smin=0.0, Smax=1.0, nmers_i=4, nmers_f=4):
    nJ=5; nJs=10; nS=5;
    Jrange = np.concatenate([np.linspace(Jmin,-1000,nJ),np.linspace(-950,-5,nJs),[0],np.linspace(5,950,nJs),np.linspace(1000,Jmax,nJ)])#([np.linspace(Jmin,-1000,nJ), np.linspace(-900,-10,nJs), [0],np.linspace(10,900,nJs), np.linspace(1000,Jmax,nJ)])
#    print "Jrange = ", Jrange
    Srange = np.linspace(Smin,Smax,nS)
#    print "Srange = ", Srange
    nmers_range = [nmers_i+i for i in range(nmers_f-nmers_i+1)]
#    print "nmers_range = ", nmers_range
    return Jrange, Srange, nmers_range


Nparticle=2

if __name__=="__main__":
    print " Getting ready to run a job series "
   
    Jrange, Srange, nmers_range = prepare_job()
    import xarray as xr
    import pandas as pd
    parser = argparse.ArgumentParser()
    parser.add_argument('--path_to_executable',type=str,help='Executable for the fsp_v2 program',required=True)
    args = parser.parse_args()

    ### Store Iems there
    data = np.zeros([np.size(Jrange),np.size(Srange),np.size(nmers_range)])
    #Iems = xr.DataArray(data,coords=[Jrange,Srange,nmers_range],dims=['J','S','N'])
    #Iems = xr.Dataset({'data':(['J','S','N'],data)},{'J':Jrange,'S':Srange,'N':nmers_range})
    data_array = xr.DataArray(data,dims=('J','S','N'),coords={'J':Jrange,'S':Srange,'N':nmers_range}) #({'data':(['J','S','N'],data)},{'J':Jrange,'S':Srange,'N':nmers_range})
    data = xr.Dataset({'Iems':data_array.copy(),'pr_1p':data_array.copy(),'pr_2p':data_array.copy(),'c_1p':data_array.copy(), 'c_2p':data_array.copy(),'ipr_total':data_array.copy()})
    
    print " ================================================================================ "
    print " J = ", Jrange
    print " N = ", nmers_range
    print " S = ", Srange
    print " ================================================================================ "
    total_counter = 0
    total_size    = np.size(Jrange)*np.size(Srange)*np.size(nmers_range)
    for J in Jrange:
        for S in Srange:
            for Nmers in nmers_range:
                total_counter = total_counter + 1
                print " * Setting up job with J=",J," S=",S," Nmers=",Nmers, "   ::  %5.2f done "%(100*total_counter/total_size), 
                
                print "   I'm in: ", os.getcwd()
                initial_path = os.getcwd()
                working_path = str(os.getcwd())+"/current_job"
                print "   Making new dir at: ", working_path
                try:
                    os.mkdir(working_path)
                except Exception, message:
                    print " Can't mkdir: ", message
                    print " Attempting to delete and create again "
                    shutil.rmtree(working_path)
                    os.mkdir(working_path)
                    
                os.chdir(working_path)
                print "   I'm in: ", os.getcwd()
                
                pos, dipoles, E0 = gen_pos_and_dipole(J,Nmers)
                write_input_file(filename='input.h5',
                                 modes=np.array([1500]),
                                 HRs=np.array([S]),
                                 T=np.array([298]),
                                 magnitude=dipoles,
                                 positions=pos,
                                 E0=E0,
                                 Ng=np.array([7]),
                                 Ne=np.array([10]))
                logfile = open("run.log","wb")
                errfile = open("err.log","wb")
                call([args.path_to_executable,"input.h5"],stdout=logfile,stderr=errfile)
                
                isum = read_ISUM("emission.h5",take_iw=False)
                w, c_1p, c_2p, pr_1p, pr_2p, ipr_total    = read_eigenstates("eigenstates.h5")
                data['Iems'].loc[{'J':J,'S':S,'N':Nmers}]=isum
                data['c_1p'].loc[{'J':J,'S':S,'N':Nmers}]=c_1p
                data['c_2p'].loc[{'J':J,'S':S,'N':Nmers}]=c_2p
                data['pr_1p'].loc[{'J':J,'S':S,'N':Nmers}]=pr_1p
                data['pr_2p'].loc[{'J':J,'S':S,'N':Nmers}]=pr_2p
                data['ipr_total'].loc[{'J':J,'S':S,'N':Nmers}]=ipr_total
                os.chdir(initial_path)
                shutil.rmtree(working_path)
                data.to_netcdf('iems_results.nc')
                #print "ISUM = ", isum
                #print "w    = ", w
                #shutil.rmtree(working_path)
                #sys.exit()
    ###

    print " * DONE * "
    for key in data.keys():
        print data[key]
    #print Iems['data'].loc[{'N':2}]
    
