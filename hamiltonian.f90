module hamiltonian
  use types
  use utils
  use basis_set
  use rtlib
  use ieee_arithmetic
  
  implicit none

  
  type hamiltonian_type
     real(kind=dp), allocatable  :: data_od(:)
     integer, allocatable        :: idx(:), idy(:)
     real(kind=dp), allocatable  :: data_d(:)
     integer, allocatable        :: idxx(:)
     integer :: size_od, size_d
     integer :: position_od, position_d
     integer :: increment
     real(kind=dp) :: shift=0.0d0
     logical :: is_initialized=.FALSE.
     logical :: checked_allocation=.FALSE.
   contains
     procedure, pass :: add => hamiltonian_type_add_element
     procedure, pass :: set => hamiltonian_type_set_element
     procedure, pass :: initialize => hamiltonian_type_initialize_size
     procedure, pass :: matvec => matvec_hamiltonian
     procedure, pass :: get_diagonal => get_diagonal_hamiltonian
     procedure, pass :: set_shift => set_shift_hamiltonian
     procedure, pass :: diagonal_preconditioner => diagonal_preconditioner
     procedure, pass :: GS_preconditioner => apply_GS
     procedure :: finalize  => hamiltonian_type_finalize_size
  end type hamiltonian_type
  
  public :: hamiltonian_matrix_element
  public :: hamiltonian_type

  
  private
  
contains

  subroutine set_shift_hamiltonian(hamiltonian, a)
    class(hamiltonian_type), intent(inout) :: hamiltonian
    real(kind=dp),intent(in) :: a
    hamiltonian%shift = a
  end subroutine set_shift_hamiltonian
  


  subroutine get_diagonal_hamiltonian(hamiltonian, ff_d)
    class(hamiltonian_type), intent(in) :: hamiltonian
    real(kind=dp), intent(out) :: ff_d(:)
    integer :: i, p
    do i=1,basis_set_size
       ff_d(hamiltonian%idxx(i)) = hamiltonian%data_d(i)
    end do
  end subroutine get_diagonal_hamiltonian


  subroutine diagonal_preconditioner(hamiltonian, xx_d, ff_d)
    class(hamiltonian_type), intent(in) :: hamiltonian
    real(kind=dp), intent(in) :: xx_d(:)
    real(kind=dp), intent(out) :: ff_d(:)
    integer :: i
    ff_d(:) = 0.0d0
    if ((size(xx_d).ne.basis_set_size).or.(size(ff_d).ne.basis_set_size)) then
       write(*,*) "[matvec]: Error, size is wrong"
       stop
    end if
    call mkl_dcoosymv('u', basis_set_size, 1.0d00/(hamiltonian%data_d), hamiltonian%idxx, hamiltonian%idxx, hamiltonian%size_d, xx_d, ff_d)

  end subroutine diagonal_preconditioner



  
  ! (D+L)*D^-1*(D+U)
  subroutine apply_GS(hamiltonian, xx_d, ff_d)
    class(hamiltonian_type), intent(in) :: hamiltonian
    real(kind=dp), intent(in) :: xx_d(:)
    real(kind=dp), intent(out) :: ff_d(:)
    real(kind=dp), dimension(basis_set_size) :: tmp_1
    real(kind=dp), dimension(basis_set_size) :: tmp_2
    integer :: i
    ff_d(:) = 0.0d0
    tmp_1(:) = 0.0d0
    tmp_2(:) = 0.0d0
    if ((size(xx_d).ne.basis_set_size).or.(size(ff_d).ne.basis_set_size)) then
       write(*,*) "[matvec]: Error, size is wrong"
       stop
    end if

    !(D+U)*x
    call mkl_dcoogemv('N', basis_set_size, hamiltonian%data_od, hamiltonian%idx, hamiltonian%idy, hamiltonian%size_od, xx_d, tmp_1)
    call mkl_dcoosymv('u',basis_set_size, hamiltonian%data_d, hamiltonian%idxx, hamiltonian%idxx, hamiltonian%size_d, xx_d, tmp_2)
    ff_d = tmp_1 + tmp_2

    ! D^-1* [(D+U)*x]

    call mkl_dcoosymv('u', basis_set_size, 1.0d00/hamiltonian%data_d, hamiltonian%idxx, hamiltonian%idxx, hamiltonian%size_d, ff_d, tmp_1)
    
    ! (D+L)* [D^-1*(D+U)*x]
    call mkl_dcoogemv('T', basis_set_size, hamiltonian%data_od, hamiltonian%idx, hamiltonian%idy, hamiltonian%size_od, tmp_1, tmp_2)
    call mkl_dcoosymv('u',basis_set_size, hamiltonian%data_d, hamiltonian%idxx, hamiltonian%idxx, hamiltonian%size_d, tmp_1, ff_d)
    ff_d = ff_d + tmp_2
    
  end subroutine apply_GS

  
  subroutine matvec_hamiltonian(hamiltonian, xx_d, ff_d)
    class(hamiltonian_type), intent(in) :: hamiltonian
    real(kind=dp), intent(in) :: xx_d(:)
    real(kind=dp), intent(out) :: ff_d(:)
    real(kind=dp), dimension(basis_set_size) :: ff_tmp
!    real(kind=dp), dimension(hamiltonian%size) :: diag_els
    integer :: i
    ff_d(:) = 0.0d0
    ff_tmp(:) = 0.0d00
    if ((size(xx_d).ne.basis_set_size).or.(size(ff_d).ne.basis_set_size)) then
       write(*,*) "[matvec]: Error, size is wrong"
       stop
    end if
    
    ! if (hamiltonian%shift.ne.0.0d0) then
    !    diag_els(:) = 0.0d00
    !    do i=1,hamiltonian%size
    !       if (hamiltonian%idx(i)==hamiltonian%idy(i)) then
    !          diag_els(i) = hamiltonian%shift
    !       end if
    !    end do
    ! end if
    
       
    ! write(*,*) " * On input *"
    ! write(*,*) "     basis_set_size = ", basis_set_size
    ! write(*,*) "     size(hamiltonian%data) = ", size(hamiltonian%data)
    ! write(*,*) "     size(hamiltonian%idx) = ", size(hamiltonian%idx)
    ! write(*,*) "     size(hamiltonian%idy) = ", size(hamiltonian%idy)
    ! write(*,*) "     hamiltonian%size = ", hamiltonian%size
    ! write(*,*) "     size(xx_d) = ", size(xx_d)
    ! write(*,*) "     size(ff_d) = ", size(ff_d)

    call mkl_dcoosymv('u', basis_set_size, hamiltonian%data_d+hamiltonian%shift, hamiltonian%idxx, hamiltonian%idxx, hamiltonian%size_d, xx_d, ff_tmp)
    call mkl_dcoosymv('u', basis_set_size, hamiltonian%data_od, hamiltonian%idx, hamiltonian%idy, hamiltonian%size_od, xx_d, ff_d)
    ff_d = ff_tmp+ff_d
  end subroutine matvec_hamiltonian

  

  subroutine hamiltonian_type_finalize_size(hamiltonian)
    class(hamiltonian_type), intent(inout) :: hamiltonian
    real(kind=dp), allocatable, dimension(:) :: new_data
    integer, allocatable, dimension(:)       :: new_idx, new_idy
    integer :: ierr
    if (allocated(new_data)) deallocate(new_data)
    if (allocated(new_idx)) deallocate(new_idx)
    if (allocated(new_idy)) deallocate(new_idy)
    allocate(new_data(hamiltonian%position_od),stat=ierr);  call chkmerr(ierr,info="[hamiltonian]: finalization allocation")
    new_data(1:hamiltonian%position_od) = hamiltonian%data_od(1:hamiltonian%position_od)
    call move_alloc(new_data,hamiltonian%data_od)
    allocate(new_idx(hamiltonian%position_od),stat=ierr);  call chkmerr(ierr,info="[hamiltonian]: finalization idx allocation")
    new_idx(1:hamiltonian%position_od) = hamiltonian%idx(1:hamiltonian%position_od)
    call move_alloc(new_idx,hamiltonian%idx)
    allocate(new_idy(hamiltonian%position_od),stat=ierr);  call chkmerr(ierr,info="[hamiltonian]: finalization idy allocation")
    new_idy(1:hamiltonian%position_od) = hamiltonian%idy(1:hamiltonian%position_od)
    call move_alloc(new_idy,hamiltonian%idy)
    hamiltonian%size_od = hamiltonian%position_od
  end subroutine hamiltonian_type_finalize_size
     
  subroutine hamiltonian_type_initialize_size(hamiltonian,initial_size,increment)
    class(hamiltonian_type), intent(inout) :: hamiltonian
    integer, intent(in) :: initial_size, increment
    hamiltonian%increment = increment
    hamiltonian%size_od  = initial_size
    hamiltonian%size_d  = basis_set_size
    hamiltonian%position_d = 0
    hamiltonian%position_od = 0
    allocate(hamiltonian%data_d(basis_set_size))
    allocate(hamiltonian%idxx(basis_set_size))
    allocate(hamiltonian%data_od(hamiltonian%size_od))
    allocate(hamiltonian%idx(hamiltonian%size_od))
    allocate(hamiltonian%idy(hamiltonian%size_od))
    hamiltonian%is_initialized = .TRUE.
  end subroutine hamiltonian_type_initialize_size
  
  subroutine hamiltonian_type_add_element(hamiltonian,X, ix, iy)
    class(hamiltonian_type), intent(inout) :: hamiltonian
    integer, intent(in)  :: ix, iy
    real(kind=dp),intent(in) :: X
    if (hamiltonian%is_initialized) then
       if (ix==iy) then
          call hamiltonian%set(hamiltonian%position_d+1,X,ix,iy,diag=.TRUE.)
       else
          call hamiltonian%set(hamiltonian%position_od+1,X,ix,iy,diag=.FALSE.)
       end if
    else
       write(*,*) "[hamiltonian_type_add_element]: type is not initialized"
       stop
    end if
  end subroutine hamiltonian_type_add_element
  
  subroutine hamiltonian_type_set_element(hamiltonian,i,X,ix,iy,diag)
    class(hamiltonian_type), intent(inout) :: hamiltonian
    integer, intent(in) :: i, ix, iy
    logical, intent(in) :: diag
    real(kind=dp),intent(in) :: X
    real(kind=dp), dimension(:), allocatable :: new_data
    integer, dimension(:), allocatable :: new_idx, new_idy
    integer :: ierr

    if (.not.(hamiltonian%checked_allocation)) then
       if (&
            (.not.(allocated(hamiltonian%data_d)))&
            .or.&
            (.not.(allocated(hamiltonian%data_od)))&
            .or.&
            (.not.(allocated(hamiltonian%idx)))&
            .or.&
            (.not.(allocated(hamiltonian%idy))) &
            .or.&
            (.not.(allocated(hamiltonian%idxx))) &
            ) then
          write(*,*) "[hamiltonain_type_set_element]: error - not allocated"
          stop
       end if
    else
       hamiltonian%checked_allocation=.TRUE.
    end if

    if (diag) then
       if (i<=hamiltonian%size_d) then
          hamiltonian%data_d(i) = X
          hamiltonian%idxx(i)  = ix
          hamiltonian%position_d = i
       else
          ! # Should not happen: diagonal elements are allocated before, so we stop here.
          write(*,*) "[hamiltonian_type_set_element]: error - diagonal elements are out of range"
          stop
       end if
    else
       if (i<=hamiltonian%size_od) then
          hamiltonian%data_od(i) = X
          hamiltonian%idx(i)  = ix
          hamiltonian%idy(i)  = iy
          hamiltonian%position_od = i
       else
          !       write(*,*) " [hamiltonian] :: Expanding size to ",hamiltonian%size+hamiltonian%increment
          if (allocated(new_data)) deallocate(new_data)
          if (allocated(new_idx)) deallocate(new_idx)
          if (allocated(new_idy)) deallocate(new_idy)
          allocate(new_data(hamiltonian%size_od+hamiltonian%increment),stat=ierr);  call chkmerr(ierr,info="[hamiltonian]: new data allocation")
          new_data(1:hamiltonian%size_od) = hamiltonian%data_od(:)
          new_data(hamiltonian%size_od+1:hamiltonian%size_od+hamiltonian%increment) = 0.0d0
          call move_alloc(new_data,hamiltonian%data_od)
          allocate(new_idx(hamiltonian%size_od+hamiltonian%increment),stat=ierr);  call chkmerr(ierr,info="[hamiltonian]: new idx allocation")
          new_idx(1:hamiltonian%size_od) = hamiltonian%idx(:)
          new_idx(hamiltonian%size_od+1:hamiltonian%size_od+hamiltonian%increment) = 0
          call move_alloc(new_idx,hamiltonian%idx)
          allocate(new_idy(hamiltonian%size_od+hamiltonian%increment),stat=ierr);  call chkmerr(ierr,info="[hamiltonian]: new idy allocation")
          new_idy(1:hamiltonian%size_od) = hamiltonian%idy(:)
          new_idy(hamiltonian%size_od+1:hamiltonian%size_od+hamiltonian%increment) = 0
          call move_alloc(new_idy,hamiltonian%idy)
          hamiltonian%size_od = hamiltonian%size_od + hamiltonian%increment
          hamiltonian%data_od(i) = X
          hamiltonian%idx(i)  = ix
          hamiltonian%idy(i)  = iy
          hamiltonian%position_od = i
       end if
    end if
    
  end subroutine hamiltonian_type_set_element
  
  pure real(kind=dp) function hamiltonian_matrix_element(istate, jstate)
    integer, intent(in) :: istate, jstate
    integer :: iparticle, im
    integer(kind=k1) :: idxA, idxB
    integer(kind=k1) :: vibgA, vibgB
    integer(kind=k1) :: iA,iB,iC,iP,iQ,iR, iMg, iMe, iNg, iNe  ! # Used for 3p case.
    logical :: linear_EV

    hamiltonian_matrix_element = 0.0d00
    if (istate==jstate) then      ! # Diagonal elements
       ! ### Different modes for monomers? Maybe later. ###
       hamiltonian_matrix_element = rt%E00(basis(istate)%particle(1)%idx)
       do iparticle=1,basis(istate)%np   ! # Scan all N-particle states
          ! # Reorganization energy for all modes
!          hamiltonian_matrix_element = hamiltonian_matrix_element + &
!               sum(rt%modes(:)*rt%HRs(:))
          do im=1,rt%Nmodes    ! # Scan all modes
             hamiltonian_matrix_element = hamiltonian_matrix_element + &
                  rt%modes(im)*(basis(istate)%particle(iparticle)%vib(im)) 
          end do
          ! # Zero-point vibrations for other monomers = Nmers - Nparticle
          !   e.g. 2 monomers, 1-p basis => 1 zero-point correction
          !   e.g. 3 monomers, 3-p basis => 0 zero-point correction
          ! if (rt%Nmers.ne.basis(istate)%np) then
          !    hamiltonian_matrix_element = hamiltonian_matrix_element + &
          !         sum((rt%Nmers-basis(istate)%np)*rt%modes(:))
          ! end if
       end do
    else                ! # Off-diagonal elements
       if (basis(istate)%particle(1)%idx.ne.basis(jstate)%particle(1)%idx) then
          ! # Second case: excited state on different monomers,
          ! # dipole-dipole coupling.

          ! # Make sure that all other monomers are in the same vib state

          idxA = basis(istate)%particle(1)%idx
          idxB = basis(jstate)%particle(1)%idx
          ! Check for condition gC = gC' on two basis functions
          ! ### 1p-1p ###
          if ((basis(istate)%np==1).and.(basis(jstate)%np==1)) then
             hamiltonian_matrix_element = rt%couplings(idxA,idxB)
             do im=1,rt%Nmodes
                vibgA = 0_k1!basis(jstate)%particle(1)%vib(im)
                vibgB = 0_k1!basis(istate)%particle(1)%vib(im)
                hamiltonian_matrix_element = hamiltonian_matrix_element*&
                     fcOverlapIntegral(vibgA,&
                     basis(istate)%particle(1)%vib(im),rt%HRs(im))*&
                     fcOverlapIntegral(vibgB,&
                     basis(jstate)%particle(1)%vib(im),rt%HRs(im))
             end do
             ! ### 1p-2p ###
          else if ( (basis(istate)%np==1).and.(basis(jstate)%np==2) ) then
             ! # Ground state on 2nd particle basis component must correspond to the first exc. st. component
             if (basis(istate)%particle(1)%idx==basis(jstate)%particle(2)%idx) then
                hamiltonian_matrix_element = rt%couplings(idxA,idxB)
                do im=1,rt%Nmodes
                   !                   vibgA = basis(jstate)%particle(2)%vib(im)
                   !                   vibgB = 0_k1
                   hamiltonian_matrix_element = hamiltonian_matrix_element*&
                        fcOverlapIntegral(basis(jstate)%particle(2)%vib(im),&
                        basis(istate)%particle(1)%vib(im),rt%HRs(im))*&
                        fcOverlapIntegral(0_k1,&
                        basis(jstate)%particle(1)%vib(im),rt%HRs(im))
                end do
             end if
             ! ### 2p-1p ###
          else if ( (basis(istate)%np==2).and.(basis(jstate)%np==1) ) then
             ! # Ground state on 2nd particle basis component must correspond to the first exc. st. component
             if (basis(istate)%particle(2)%idx==basis(jstate)%particle(1)%idx) then
                hamiltonian_matrix_element = rt%couplings(idxA,idxB)
                do im=1,rt%Nmodes
                   !                   vibgA = basis(jstate)%particle(2)%vib(im)
                   !                   vibgB = 0_k1
                   hamiltonian_matrix_element = hamiltonian_matrix_element*&
                        fcOverlapIntegral(0_k1,&
                        basis(istate)%particle(1)%vib(im),rt%HRs(im))*&
                        fcOverlapIntegral(basis(istate)%particle(2)%vib(im),&
                        basis(jstate)%particle(1)%vib(im),rt%HRs(im))
                end do
             end if
             ! ### 2p-2p ###
          else if ( (basis(istate)%np==2).and.(basis(jstate)%np==2) ) then
             if ( (basis(istate)%particle(2)%idx==idxB).and.(basis(jstate)%particle(2)%idx==idxA) ) then
                hamiltonian_matrix_element = rt%couplings(idxA,idxB)
                do im=1,rt%Nmodes
                   vibgA = basis(jstate)%particle(2)%vib(im)
                   vibgB = basis(istate)%particle(2)%vib(im)
                   hamiltonian_matrix_element = hamiltonian_matrix_element*&
                        fcOverlapIntegral(vibgA,&
                        basis(istate)%particle(1)%vib(im),rt%HRs(im))*&
                        fcOverlapIntegral(vibgB,&
                        basis(jstate)%particle(1)%vib(im),rt%HRs(im))
                end do
             else
                hamiltonian_matrix_element = 0.0d00
             end if

             ! ### 3p cases ###
             ! ### 3p-2p ###
          else if ((basis(istate)%np==3).and.(basis(jstate)%np==2)) then
             iA = basis(istate)%particle(3)%idx
             iB = basis(istate)%particle(2)%idx
             iC = basis(istate)%particle(1)%idx

             iP = iC
             iQ = basis(jstate)%particle(2)%idx
             iR = basis(jstate)%particle(1)%idx

             ! # iA==iR, iB==iQ, iC==iP  :: Coupling terms J_AC*|gAeC><eAg0C|
             if ((iA==iR).and.(iB==iQ).and.(iC==iP)) then
                ! # Delta(M,X) for <Mg^B|Xg^B> for iB==iQ, (2) == (2)
                if (all(basis(istate)%particle(2)%vib(:)==basis(jstate)%particle(2)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = basis(istate)%particle(3)%vib(im)
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = 0_k1
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if
                ! # iA==iQ, iB==iR, iC==iP  :: Coupling terms J_AC*|gAeC><eAg0C|
             else if ((iA==iQ).and.(iB==iR).and.(iC==iP)) then
                ! # Delta(M,X) for <Mg^A|Xg^A> for iA==iQ, (3) == (2)
                if (all(basis(istate)%particle(3)%vib(:)==basis(jstate)%particle(2)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = basis(istate)%particle(2)%vib(im)
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = 0_k1
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if

                ! # iA==iR, iB==iQ, iC==iP  :: Coupling terms J_AP*|gAeP><eAgP|

             end if

             ! ### 2p-3p ###
          else if ((basis(istate)%np==2).and.(basis(jstate)%np==3)) then

             iP = basis(jstate)%particle(3)%idx
             iQ = basis(jstate)%particle(2)%idx
             iR = basis(jstate)%particle(1)%idx

             iA = iR
             iB = basis(istate)%particle(2)%idx
             iC = basis(istate)%particle(1)%idx

             ! # iA==iR, iB==iQ, iC==iP  :: Coupling terms J_AC*|gAeC><eAg0C|
             if ((iA==iR).and.(iB==iQ).and.(iC==iP)) then
                ! # Delta(M,X) for <Mg^B|Xg^B> for iB==iQ, (2) == (2)
                if (all(basis(istate)%particle(2)%vib(:)==basis(jstate)%particle(2)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = 0_k1
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = basis(jstate)%particle(3)%vib(im) 
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if
                ! # iA==iQ, iB==iR, iC==iP  :: Coupling terms J_AC*|gAeC><eAg0C|
             else if ((iA==iR).and.(iB==iP).and.(iC==iQ)) then
                ! # Delta(M,X) for <Mg^A|Xg^A> for iB==iP, (2) == (3)
                if (all(basis(istate)%particle(2)%vib(:)==basis(jstate)%particle(3)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = 0_k1
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = basis(jstate)%particle(2)%vib(im)
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if
             end if

             ! ### 3p-3p ###
          else if ((basis(istate)%np==3).and.(basis(jstate)%np==3)) then

             iA = basis(istate)%particle(3)%idx
             iB = basis(istate)%particle(2)%idx
             iC = basis(istate)%particle(1)%idx

             iP = basis(jstate)%particle(3)%idx
             iQ = basis(jstate)%particle(2)%idx
             iR = basis(jstate)%particle(1)%idx

             ! ###############################################################################################
             ! ### 6 cases where pairwise combinations between A,B,C and P,Q,R can give meaningful results ###
             ! ###############################################################################################
             !
             ! # iA==iP, iB==iQ, iC==iR  :: Diagonal only, skipping

             ! # iA==iQ, iB==iP, iC==iR  :: Diagonal only, skipping

             ! # iA==iR, iB==iP, iC==iQ  :: Coupling terms J_AC*|gAeC><eAgC|
             if ((iA==iR).and.(iB==iP).and.(iC==iQ)) then
                ! # Delta(M,X) for <Mg^B|Xg^B> for iB==iP, (2) == (3)
                if (all(basis(istate)%particle(2)%vib(:)==basis(jstate)%particle(3)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = basis(istate)%particle(3)%vib(im)
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = basis(jstate)%particle(2)%vib(im)
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if

                ! # iA==iR, iB==iQ, iC==iP  :: Coupling terms J_AP*|gAeP><eAgP|
             else if ((iA==iR).and.(iB==iQ).and.(iC==iP)) then
                ! # Delta(M,X) for <Mg^B|Xg^B> for iB==iQ, (2) == (2)
                if (all(basis(istate)%particle(2)%vib(:)==basis(jstate)%particle(2)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = basis(istate)%particle(3)%vib(im)
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = basis(jstate)%particle(3)%vib(im)
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if

                ! # iA==iP, iB==iR, iC==iQ  :: Coupling terms J_BC*|gBeC><eBgC|
             else if ((iA==iP).and.(iB==iR).and.(iC==iQ)) then
                ! # Delta(M,X) for <Ng^A|Xg^A> for iA==iP, (3) == (3)
                if (all(basis(istate)%particle(3)%vib(:)==basis(jstate)%particle(3)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = basis(istate)%particle(2)%vib(im)
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = basis(jstate)%particle(2)%vib(im)
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if

                ! # iA==iQ, iB==iR, iC==iP  :: Coupling terms J_BC*|gBeC><eBgC|
             else if ((iA==iQ).and.(iB==iR).and.(iC==iP)) then
                ! # Delta(M,X) for <Ng^A|Xg^A> for iA==iQ, (3) == (2)
                if (all(basis(istate)%particle(3)%vib(:)==basis(jstate)%particle(2)%vib(:))) then
                   hamiltonian_matrix_element = rt%couplings(iR,iC)
                   do im=1,rt%Nmodes
                      iMg = basis(istate)%particle(2)%vib(im)
                      iMe = basis(jstate)%particle(1)%vib(im)
                      iNg = basis(jstate)%particle(3)%vib(im)
                      iNe = basis(istate)%particle(1)%vib(im)
                      hamiltonian_matrix_element = hamiltonian_matrix_element*&
                           fcOverlapIntegral(iMg,iMe,rt%HRs(im))*&
                           fcOverlapIntegral(iNg,iNe,rt%HRs(im))
                   end do
                end if
             end if
          end if
       end if
    end if

    ! ### To ensure good sparsity, we put element below 1.0d-10 to be 0.0d00 ###
    if (abs(hamiltonian_matrix_element)<1.0d-10) then
       hamiltonian_matrix_element = 0.d00
    end if
  end function hamiltonian_matrix_element

  pure real(kind=dp) function positionOperatorME(M,N)
    integer(kind=k1), intent(in) :: M, N
    positionOperatorME = (1.0d00/sqrt(2.0d00))*&
         (k1delta(M+1_k1,N)*sqrt(M+1.0d0)+k1delta(M-1_k1,N)*sqrt(1.0d00*M))
  end function positionOperatorME

  
  
end module hamiltonian
