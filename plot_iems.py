#coding: utf8
###############################################
import sys
import os
import argparse
### Plotting tools ###
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import MaxNLocator

import seaborn as sns
### HDF5 I/O ###
import h5py
### NumPy module ###
import numpy as np

### External Fortran module to speed-up calculations ###
from lorentzian_profile import lorentzian_profile
###############################################


def ha_to_rcm(x):
    return x*219474.631370515

def rcm_to_ha(x):
    return x/219474.631370515

def ha_to_ev(x):
    return x*27.2113850560

def ev_to_ha(x):
    return x/27.2113850560

def ev_to_rcm(x):
    return ha_to_rcm(ev_to_ha(x))

def rcm_to_ev(x):
    return ha_to_ev(rcm_to_ha(x))

### Helper function to convert centimeters to inches ###
def cm_to_in(x):
    return x*0.393701

### Generator returning walks inside the path ###
def walk_through_files(path):
    for subdir, dirs, files in os.walk(path):
        for file in files:
            yield os.path.join(subdir, file)

def prepare_plot():
    sns.set_style("ticks",{'xtick.major.size': 5.0,
                           'xtick.direction': u'inout',
                           'ytick.direction': u'inout',
                           'zorder':1,
                           'ytick.major.size': 5.0})
    matplotlib.rcParams.update({"font.size":10})
#    ax = plt.gca()
#    plt.setp(ax.xticks, zorder=100)
    ####################################
    ### Physial size of the figure   ###
    ### Set up something meaningful  ###
    ###                              ###
    #fig = plt.figure(figsize=[cm_to_in(15),cm_to_in(8)])
    fig = plt.figure(figsize=[cm_to_in(12),cm_to_in(8)])
    ###                              ###
    ####################################

### Main plotting function: takes filename as an argument,  ###
### be it either absoprtion.h5 or emission.h5, and does all ###
### the plotting for the intensity with idx from the input  ###
def plot_from_file(filename,spectrum_index,xmin,xmax,width,width_fine,take_iw,exp_data,plot_yfine):
    f = h5py.File(filename,'r')
    
    for key in f.keys():
        if (("from" not in key) and ("to" not in key)):
            if "I" in key:
                try:
                    if (take_iw==1):
                        splitted = key.split("I_w")
                    else:
                        splitted = key.split("I")
                    idx_current=int(splitted[1])
                    if spectrum_index==idx_current:
                        print " * Reading key: ", key
                        I = f.get(key)
                        I = np.array(I,dtype=np.float64)
                        key_E = key.replace("I","E")
                        print "key_E = ", key_E
                        E = f.get(key_E)
                        E = np.array(E,dtype=np.float64)
                        E = ha_to_rcm(E)
                    #I = I[:,0]
                    #E = E[:,0]
                except:
                    pass        #print " * Skipping key: ", key
                
    
    Erange = np.linspace(np.min(E)-2000,np.max(E)+2000,10000,dtype=np.float64)
    y = np.zeros_like(Erange)

    ### Here we call the external Fortran function, which ###
    ### sums up the contributions to the abs/ems profile  ###                   
    y = lorentzian_profile.profile(energy=E,intensity=I,erange=Erange,size_e=np.size(E),size_erange=np.size(Erange),width=width)
    # Normalize profile to unity:
    y = y/np.max(y)
    
    if plot_yfine:
        y_fine = lorentzian_profile.profile(energy=E,intensity=I,erange=Erange,size_e=np.size(E),size_erange=np.size(Erange),width=width_fine)
        # Normalize profile to unity:
        y_fine = y_fine/np.max(y_fine)

    Erange = ha_to_ev(rcm_to_ha(Erange))

    ### Plot the graph ###

    Erange = (ev_to_rcm(Erange)-ev_to_rcm(E00))/(omega_vib)
    
    plt.plot(Erange,y,linewidth=1.5,label='Profile') #,label=str(int(key_splitted[1])))

    if plot_yfine:
        plt.plot(Erange,y_fine,linewidth=0.5,label='Fine details',) #,label=str(int(key_splitted[1])))

    if (exp_data is not None):
        import csv
        x_exp = []
        y_exp = []
        with open(exp_data, 'rb') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            for row in reader:
                x_exp.append(np.float64(row[0]))
                y_exp.append(np.float64(row[1]))
            x_exp = np.array(x_exp)
            y_exp = np.array(y_exp)
            y_exp = y_exp/np.max(y_exp)
            plt.plot(x_exp,y_exp,'.',label="Experiment")
                
    
    # Set limits
    xlim_min = 0.0
    for xi, yi in zip(Erange,y):
        if yi>0.01:
            xlim_min=xi - 0.5
            break
    xlim_max = np.max(Erange)+0.5
    if (xmin is not None):
        xlim_min = xmin
    if (xmax is not None):
        xlim_max = xmax
        
    plt.xticks([1.5*i for i in range(-25,26)])
    plt.xlim([xlim_min,xlim_max])
    plt.ylim([0,1.05])
    # Set labels
    #plt.xlabel(r"$(\omega-\Omega_{ge})/\omega_{vib}$")

    
JOB_NAME="TETRAMER_emission"
#########################################################
### IMPORTANT PART: PUT E00 and omega_vib as they are ###
###                 in order to get correct graphs    ###
E00 = 3; omega_vib = 1500;
###                                                   ###
#########################################################


#JOB_NAME="TRIMER_emission"

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',help="HDF5 file to read")
    parser.add_argument('--path',help="Path to walk through")
    parser.add_argument('--title',help="Plot title")
    parser.add_argument('--output',help="Output file prefix")
    legend_parser = parser.add_mutually_exclusive_group(required=False)
    legend_parser.add_argument('--legend',help="Plot legend",dest="legend",action='store_true')
    legend_parser.add_argument('--no-legend',help="Don't plot legend",dest="legend",action='store_false')
    parser.set_defaults(legend=False)
    xlabel_parser = parser.add_mutually_exclusive_group(required=False)
    xlabel_parser.add_argument('--xlabel',help="Plot xlabel",dest="xlabel",action='store_true')
    xlabel_parser.add_argument('--no-xlabel',help="Don't plot xlabel",dest="xlabel",action='store_false')
    parser.set_defaults(xlabel=False)
    ylabel_parser = parser.add_mutually_exclusive_group(required=False)
    ylabel_parser.add_argument('--ylabel',help="Plot ylabel",dest="ylabel",action='store_true')
    ylabel_parser.add_argument('--no-ylabel',help="Don't plot ylabel",dest="ylabel",action='store_false')
    parser.set_defaults(ylabel=False)

    parser.add_argument('--xmin',help="X limit (min)",type=float,default=None)
    parser.add_argument('--xmax',help="X limit (max)",type=float,default=None)
    parser.add_argument('--use_iw',help="Use frequency multiplier",type=int,default=1)
    args = parser.parse_args()
    gen = walk_through_files(path=args.path)
    for el in gen:
        if args.filename in el:
            print " * File: ", el
            output_fname_prefix = el.replace("/","_")
            prepare_plot()
            plot_from_file(filename=el,spectrum_index=args.idx,xmin=args.xmin,xmax=args.xmax,width=args.width,width_fine=args.width_fine,take_iw=args.use_iw,exp_data=args.experiment,plot_yfine=args.plot_yfine)
            plt.tight_layout()
            if args.legend:
                plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
            if args.xlabel:
                plt.xlabel(u"Energy, (E-E$_{00})/\omega_{vib}$")
            if args.ylabel:
                plt.ylabel(u"Intensity, a.u.")

            # Save a .png figure with DPI=600
            plt.savefig(output_fname_prefix+args.output+".png",format="png",dpi=600)
            # Save an .eps figure
            plt.savefig(output_fname_prefix+args.output+".eps",format="eps")

    #plot_stuff(filename="emission_TRIMER.h5")
    #########################
    ### Do you want grid? ###
    ### Uncomment this:   ###
    plt.grid()
    ###                   ###
    #########################

    ############################
    ### Write down the title ###
    ###                      ###
    #plt.title("Trimer G7E7. "+r"$J=2000$ cm$^{-1}$, $\omega_{vib}=1500$ cm$^{-1}$, $FWHM=250$ cm$^{-1}$ ", y=1.02,size=10)
    #plt.title(args.title, y=1.02,size=10)
    ###                      ###
    ############################

    # Set up legend
    # plt.tight_layout()
    # plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
    # # Save a .png figure with DPI=600
    # plt.savefig(args.output+".png",format="png",dpi=600)
    # # Save an .eps figure
    # plt.savefig(args.output+".eps",format="eps")
    # plt.savefig("output.png",format="png",dpi=300)
    
        
    
    
