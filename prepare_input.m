clear all
close all
%% In this file we're ready to prepare the input for the program.
%  You have to specify several input parameters described below. 

%% Runtype: use dense or sparse solver for the eigenproblem
%  Since I broke the dense solver, you may as well leave this parameter as
%  it is
dense_run=0; % 1 for dense run, 0 for matrix-free run
compute_eigenstates = 1; % 1 to compute eigenstates, else 0
compute_absorption  = 1; % 1 to compute absorption, else 0
compute_emission    = 1; % 1 to compute emission, else 0
only_nearest_neighbors = 0; % IMPORTANT! USE CAREFULLY!
full_output         = 0;  % Save dipole moments? Output file size will be huge.
%% Number of eigenvalues and eigenvectors to compute
%  Pick up the number of excitonic states to find. Since generally we're
%  not interested in the highest-lying (in energy) excitonic states, we
%  should pick up some fixed number of lowest states. 
number_of_eig=-1; % Number of eigenvalues for the sparse solver
%% N-particle basis set
%  Npaticle = 1 is the one-particle basis set

%  Npaticle = 2 is the two-particle basis set, etc.
Nparticle = 1; % For now, only up to 2-p basis. 3-p will be later.

%% Mode frequencies and HR (Hmuang-Rhys) factors
%  In this two vectors you should put pairs of mode frequencies, and
%  corresponding HR factor. If you have two modes, you should write it in
%  this way:
%  modes = [freq1 freq2]; Frequencies of the mode1 and mode2, in cm^-1
%  HRs   = [HR1   HR2];  HR1 and HR2 correspond to mode1 and mode2
%                        respectively
%modes=[1411.47 ]; % Mode frequencies in  * * cm^-1 * *
modes=[1500 ]; % Mode frequencies in  * * cm^-1 * *
HRs=[0.0 ] % HRs, dimensionless
%modes=[1500 ] ; % Mode frequencies in  * * cm^-1 * *
%HRs=[0.05   ] ;  % HRs, dimensionless

%% This is just a rotation in (x,y) plane matrix. It may be used to rotate dipole moments below
%  You can multiply a 3D-vector to rotate in by angle.
rotxy = @(angle) [cosd(angle) -sind(angle) 0; sind(angle) cosd(angle) 0; 0 0 1];

%% Transition dipole moment for every monomer in * * atomic units * *
%  Specify the magnitudes of every monomer in 3D space
magnitude = [1 0 0; 
             (rotxy(120)*[1 0 0]')'; 
             (rotxy(-120)*[1 0 0]')';]     
%             0 1 0;]
             %0 1 0;   ] ;% direction of the transition dipole moment

%% Positions of the monomers in * * agnstroems * *

do_Jmanual=1;
if (do_Jmanual)
%    J = 219474.631370515*0.065/27.211%141.14;
    J = modes(1)
    % specify dipole moment vectors
    mi = magnitude(1,:)';
    mj = magnitude(2,:)';
    rij0 = [0 0 0]' + [0 0 5]'; % Initial guess for rij
    J_obj_func = @(rij) J-(4*pi/(norm(rij)^3))*(dot(mi,mj)-3.0d00*(dot(mi,rij)*dot(mj,rij)/(norm(rij)^2.0d00)))*219474.63;
    options = optimset('TolFun',1e-10,'TolX',1e-10);
    rij = fsolve(J_obj_func,rij0,options);
    rij = rij/1.88973;
%    positions = [0 0 0; rij']/1.88973 % convert back to angstroem
end

dR=norm(rij)
%dR = 8
positions = [0 0 0;
             dR 0 0;
             (rotxy(60)*[dR 0 0]')';]

%             0 0 15;]

smagn = size(magnitude)
%% SWITCH BETWEEN DIMER AND POLYMER CASE
%

%% 0-0 transition energies for the monomers in * * eV * *

%E0 = [3 3 3 3 3 3 3 3 3 ];
E0 = [3 3 3]
%% Temperatures to study absorption and emission, * * K * * 
% Specify temperature in increasing order!
T = linspace(1,400,100);

%% Basis set: assumed to be the same for all monomers
% Ng = 1 or Ne = 1 correspond to absence of vibrational excitations in the basis set
% It means that only zero-point vibrational wavefunction is considered
Ng = [7 ]; % Number of vibrational excitations in ground state
Ne = [7 ] ; % Number of vibrational excitations in excited state

%
%
%
%
% 
%% ========================================================================
%% Output block: DO NOT EDIT
nmers = size(positions);
Nmers = nmers(1);
ntemp = max(size(T));
Nmodes = max(size(modes));
if (size(HRs)~=size(modes))
    error('Number of modes is not equal to number of HRs')
elseif (max(size(E0))~=nmers)
    error('Number of monomers is not equal to number of 0-0 transition energies')
elseif (any(size(magnitude)~=size(positions)))
    error('Number of monomers is not equal to number of positions')
end

% Saving input into hdf5 format
delete('input.h5')

h5create('input.h5', '/dense_run', size(dense_run))
h5write('input.h5', '/dense_run', dense_run)

h5create('input.h5', '/number_of_eig', size(number_of_eig))
h5write('input.h5', '/number_of_eig', number_of_eig)

h5create('input.h5', '/full_output', size(full_output))
h5write('input.h5', '/full_output', full_output)


h5create('input.h5', '/Nmers', size(Nmers))
h5write('input.h5', '/Nmers', Nmers)

h5create('input.h5', '/Nparticle', size(Nparticle))
h5write('input.h5', '/Nparticle', Nparticle)

h5create('input.h5', '/positions', size(positions'))
h5write('input.h5', '/positions', positions')

h5create('input.h5', '/magnitude', size(magnitude'))
h5write('input.h5', '/magnitude', magnitude')

h5create('input.h5', '/modes', size(modes))
h5write('input.h5', '/modes', modes)

h5create('input.h5', '/HRs', size(HRs))
h5write('input.h5', '/HRs', HRs)

h5create('input.h5', '/Ng', size(Ng))
h5write('input.h5', '/Ng', Ng)

h5create('input.h5', '/Ne', size(Ne))
h5write('input.h5', '/Ne', Ne)

h5create('input.h5', '/Nmodes', size(Nmodes))
h5write('input.h5', '/Nmodes', Nmodes)

h5create('input.h5', '/E0', size(E0))
h5write('input.h5', '/E0', E0)

h5create('input.h5', '/ntemp', size(ntemp))
h5write('input.h5', '/ntemp', ntemp)

h5create('input.h5', '/compute_eigenstates', size(compute_eigenstates))
h5write('input.h5', '/compute_eigenstates', compute_eigenstates)

h5create('input.h5', '/compute_absorption', size(compute_absorption))
h5write('input.h5', '/compute_absorption', compute_absorption)

h5create('input.h5', '/compute_emission', size(compute_emission))
h5write('input.h5', '/compute_emission', compute_emission)

h5create('input.h5', '/only_nearest_neighbors', size(only_nearest_neighbors))
h5write('input.h5', '/only_nearest_neighbors', only_nearest_neighbors)

h5create('input.h5', '/T', size(T))
h5write('input.h5', '/T', T)

dte = clock;
dte = dte(4:end);
fprintf('Input file written at %.0f:%.0f:%.0f', dte)
fprintf('\n')


