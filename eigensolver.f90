module eigensolver
  use types
  use rtlib
  use basis_set
  use utils
  use hamiltonian
  use eigensolver_slepc
  use mpi_f08
  use io
  use ieee_arithmetic
  implicit none

  real(kind=dp), dimension(:), allocatable    :: eigenvalues
  real(kind=dp), dimension(:,:), allocatable  :: eigenvectors
  integer, parameter :: maximum_hamiltonian_size = 2000


  public :: compute_eigenstates, read_eigenstates
  public :: eigenvalues, eigenvectors
  private

contains

  subroutine compute_eigenstates()
    integer :: ierr, i, j
    real(kind=dp) :: matrix_element
    real(kind=dp), dimension(:), allocatable :: ipr_total, pr_1p,&
         & pr_2p, pr_3p, pr_1p_vib, pr_1p_vib_nu1, &
         pr_1p_el, pr_2p_vib, pr_2p_el, c_1p, c_2p, c_3p, c_1p_vib, c_2p_vib, c_3p_vib, c_1p_el, c_2p_el, c_3p_el
    real(kind=dp), dimension(:,:), allocatable :: hamiltonian
    real(kind=dp), dimension(basis_set_size) :: tmp_in, tmp_out

    character*1 :: bmat
    character*2 :: which
    real(kind=dp) :: sigma, tol=1.0d-15, h2, snrm2
    integer       :: maxn, maxnev, maxncv, ldv
    integer       :: ido, n, nev, ncv, lworkl, info, nconv, maxitr, ishfts, mode
    real(kind=dp), dimension(:,:), allocatable :: v, d
    real(kind=dp), dimension(:), allocatable :: workl, workd, resid, ad, adl, adu, adu2, ax
    logical, allocatable, dimension(:)   :: select_ncv
    logical :: rvec
    integer, dimension(11) :: iparam, ipntr
    integer, allocatable, dimension(:)   :: ipiv

    if ((rt%number_of_eig<0).or.(rt%number_of_eig>basis_set_size)) then
       call set_number_of_eig(basis_set_size)
    end if

    write(*,*) "  [EIGENSOLVER]  Getting ready to compute eigenstates: using SLEPc "
    call prepare_eigensolver(&
         number_of_eig_in=rt%number_of_eig, &        ! Number of eigenvalues to compute
         local_size_in=rt%number_of_eig, &              ! Local portion of eigenpairs stored on the MPI process
         problem_size_in=basis_set_size, &          ! Matrix dimensions (problem_size, problem_size)
         local_neig_start_in=1, &  ! Starting index for local portion of eigenpairs, inclusive
         local_neig_stop_in=rt%number_of_eig,&     ! Stopping index for local portion of eigenpairs, inclusive
         print_output_in=.TRUE. &                ! Print eigenvalues when they are computed?
         )

    if (basis_set_size<maximum_hamiltonian_size) then
       allocate(hamiltonian(basis_set_size,basis_set_size),stat=ierr); call chkmerr(ierr,info="eig:hamiltonian")
       hamiltonian(:,:) = 0.0d00
    end if

    write(*,*) "  [EIGENSOLVER]  Setting matrix elements... "
    do i=1,basis_set_size
       do j=i,basis_set_size
          matrix_element = hamiltonian_matrix_element(i,j)
          if (abs(matrix_element)>1.0d-10) then ! # Note: elements smaller then 1.0d-10 are neglected.
             if (basis_set_size<maximum_hamiltonian_size) then
                hamiltonian(i,j) = matrix_element
                if (i.ne.j) then
                   if (basis_set_size<maximum_hamiltonian_size) then
                      hamiltonian(j,i) = matrix_element
                   end if
                end if
             end if
             call set_matrix_element(i,j,matrix_element)
             if ((i.ne.j).and.(.not.(rt%implicit_solver))) then
                call set_matrix_element(j,i,matrix_element)
             end if
          end if
       end do
    end do

    ! ! ! ### Testing.
    ! tmp_in(:) = 1.0d00
    ! call Hmatrix%matvec(tmp_in,tmp_out)
    ! write(*,*) " In: ", tmp_in
    ! write(*,*) " Out: ", tmp_out
    ! stop

    write(*,*) "  [EIGENSOLVER]  Computing eigenstates... "
    allocate(eigenvalues(rt%number_of_eig),stat=ierr); call chkmerr(ierr,info="eig:eigenvalues")
    allocate(eigenvectors(basis_set_size, rt%number_of_eig),stat=ierr); call chkmerr(ierr,info="eig:eigenvectors")
    call compute_eigenpairs(eigenvalues=eigenvalues, eigenvectors=eigenvectors, status=ierr)
    if (ierr==0) then
       write(*,*)  "  [EIGENSOLVER]  Eigenstates are computed. "
    else
       write(*,*)  "  [EIGENSOLVER]  ERROR: Failed to compute eigenstates, ierr = ", ierr
       stop
    end if
    call finalize_eigensolver()


    do j=1,rt%number_of_eig
       if ( .not.(ieee_is_normal(eigenvalues(j))) ) then
          write(*,*) " [EIGENSOLVER]  ERROR: Something is wrong with eigenvalue ",j
          write(*,*) " [EIGENSOLVER]         Value = ", eigenvalues(j)
          stop
       end if
       do i=1,basis_set_size
          if ( .not.(ieee_is_normal(eigenvectors(i,j))) ) then
             if (abs(eigenvectors(i,j)<1.0d-20)&
                  .and.&
                  ( (ieee_class(eigenvectors(i,j)) == ieee_positive_denormal)&
                  .or.&
                  (ieee_class(eigenvectors(i,j)) == ieee_negative_denormal)&
                  ) ) &
                  then
                eigenvectors(i,j) = 0.0d00
             else
                write(*,*) " [EIGENSOLVER]  ERROR: Something is wrong with eigenvector ",j," at point ",i
                write(*,*) " [EIGENSOLVER]         Value = ",eigenvectors(i,j)
                stop
             end if

          end if
       end do
    end do

    call writeHDF5dataset2D_new(data=eigenvectors,size_in=[basis_set_size,rt%number_of_eig],location="/U",filename="eigenstates.h5")
    if (basis_set_size<maximum_hamiltonian_size) then
       call writeHDF5dataset2D(data=hamiltonian,size_in=[basis_set_size,basis_set_size],location="/H",filename="eigenstates.h5")
    end if

    call writeHDF5dataset1D(data=eigenvalues,size_in=rt%number_of_eig,location="/w",filename="eigenstates.h5")

    ! ### Compute inverse participation ratios for the eigenstates ###
    allocate(ipr_total(rt%number_of_eig),stat=ierr);  call chkmerr(ierr,info="eig:ipr_total")
    ipr_total(:) = 0.0d00
    do j=1,rt%number_of_eig
       ipr_total(j) = 1.0d0/sum(eigenvectors(:,j)**4)
    end do
    call writeHDF5dataset1D(data=ipr_total,size_in=rt%number_of_eig,location="/ipr_total",filename="eigenstates.h5")


    allocate(pr_1p(rt%number_of_eig),stat=ierr);     call chkmerr(ierr,info="eig:pr_1p")
    allocate(pr_2p(rt%number_of_eig),stat=ierr);     call chkmerr(ierr,info="eig:pr_2p")
    allocate(pr_1p_vib(rt%number_of_eig),stat=ierr);
    call chkmerr(ierr,info="eig:pr_1p_vib")
    allocate(pr_1p_vib_nu1(rt%number_of_eig),stat=ierr);
    call chkmerr(ierr,info="eig:pr_1p_vib_nu1")
    allocate(pr_2p_vib(rt%number_of_eig),stat=ierr);
    call chkmerr(ierr,info="eig:pr_2p_vib")
    allocate(pr_1p_el(rt%number_of_eig),stat=ierr);
    call chkmerr(ierr,info="eig:pr_1p_el")
    allocate(pr_2p_el(rt%number_of_eig),stat=ierr);
    call chkmerr(ierr,info="eig:pr_2p_el")
    allocate(pr_3p(rt%number_of_eig),stat=ierr);     call chkmerr(ierr,info="eig:pr_3p")
    allocate(c_1p(rt%number_of_eig),stat=ierr);      call chkmerr(ierr,info="eig:c_1p")
    allocate(c_2p(rt%number_of_eig),stat=ierr);      call chkmerr(ierr,info="eig:c_2p")
    allocate(c_3p(rt%number_of_eig),stat=ierr);      call chkmerr(ierr,info="eig:c_3p")
    allocate(c_1p_vib(rt%number_of_eig),stat=ierr);  call chkmerr(ierr,info="eig:c_1p_vib") 
    allocate(c_2p_vib(rt%number_of_eig),stat=ierr);  call chkmerr(ierr,info="eig:c_2p_vib") 
    allocate(c_3p_vib(rt%number_of_eig),stat=ierr);  call chkmerr(ierr,info="eig:c_3p_vib") 
    allocate(c_1p_el(rt%number_of_eig),stat=ierr);   call chkmerr(ierr,info="eig:c_1p_el") 
    allocate(c_2p_el(rt%number_of_eig),stat=ierr);   call chkmerr(ierr,info="eig:c_2p_el") 
    allocate(c_3p_el(rt%number_of_eig),stat=ierr);   call chkmerr(ierr,info="eig:c_3p_el")

    ! ### Participation ratio = 1/IPR computed over 1-p, 2-p and 3-p subspaces.
    pr_1p(:)    = 0.0d00;       pr_2p(:) = 0.0d00;        pr_3p(:) = 0.0d00
    pr_1p_el(:) = 0.0d00;   pr_1p_vib(:) = 0.0d00;
    pr_1p_vib_nu1(:) = 0.0d00;
    pr_2p_el(:) = 0.0d00;   pr_2p_vib(:) = 0.0d00;      
    c_1p(:)     = 0.0d00;       c_2p(:)  = 0.0d00;        c_3p(:)  = 0.0d00
    c_1p_vib(:) = 0.0d00;    c_2p_vib(:) = 0.0d00;     c_3p_vib(:) = 0.0d00
    c_1p_el(:)  = 0.0d00;     c_2p_el(:) = 0.0d00;      c_3p_el(:) = 0.0d00
    
    do i=1,basis_set_size
       select case (basis(i)%np)
       case (1)
          pr_1p(:) = pr_1p(:) + eigenvectors(i,:)**4
          c_1p(:)  = c_1p(:)  + eigenvectors(i,:)**2
          if (any(basis(i)%particle(1)%vib(:)>0)) then
             c_1p_vib(:)  = c_1p_vib(:) + eigenvectors(i,:)**2
             pr_1p_vib(:) = pr_1p_vib(:) + eigenvectors(i,:)**4
             if (any(basis(i)%particle(1)%vib(:)==1)) then
                pr_1p_vib_nu1(:) = pr_1p_vib_nu1(:) + eigenvectors(i&
                     &,:)**4
             end if
             
          else
             c_1p_el(:)  = c_1p_el(:) + eigenvectors(i,:)**2
             pr_1p_el(:) = pr_1p_el(:) + eigenvectors(i,:)**4
          end if
       case (2)
          pr_2p(:) = pr_2p(:) + eigenvectors(i,:)**4
          c_2p(:)  = c_2p(:)  + eigenvectors(i,:)**2
          if (any(basis(i)%particle(1)%vib(:)>0)) then
             c_2p_vib(:)  = c_2p_vib(:) + eigenvectors(i,:)**2
             pr_2p_vib(:) = pr_2p_vib(:) + eigenvectors(i,:)**4
          else
             c_2p_el(:)  = c_2p_el(:) + eigenvectors(i,:)**2
             pr_2p_el(:) = pr_2p_el(:) + eigenvectors(i,:)**4
          end if
       case (3)
          pr_3p(:) = pr_3p(:) + eigenvectors(i,:)**4
          c_3p(:)  = c_3p(:)  + eigenvectors(i,:)**2
          if (any(basis(i)%particle(1)%vib(:)>0)) then
             c_3p_vib(:)  = c_3p_vib(:) + eigenvectors(i,:)**2
          else
             c_3p_el(:)  = c_3p_el(:) + eigenvectors(i,:)**2
          end if
       end select
    end do


    select case (rt%nparticle)
    case (1)
       call writeHDF5dataset1D(data=pr_1p,size_in=rt%number_of_eig,location="/pr_1p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p,size_in=rt%number_of_eig,location="/c_1p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p_el,size_in=rt%number_of_eig,location="/c_1p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p_vib,size_in=rt%number_of_eig,location="/c_1p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_el,size_in=rt%number_of_eig,location="/pr_1p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_vib,size_in=rt%number_of_eig,location="/pr_1p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_vib_nu1,size_in=rt%number_of_eig,location="/pr_1p_vib_nu1",filename="eigenstates.h5")
    case (2)
       call writeHDF5dataset1D(data=pr_1p,size_in=rt%number_of_eig,location="/pr_1p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p,size_in=rt%number_of_eig,location="/c_1p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p_el,size_in=rt%number_of_eig,location="/c_1p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p_vib,size_in=rt%number_of_eig,location="/c_1p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_el,size_in=rt%number_of_eig,location="/pr_1p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_vib,size_in=rt%number_of_eig,location="/pr_1p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_vib_nu1,size_in=rt%number_of_eig,location="/pr_1p_vib_nu1",filename="eigenstates.h5")       

       call writeHDF5dataset1D(data=pr_2p,size_in=rt%number_of_eig,location="/pr_2p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_2p,size_in=rt%number_of_eig,location="/c_2p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_2p_el,size_in=rt%number_of_eig,location="/c_2p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_2p_vib,size_in=rt%number_of_eig,location="/c_2p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_2p_el,size_in=rt%number_of_eig,location="/pr_2p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_2p_vib,size_in=rt%number_of_eig,location="/pr_2p_vib",filename="eigenstates.h5")

    case (3)
       call writeHDF5dataset1D(data=pr_1p,size_in=rt%number_of_eig,location="/pr_1p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p,size_in=rt%number_of_eig,location="/c_1p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p_el,size_in=rt%number_of_eig,location="/c_1p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_1p_vib,size_in=rt%number_of_eig,location="/c_1p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_el,size_in=rt%number_of_eig,location="/pr_1p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_vib,size_in=rt%number_of_eig,location="/pr_1p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_1p_vib_nu1,size_in=rt%number_of_eig,location="/pr_1p_vib_nu1",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_2p,size_in=rt%number_of_eig,location="/pr_2p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_2p,size_in=rt%number_of_eig,location="/c_2p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_2p_el,size_in=rt%number_of_eig,location="/c_2p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_2p_vib,size_in=rt%number_of_eig,location="/c_2p_vib",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_2p_el,size_in=rt%number_of_eig,location="/pr_2p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=pr_2p_vib,size_in=rt%number_of_eig,location="/pr_2p_vib",filename="eigenstates.h5")
              
       call writeHDF5dataset1D(data=pr_3p,size_in=rt%number_of_eig,location="/pr_3p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_3p,size_in=rt%number_of_eig,location="/c_3p",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_3p_el,size_in=rt%number_of_eig,location="/c_3p_el",filename="eigenstates.h5")
       call writeHDF5dataset1D(data=c_3p_vib,size_in=rt%number_of_eig,location="/c_3p_vib",filename="eigenstates.h5")
    end select
    
    
  end subroutine compute_eigenstates

  subroutine read_eigenstates()
    write(*,*)  "  [EIGENSOLVER]  ERROR: Read eigenstates is not implemented yet "
    stop
  end subroutine read_eigenstates


end module eigensolver
