program fsp
  use types
  use utils
  use basis_set
  use init
  use rtlib
  use absems
  use eigensolver
  use mpi_f08
  use testing
  implicit none
  integer :: ierr

  write(*,*) " [MAIN] STARTING THE PROGRAM "

  ! ### Prelimary tests: add more later. ###
  call fc_test()
  !call dat_type_test()
  !  call hamiltonian_type_test()

  ! ### MPI Initialization (for PETSc/SLEPc)
  call MPI_INIT(ierr)
  
  ! # First, read the input, initialize the runtime lib and generate the basis set
  call init_runtime()

  ! # Second, find out what job type is.
  ! # Possible job types:
  !      - Compute eigenstates and save them
  !      - Compute absorption, given the eigenstates 
  !      - Compute emission, given the eigenstates
  if (rt%compute_eigenstates) then
     write(*,*) " [MAIN] Computing eigenstates... "
     call compute_eigenstates()
  else
     write(*,*) " [MAIN] Reading eigenstates from ... "
     call read_eigenstates()
  end if

  if ((rt%compute_absorption).or.(rt%compute_emission)) then
     write(*,*) " [MAIN] Preparing ground state basis... "
     call prepare_gs_basis()
     write(*,*) " [MAIN] Done. "
  end if
  
  if (rt%compute_absorption) then
     write(*,*) " [MAIN] Computing absorption... "
     call compute_absorption()
  end if
  
  
  if (rt%compute_emission) then
     write(*,*) " [MAIN] Computing emission... "
     call compute_emission()
  end if
  
  call mkl_free_buffers()
  call MPI_FINALIZE(ierr)
  write(*,*) " [MAIN] DONE "
  
end program
