module testing
  use types
  use utils
  use hamiltonian
  implicit none

  public :: fc_test
  public :: hamiltonian_type_test
  
contains

  subroutine dat_type_test()
    type(dynamic_array_type) :: A
    integer :: i, j, k
    real(kind=dp) :: x
    real(kind=dp) :: rmse

    call A%initialize(initial_size=100,increment=300)
    do i=1,1000
       do j=1,30
          x = i*1.0d00+j*0.1d00
          call A%add(x,i,j)
       end do
    end do
    call A%finalize()
    rmse = 0.0d0
    do i=1,A%size
       rmse = rmse + abs(A%data(i)-(A%idx(i)+0.1d00*A%idy(i)))
    end do
    if (rmse>0.0d00) then
       write(*,*) " [testing] Failed for dynamic_array_type (rmse>0.0). Check the sources. rmse = ", rmse
    end if
    
    
  end subroutine dat_type_test
  
  

  subroutine hamiltonian_type_test()
    type(hamiltonian_type) :: H
    integer :: i, j, k
    real(kind=dp) :: cpu_tstart, cpu_tstop
    real(kind=dp), allocatable, dimension(:) :: x, f
    real :: rmse
    ! call cpu_time(cpu_tstart)
    ! call H%initialize(initial_size=3,increment=2)
    ! do i=1,12
    !    do j=i,14
    !      call H%add((sin(j*1.0)+cos(i*1.0))*1.0_dp,i,j)
    !    end do
    ! end do
    ! call H%finalize()
    ! call cpu_time(cpu_tstop)
    ! ! write(*,*) " Size of H%data: ", size(H%data), " Time = ", cpu_tstop - cpu_tstart
    ! ! do i=1,size(H%data)
    ! !    write(*,*) H%idx(i), H%idy(i), " :: ", H%data(i), sin(1.0*H%idy(i))+cos(1.0*H%idx(i))
    ! ! end do
    ! rmse = 0.0d0
    ! do i=1,size(H%data)
    !    rmse = rmse + abs(H%data(i)- ( sin(1.0*H%idy(i))+cos(1.0*H%idx(i))))
    ! end do

    ! if (rmse>1.0d-16) then
    !    write(*,*) " * * * TEST FAILED * * *"
    !    write(*,*) " hamiltonian_type does not work as expected. "
    !    stop
    ! end if
    
    
    ! allocate(x(H%size))
    ! allocate(f(H%size))
    ! x(:) = 1.0
    ! call mkl_dcoosymv('U', 14, H%data, H%idx, H%idy, H%size, x, f)
  end subroutine hamiltonian_type_test
  
  
  subroutine fc_test()
    integer(kind=k1) :: i, j

    open(unit=10,file="fc_test.dat")
    do i=0,15
       do j=0,15
          write(10,"(I2,A,I2,A,4F25.15)") i, "  ", j, " : ",fcOverlapIntegral(i,j,0.0d0), fcOverlapIntegral(i,j,0.5d0), fcOverlapIntegral(i&
               &,j,1.0d0), fcOverlapIntegral(i,j,1.5d0)
       end do
    end do

    close(unit=10)
  end subroutine fc_test
  

end module testing
