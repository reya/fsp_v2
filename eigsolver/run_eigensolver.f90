! # This is a small test program which illustrates the usage of the eigensolver module #
program eigsolver_test
  use mpi ! We want MPI anyway, even if we're running only a single process
  use eigensolver ! Using the module
  implicit none
  integer, parameter :: dp=kind(1.0d00) ! 8-byte real
  integer :: mpi_rank, mpi_comm, mpi_size, mpi_status
  integer :: number_of_eig, local_size, problem_size, local_neig_start, local_neig_stop
  integer :: i, j, ierr, loop_start, loop_stop, step_neig, step_loop
  real(kind=dp) :: matrix_element
  real(kind=dp), allocatable, dimension(:) :: w ! Eigenvalues
  real(kind=dp), allocatable, dimension(:,:) :: U ! Eigenvectors
  
  call MPI_INIT(mpi_status)
  mpi_comm = MPI_COMM_WORLD
  call MPI_COMM_RANK(mpi_comm, mpi_rank, mpi_status)
  call MPI_COMM_SIZE(mpi_comm, mpi_size, mpi_status)

  ! First, define number of eigenpairs to compute:
  number_of_eig = 200
  ! Second, define size of the matrix in one dimension,
  ! meaning that the matrix has size (problem_size, problem_size) 
  problem_size = 1500

  ! Third, set up the local sizes owned by processes
  if (mpi_size==1) then
     ! ############################
     ! ### SERIAL USAGE EXAMPLE ###
     ! ############################
     ! Next, since we're dealing only with one process, set local sizes
     ! to cover the whole problem
     local_size = number_of_eig
     local_neig_start = 1
     local_neig_stop = number_of_eig
     loop_start=1
     loop_stop=problem_size
  else
     ! ##############################
     ! ### PARALLEL USAGE EXAMPLE ###
     ! ##############################
     ! If we have more then one process, we should divide the job
     step_neig = floor(1.0*number_of_eig/mpi_size)
     step_loop = floor(1.0*problem_size/mpi_size)
     if (mpi_rank==0) then
        loop_start = 1
        loop_stop = step_loop
        local_neig_start = 1
        local_neig_stop = step_neig
        local_size = step_neig
     else if (mpi_rank==(mpi_size-1)) then
        loop_start = step_loop*(mpi_rank)+1
        loop_stop = problem_size
        local_neig_start = step_neig*(mpi_rank)+1
        local_neig_stop = number_of_eig
        local_size = local_neig_stop-local_neig_start+1
     else
        loop_start = step_loop*mpi_rank+1
        loop_stop = step_loop*(mpi_rank+1)
        local_neig_start = step_neig*mpi_rank+1
        local_neig_stop = step_neig*(mpi_rank+1)
        local_size = local_neig_stop-local_neig_start+1
     end if
  end if

  
  ! When everything is ready, let's prepare the eigensolver by this call:
  call prepare_eigensolver(&
       number_of_eig_in=number_of_eig, &        ! Number of eigenvalues to compute
       local_size_in=local_size, &              ! Local portion of eigenpairs stored on the MPI process
       problem_size_in=problem_size, &          ! Matrix dimensions (problem_size, problem_size)
       local_neig_start_in=local_neig_start, &  ! Starting index for local portion of eigenpairs, inclusive
       local_neig_stop_in=local_neig_stop,&     ! Stopping index for local portion of eigenpairs, inclusive
       print_output_in=.FALSE. &                   ! Print eigenvalues when they are computed?
       )  

  ! Now, let's set up the matrix elements for the problem

  
  do i=loop_start, loop_stop
     do j=i,problem_size
        if (i==j) then ! Set the diagonal matrix elements
           matrix_element=1.0d00*i
           call set_matrix_element(i,j,matrix_element) ! Calling this only once, since it's a
                                                       ! diagonal element
        else           ! Set the off-diagonal matrix elements
           matrix_element= 0.5d00*sin(1.0d00*i*j+j*i) ! Matrix element is the same since matrix is
                                                       ! real and symmetric;
           call set_matrix_element(i,j,matrix_element) ! Setting matrix element for upper triangle
           call set_matrix_element(j,i,matrix_element) ! and for lower triangle
        end if
     end do
  end do

  ! Next, we're allocating storage for eigenvalues and eigenvectors
  allocate(w(local_size),stat=ierr)
  ! Note that eigenvectors are stored in the following way:
  ! First index - large "problem_size" sized dimension
  ! Second index - small "number_of_eig" sized dimension
  allocate(U(problem_size,local_size),stat=ierr)
  
  ! After allocation, we're ready to run the solver! 
  call compute_eigenpairs(eigenvalues=w, eigenvectors=U, status=ierr) ! ierr must be 0 on the output

  ! Finally, free the memory used by the solver
  call finalize_eigensolver()

  ! Finalize MPI
  call MPI_FINALIZE(mpi_status)
  
end program eigsolver_test
