! ###########################
! ###  MODULE EIGENSOLVER ###
! ###########################
!
! PURPOSE
! Compute M lowest-lying eigenpairs (eigenvalues and eigenvectors) of the 8-byte real symmetric N*N matrix
! using the iterative Krylov-Schur method from SLEPc
!
! This module is designed as a wrapper around PETSc and SLEPc functions. You must have PETSc and
! SLEPc installed before using it.
!    PETSc: http://www.mcs.anl.gov/petsc/
!    SLEPc: http://slepc.upv.es/
! MPI must be installed. Take a look at the Makefile to get an idea how to compile this module. 
!
! USAGE
! To compute M lowest-lying eigenpairs of the real symmetric N*N matrix, you should make a sequence
! of calls to this module. The module has been tested to with 2, 4, 6, 8, 10, 12, 14, 16 MPI processes.
!
! 1. Put "use eigensolver" in the appropriate place at the beggining of the code:
!    program foo
!    use mpi
!    use eigensolver ! <- there.
!    implicit none
!    type :: vars 
!
! 2. Define N, M and local sizes of portions of eigenvalues and eigenvectors stored on a single MPI
!    process. Look at the examples in the "main.f90" file.
!
! 3. Make a call to the prepare_eigensolver() subroutine. Look at the example in the "main.f90" file.
!    call prepare_eigensolver(&
!         number_of_eig_in=number_of_eig, &        ! Number of eigenvalues to compute
!         local_size_in=local_size, &              ! Local portion of eigenpairs stored on the MPI process
!         problem_size_in=problem_size, &          ! Matrix dimensions (problem_size, problem_size)
!         local_neig_start_in=local_neig_start, &  ! Starting index for local portion of eigenpairs, inclusive
!         local_neig_stop_in=local_neig_stop,&     ! Stopping index for local portion of eigenpairs, inclusive
!         print_output_in=.FALSE. &                ! Print eigenvalues when they are computed?
!         )
!
! 4. Call set_matrix_element(i,j,matrix_element) subroutine for every non-zero matrix element i,j.
!    For zero elements, don't call this subroutine.
!
! 5. To start the solution process,
!    call compute_eigenpairs(eigenvalues=w, eigenvectors=U, status=ierr).
!    Make sure that sizes of array w and U on every MPI process correspond to the pattern:
!    w(local_size), U(problem_size,local_size), local_size==M is the number of eigenpairs stored on a
!    given MPI process, and problem_size==N is the dimension of the N*N matrix specifying the problem.
!    
! 6. After that, finalize the solver:
!    call finalize_eigensolver()
!
! NOTE: Module assumes usage of 4-byte integers. This behavior can be changed by modifying the ik
!       variable below.
!
! AUTHOR:
! Andrei Zimin (reyenka@gmail.com)
!

module eigensolver_slepc
  use mpi
#include "slepc/finclude/slepcepsdef.h"
  use slepceps
  use hamiltonian, only: hamiltonian_type
  use basis_set, only: basis_set_size
  use rtlib, only : rt
  use omp_lib
!  use utils !, only: chkierr 
  implicit none

  ! ###
  ! Context used to store iteration number
  type iteration_context
     integer :: iter
     double precision :: finish, start
  end type iteration_context

  
  integer(kind=4), parameter :: ik=4  ! Default integer kind (4-byte)
  integer(kind=4), parameter :: dp=kind(1.0d00) ! 8-byte real kind

  integer(kind=ik) :: number_of_eig, local_size, problem_size
  integer(kind=ik) :: local_neig_start, local_neig_stop
  integer(kind=ik) :: mpi_comm, mpi_size, mpi_rank
  type(iteration_context) :: itercounter
  type(hamiltonian_type) :: Hmatrix
  ! #########################################
  ! ### PETSc and SLEPc related variables ###
  ! #########################################
  ! Matrix of the problem
  Mat  :: H
  EPS  :: solver    ! Eigensolver object
  ST   :: sptrans   ! Spectral transform object
  KSP  :: kspsolver ! KSP solver object
  PC   :: precond   ! Preconditioner object
  PetscInt :: d_nz, o_nz, p_psize, p_local_size, p_neig, ctx
  Vec  :: xr_global
  Vec  :: xr, xi    ! Real and Imag parts of the solution (only xr is used)
  logical :: print_output

  public :: prepare_eigensolver, set_matrix_element, compute_eigenpairs, finalize_eigensolver
  private
  
contains
  
  
  ! ######
  ! Prepare the module, set sizes of the problem
  subroutine prepare_eigensolver(number_of_eig_in, local_size_in, problem_size_in, local_neig_start_in&
       &, local_neig_stop_in, print_output_in)
    ! number_of_eig:           number of lowest-lying eigenpairs to compute
    !
    ! local_size:              Size of the local portion of eigenvalues and eigenvectors
    !                          stored on the given process
    ! 
    ! problem_size:            number of rows = number of columns in the
    !                          matrix of the problem
    ! local_neig_start:        Index of the first eigenvalue owned by the process (1-based)
    ! local_neig_stop:         Index of the last eigenvalue owned by the process (1-based)
    integer(kind=ik), intent(in) :: number_of_eig_in, local_size_in, problem_size_in
    integer(kind=ik), intent(in) :: local_neig_start_in, local_neig_stop_in
    logical, intent(in) :: print_output_in
    integer(kind=ik) :: ierr

    
    ! ### Assign global module variables ###
    number_of_eig = number_of_eig_in
    p_neig = number_of_eig_in
    local_size = local_size_in
    problem_size = problem_size_in
    local_neig_start = local_neig_start_in
    local_neig_stop = local_neig_stop_in
    p_psize = problem_size_in
    p_local_size = local_size_in
    print_output = print_output_in
    ! ###
    
    if (number_of_eig>problem_size) then
       call raiseException(" prepare_eigensolver: number of requested eigenvalues is greater then si&
            ze of the problem")
    end if
    if (local_size>problem_size) then
       call raiseException(message=" prepare_eigensolver: local size of eigenpair portion is greater then si&
            ze of the problem")
    end if
    if ((local_size<=0).or.(problem_size<=0).or.(number_of_eig<=0)&
         .or.(local_neig_start<=0).or.(local_neig_stop<=0)) then
       call raiseException(message="prepare_eigensolver: size of the problem must be greater then 0")
    end if
    if (local_neig_start>=local_neig_stop) then
       call raiseException(message=" prepare_eigensolver: wrong start and stop indices for local portion of &
            eignpairs (local_neig_start>=local_neig_stop)")
    end if

    ! #######################################
    ! ### INITIALIZE MATRIX OF THE SYSTEM ###
    ! #######################################
    d_nz = problem_size
    o_nz = ((problem_size**2)/2+problem_size)/2 ! Worst-case guess for number of off-diagonal
                                             ! elements, implying 50% sparsity.

    mpi_comm = MPI_COMM_WORLD

    
    call MPI_COMM_RANK(mpi_comm, mpi_rank, ierr); call chkierr(stat=ierr,label="mpi_init")
    call MPI_COMM_SIZE(mpi_comm, mpi_size, ierr); call chkierr(stat=ierr,label="mpi_size")

    call SlepcInitialize(PETSC_NULL_CHARACTER,ierr); call chkierr(stat=ierr,label="SlepcInitialize")


    if (rt%implicit_solver) then
       write(*,*) " Creating shell "
       ctx = 1
       call MatCreateShell(PETSC_COMM_WORLD, PETSC_DECIDE, PETSC_DECIDE, p_psize, p_psize, ctx, H, ierr); call chkierr(ierr);
       write(*,*) " Setting matrix "
       call Hmatrix%initialize(initial_size=floor(0.7*basis_set_size**2),increment=floor(0.1*basis_set_size**2))
       call MatSetFromOptions(H,ierr); call chkierr(ierr);
       call MatShellSetOperation(H,MATOP_MULT,apply_hamiltonian,ierr); call chkierr(ierr);
       call MatShellSetOperation(H,MATOP_MULT_TRANSPOSE,apply_hamiltonian,ierr); call chkierr(ierr);
       write(*,*) " Get diagonal "
       call MatShellSetOperation(H,MATOP_GET_DIAGONAL,matop_get_diagonal_hamiltonian,ierr); call chkierr(ierr);
       write(*,*) " Shift "
       call MatShellSetOperation(H,MATOP_SHIFT,matop_shift_hamiltonian,ierr); call chkierr(ierr);
       ! write(*,*) " AXPY "
       ! call MatShellSetOperation(H,MATOP_AXPY,matop_axpy_hamiltonian,ierr); call chkierr(ierr);
       call MatSetOption(H, MAT_SYMMETRIC, PETSC_TRUE, ierr); call chkierr(ierr);
       call MatSetOption(H, MAT_SYMMETRY_ETERNAL, PETSC_TRUE, ierr); call chkierr(ierr);
       
    else
       call MatCreateAIJ(mpi_comm, PETSC_DETERMINE, PETSC_DETERMINE, p_psize, p_psize, d_nz, PETSC_NULL_INTEGER&
            &, o_nz, PETSC_NULL_INTEGER, H, ierr); call chkierr(stat=ierr,label="MatCreateAIJ")

       call MatSetOption(H, MAT_SYMMETRIC, PETSC_TRUE, ierr); call chkierr(stat=ierr,label="MatSetOption: symmetric")
       call MatSetOption(H, MAT_SYMMETRY_ETERNAL, PETSC_TRUE, ierr);
       ! call MatMPIAIJSetPreallocation(H, d_nz, PETSC_NULL_CHARACTER, o_nz, PETSC_NULL_CHARACTER, ierr)
       call MatSetOption(H, MAT_NEW_NONZERO_ALLOCATION_ERR, PETSC_FALSE, ierr)
       call chkierr(stat=ierr,label="MatSetOption: symmetry_eternal")
    end if
  end subroutine prepare_eigensolver

  subroutine apply_pc(A, X, F, ierr)
    Mat, intent(in)        :: A
    Vec, intent(in)        :: X
    Vec, intent(out)       :: F
    PetscErrorCode, intent(out) :: ierr
    PetscScalar, pointer :: xx_v(:)
    PetscScalar, pointer :: ff_v(:)

    call VecGetArrayReadF90(X, xx_v, ierr)
    call VecGetArrayF90(F, ff_v, ierr)

    call Hmatrix%diagonal_preconditioner(xx_v,ff_v)
!    call Hmatrix%GS_preconditioner(xx_v,ff_v)
    !call Hmatrix%matvec(xx_v,ff_v)
    
    call VecRestoreArrayReadF90(X, xx_v, ierr)
    call VecRestoreArrayF90(F, ff_v, ierr)
    ierr = 0

  end subroutine apply_pc
  
  subroutine apply_hamiltonian(A, X, F, ierr)
    Mat, intent(in)        :: A
    Vec, intent(in)        :: X
    Vec, intent(out)       :: F
    PetscErrorCode, intent(out) :: ierr
    PetscScalar, pointer :: xx_v(:)
    PetscScalar, pointer :: ff_v(:)

    call VecGetArrayReadF90(X, xx_v, ierr)
    call VecGetArrayF90(F, ff_v, ierr)

    call Hmatrix%matvec(xx_v,ff_v)

    call VecRestoreArrayReadF90(X, xx_v, ierr)
    call VecRestoreArrayF90(F, ff_v, ierr)
    ierr = 0
  end subroutine apply_hamiltonian

  subroutine matop_axpy_hamiltonian(A,X,Y,F,ierr)
    Mat, intent(in) :: A
    Vec, intent(in)        :: X, Y
    Vec, intent(out)       :: F
    PetscErrorCode, intent(out) :: ierr
    PetscScalar, pointer :: yy_v(:)
    PetscScalar, pointer :: ff_v(:)
    
    call apply_hamiltonian(A, X, F, ierr)
    
    call VecGetArrayReadF90(Y, yy_v, ierr)
    call VecGetArrayF90(F, ff_v, ierr)

    ff_v(:) = ff_v(:) + yy_v(:)

    call VecRestoreArrayF90(F, ff_v, ierr)
    call VecRestoreArrayReadF90(Y, yy_v, ierr)
  end subroutine matop_axpy_hamiltonian
  
  subroutine matop_shift_hamiltonian(A,shift,ierr)
    Mat, intent(in) :: A
    PetscReal,intent(in) :: shift
    PetscErrorCode, intent(out) :: ierr
    call Hmatrix%set_shift(shift)
    ierr = 0
  end subroutine matop_shift_hamiltonian
  
  
  subroutine matop_get_diagonal_hamiltonian(A, F, ierr)
    Mat, intent(in) :: A
    Vec, intent(out) :: F
    PetscScalar, pointer :: ff_v(:)
    PetscErrorCode, intent(out) :: ierr
    
    call VecGetArrayF90(F, ff_v, ierr)
    call Hmatrix%get_diagonal(ff_v)
    call VecRestoreArrayF90(F, ff_v, ierr)
    
    ierr = 0
  end subroutine matop_get_diagonal_hamiltonian
  
  
  ! ######
  ! Fill the matrix of the problem at positions i, j
  ! i, j are 1-based indicies (Fortran convention)
  ! NOTE: user must call this subroutine for every non-zero element of the matrix, i.e. both
  ! (i,j) and (j,i) pairs must be defined explicitly. In case of symmetric matrix, it's done by
  ! first computing the (i,j) matrix element and then calling this subroutine twice for 
  ! (i,j) and (j,i).
  subroutine set_matrix_element(i,j,matrix_element)
    integer(kind=ik), intent(in) :: i, j ! indices of the matrix element
    real(kind=dp), intent(in)    :: matrix_element
    PetscInt :: pi, pj
    PetscScalar ::  HamiltonianME
    integer(kind=ik) :: ierr
    character(80) :: current_position
    character(80) :: msg
    if ((i>problem_size).or.(i<1)) then
       write(msg,"(A,I5)") "ERROR in set_matrix_element: index i is out of bounds, i = ",i
       call raiseException(message=msg) 
    end if
    if ((j>problem_size).or.(j<1)) then
       write(msg,"(A,I5)") "ERROR in set_matrix_element: index i is out of bounds, j = ",j
       call raiseException(message=msg) 
    end if

    if (rt%implicit_solver) then
       call Hmatrix%add(matrix_element, i, j)
    else
       pi = i-1
       pj = j-1
       HamiltonianME=matrix_element
       write(current_position,"(A,I8,A,I8)") "MatSetValues at i=",i,"  j=",j
       call MatSetValues(H,1,pj,1,pi,HamiltonianME,INSERT_VALUES,ierr); call chkierr(stat=ierr,&
            label=current_position);
    end if
  end subroutine set_matrix_element
  
  ! ######
  ! Main subroutine: computes local portion of eigenpairs and returns the result.
  ! status must be 0. If it's not, something went wrong.
  subroutine compute_eigenpairs(eigenvalues, eigenvectors, status)
    real(kind=dp), dimension(local_size), intent(out) :: eigenvalues
    real(kind=dp), &
         dimension(problem_size,local_size), intent(out) :: eigenvectors
    integer(kind=ik) :: mpi_status, mpi_rank, mpi_size
    integer(kind=ik) :: i, j, k, low, high, nconv
    integer(kind=ik), intent(out) :: status ! 0 if everything was fine, negative otherwise
    integer :: ierr
    character(80) :: msg
    PetscInt :: pi, pj
    VecScatter :: vecscatctx
    PetscScalar, pointer :: xx_v(:)
    PetscScalar :: kr, ki
    PetscReal   :: p_tolerance, p_shift
    PetscInt    :: p_maxiter
    
    ! # Assemble the matrix
    call MatAssemblyBegin(H,MAT_FINAL_ASSEMBLY,ierr); call chkierr(stat=ierr,label="MatAssemblyBegin");
    call MatAssemblyEnd(H,MAT_FINAL_ASSEMBLY,ierr); call chkierr(stat=ierr,label="MatAssemblyEnd");

    if (rt%implicit_solver) then
       call Hmatrix%finalize()
    end if
    
    call VecCreateMPI(mpi_comm, PETSC_DETERMINE, p_psize, xr, ierr); call chkierr(stat=ierr,label="VecCreateMPI (xr)");
    call VecCreateMPI(mpi_comm, PETSC_DETERMINE, p_psize, xi, ierr); call chkierr(stat=ierr,label="VecCreateMPI (&
         &xi)");
    call VecCreateSeq(PETSC_COMM_SELF, p_psize, xr_global, ierr); call chkierr(stat=ierr,label="VecCreateSeq");

    ! ##################################
    ! ### Create eigensolver context ###
    ! ##################################
    !     
    call EPSCreate(mpi_comm,solver,ierr); call chkierr(stat=ierr,label="EPSCreate")
    
    !     ** Set operators. In this case, it is a standard eigenvalue problem
    call EPSSetOperators(solver,H,PETSC_NULL_OBJECT,ierr); call chkierr(stat=ierr,label="EPSSetOperators")
    call EPSSetProblemType(solver,EPS_HEP,ierr); call chkierr(stat=ierr,label="EPSSetProblemType")
    

    ! ###########################
    ! ### Set the solver type ###
    ! ###########################
    ! Pick up the Krylov-Schur solver
    call EPSSetType(solver, EPSKRYLOVSCHUR, ierr); call chkierr(stat=ierr,label="EPSSetType")
    p_tolerance = 1.0d-12
    p_maxiter   = 50000
    call EPSSetTolerances(solver,p_tolerance,p_maxiter, ierr); call chkierr(stat=ierr,label="EPSSetTolerances")
    !call EPSSetType(solver, EPSLAPACK,ierr)
    !call EPSSetType(solver, EPSARNOLDI, ierr)
    ! ###########################

    ! ########################################
    ! ### Set up the spectral transform    ###
    ! ### together with the preconditioner ###
    ! ########################################

    ! Get the spectral transform
    call EPSGetST(solver, sptrans, ierr); call chkierr(stat=ierr,label="EPSSetGetST")
    
    ! Set ST type

    ! Get KSP
    call STGetKSP(sptrans, kspsolver, ierr); call chkierr(stat=ierr,label="STGetKSP")
    p_shift = 1.2
    call STSetShift(sptrans, p_shift, ierr); call chkierr(stat=ierr,label="STSetShift")
!    call STSetType(sptrans, STPRECOND, ierr)
    call STSetType(sptrans, STSINVERT, ierr); call chkierr(stat=ierr,label="STSetType")

    ! Get PC
    call KSPGetPC(kspsolver, precond, ierr); call chkierr(stat=ierr,label="KSPGetPC")
    call KSPSetType(kspsolver, KSPGMRES, ierr); call chkierr(stat=ierr,label="KSPSetType")
    


    call KSPSetTolerances(kspsolver, p_tolerance,PETSC_DEFAULT_REAL,PETSC_DEFAULT_REAL,PETSC_DEFAULT_INTEGER, ierr);  call chkierr(stat=ierr,label="KSPSetTolerances")
    ! Set PC
    !    call PCSetType(precond, PCASM, ierr); call chkierr(stat=ierr,label="PCSetType")
    if (rt%implicit_solver) then
       call STSetMatMode(sptrans, ST_MATMODE_SHELL, ierr);  call chkierr(stat=ierr,label="STSetMatMode")
       call PCSetType(precond, PCSHELL, ierr); call chkierr(stat=ierr,label="PCSetType")
       call PCShellSetApply(precond, apply_pc, ierr); call chkierr(stat=ierr,label="PCSetApply")
    else
       call PCSetType(precond, PCASM, ierr); call chkierr(stat=ierr,label="PCSetType")
    end if
    
    !call PCSetType(precond, PCCHOLESKY, ierr); call chkierr(stat=ierr,label="PCSetType")


    if (p_psize<200) then
       call EPSSetDimensions(solver, p_neig, p_psize, p_psize,ierr); call chkierr(stat=ierr,label="EPSSetDimensions")
    else
       if (p_neig+p_neig/2+100<p_psize) then
          call EPSSetDimensions(solver, p_neig, p_neig+p_neig/2+100, p_neig+p_neig/2+100,ierr); call chkierr(stat=ierr,label="EPSSetDimensions")
       else
          call EPSSetDimensions(solver, p_neig, p_psize, p_psize,ierr); call chkierr(stat=ierr,label="EPSSetDimensions")
       end if
    end if
    
    call EPSSetWhichEigenpairs(solver, EPS_SMALLEST_MAGNITUDE,ierr); call chkierr(stat=ierr,label="EPSSetWhichEigenpairs")
    
    call EPSMonitorSet(solver, monitor_solve, PETSC_NULL_OBJECT,PETSC_NULL_FUNCTION, ierr); call chkierr(stat=ierr,label="EPSMonitorSet")


    call EPSSetFromOptions(solver,ierr); call chkierr(stat=ierr,label="EPSSetFromOptions")
    ! Set itercounter to 0
    itercounter%iter=0
    !call cpu_time(itercounter%start)
    itercounter%start = omp_get_wtime()

    call EPSSolve(solver,ierr); call chkierr(stat=ierr,label="EPSSolve")

    call MPI_BARRIER(mpi_comm, ierr); call chkierr(stat=ierr,label="MPI_BARRIER after EPSSolve")
    
    call EPSGetConverged(solver,nconv,ierr);  call chkierr(stat=ierr,label="EPSGetConverged")
    if (nconv>0) then
       i=1
       do pi=0,p_neig-1
          call EPSGetEigenPair(solver,pi,kr,ki,xr,xi,ierr);
          call chkierr(stat=ierr,label="EPSGetEigenPair")
          call MPI_BARRIER(mpi_comm, ierr); 
          
          if ((pi>=(local_neig_start-1)).and.(pi<=(local_neig_stop-1))) then
             if (print_output) then
                write(*,"(I4,A,F20.12)") pi+1," : ", kr
             end if
             
             if (i>local_size) then
                write(msg,*) " ERROR: eigenvalues array is out of bounds"
                call raiseException(message=msg)
             end if
             eigenvalues(i)=kr
          end if
          call MPI_BARRIER(mpi_comm, ierr); 
          call VecScatterCreateToAll(xr,vecscatctx,xr_global, ierr); call chkierr(stat=ierr,label="VecScatterCreateToAll")
          call VecScatterBegin(vecscatctx, xr, xr_global, &
               INSERT_VALUES, SCATTER_FORWARD, ierr); call chkierr(stat=ierr,label="VecScatterBegin")
          call VecScatterEnd(vecscatctx, xr, xr_global, &
               INSERT_VALUES, SCATTER_FORWARD, ierr); call chkierr(stat=ierr,label="VecScatterEnd")
          call VecScatterDestroy(vecscatctx, ierr); call chkierr(stat=ierr,label="VecScatterDestroy")

          call VecGetArrayF90(xr_global, xx_v, ierr);  call chkierr(stat=ierr,label="VecGetArrayF90")

          if ((pi>=(local_neig_start-1)).and.(pi<=(local_neig_stop-1))) then
             do j=1,problem_size
                eigenvectors(j,i)=xx_v(j)
             end do
          end if
          
          call VecRestoreArrayReadF90(xr_global,xx_v,ierr); call chkierr(stat=ierr,label="VecRestoreArrayF90")
          
          if ((pi>=(local_neig_start-1)).and.(pi<=(local_neig_stop-1))) then
             i=i+1
          end if
       end do
       status=0
    else
       write(msg,*) " ERROR in compute_eigenpairs(): failed to converge"
       status=-1
       call raiseWarning(message=msg)
    end if
  end subroutine compute_eigenpairs
    
  ! ######
  ! Free used memory
  subroutine finalize_eigensolver()
    integer :: ierr
    call MatDestroy(H,ierr); call chkierr(stat=ierr)
    call VecDestroy(xr,ierr); call chkierr(stat=ierr)
    call VecDestroy(xi,ierr); call chkierr(stat=ierr)
    call VecDestroy(xr_global, ierr); call chkierr(stat=ierr)
    call EPSDestroy(solver,ierr); call chkierr(stat=ierr)
    call SlepcFinalize(ierr); call chkierr(stat=ierr)
  end subroutine finalize_eigensolver

  
  subroutine monitor_solve(solver, its, nconv, eigr, eigi, errest, nest)
    implicit none 
    EPS :: solver
    PetscScalar :: eigi, eigr
    PetscReal   :: errest
    PetscInt    :: its, nconv, nest
    integer :: rank, ierr
    ! Increase number of iterations
    itercounter%iter = itercounter%iter + 1
    
    if (itercounter%iter==1) then
       if (mpi_rank.eq.0) then
          write(*, "(A)") " ------------------------------------------- "
          write(*, "(A)") "      STARTING KRYLOV-SCHUR EIGENSOLVER      "
          write(*, "(A)") " ------------------------------------------- "
          write(*, "(A,I8)") " Problem dimensions: ", problem_size
          write(*, "(A,I8)") " Requested number of eigenpairs: ", number_of_eig
          write(*, "(A,I8)") " Number of MPI processes: ", mpi_size
          write(*, "(A,I8)") " Number of OpenMP threads: ", omp_get_num_threads()
          print "(A)", " ------------------------------------------- "
          print "(A)", " # Iteration | Space size   | Converged    | "
          print "(A)", " ------------------------------------------- "
       end if
    end if
    
    if (mpi_rank.eq.0) then
       !call cpu_time(itercounter%finish)
       itercounter%finish = omp_get_wtime()
       print "(I8,A6,I8,A7,I10,A,F25.4, A)", itercounter%iter, "|", nest, "|", nconv,&
            "    |  (", itercounter%finish-itercounter%start, " s )"
    end if
    !call cpu_time(itercounter%start)
    itercounter%start = omp_get_wtime()
  end subroutine monitor_solve
  
  subroutine chkierr(stat,label)
    integer, intent(in) :: stat
    character(*), intent(in), optional :: label
    if (stat.ne.0) then
       if (present(label)) then
          write(*,*) "Problem is at label=",label
          call raiseException(message=label,errcode=stat)
       else
          call raiseException(message="Unclassified exception",errcode=stat)
       end if
    end if
  end subroutine chkierr

  subroutine raiseException(message, errcode)
    character(*), intent(in) :: message
    integer, intent(in), optional :: errcode
    integer :: ierr
    
    if (mpi_rank.eq.0) then
       write(*,"(A,I4)") " [EIGENSOLVER] ERROR :: ABORTING EXECUTION"
       write(*,*) message
    end if
         
    if (present(errcode)) then
       call MPI_ABORT(mpi_comm, errcode, ierr)
    else
       call MPI_ABORT(mpi_comm, 1, ierr)
    end if
  end subroutine raiseException

  subroutine raiseWarning(message, errcode)
    character(*), intent(in) :: message
    integer, intent(in), optional :: errcode
    integer :: ierr

    if (mpi_rank.eq.0) then
       write(*,"(A,I4)") " [EIGENSOLVER] WARNING"
       write(*,*) message
    end if
  end subroutine raiseWarning

  
end module eigensolver_slepc
