from __future__ import division
import sys
import os, shutil
import argparse
### HDF5 I/O ###
#import h5py
### NumPy module ###
import numpy as np

import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import MaxNLocator
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.colors as col
from matplotlib import cm
import seaborn as sns

#from scipy.optimize import minimize, fsolve, root
#from subprocess import call
def ha_to_rcm(x):
    return x*219474.631370515

def rcm_to_ha(x):
    return x/219474.631370515

def ha_to_ev(x):
    return x*27.2113850560

def ev_to_ha(x):
    return x/27.2113850560

def ev_to_rcm(x):
    return ha_to_rcm(ev_to_ha(x))

def rcm_to_ev(x):
    return ha_to_ev(rcm_to_ha(x))

### Helper function to convert centimeters to inches ###
def cm_to_in(x):
    return x*0.393701

def prepare_plot(number_of_lines=2):
    sns.set_style("ticks",{'xtick.major.size': 5.0,
                           'xtick.direction': u'inout',
                           'ytick.direction': u'inout',
                           'zorder':1,
                           'ytick.major.size': 5.0})

    matplotlib.rcParams.update({"font.size":10})
    sns.set_palette(sns.husl_palette(number_of_lines,l=.55))
    fig = plt.figure(figsize=[cm_to_in(9),cm_to_in(8)])
    #fig = plt.figure(figsize=[cm_to_in(5.0),cm_to_in(5.0)])

    return fig

omega_vib=1500

if __name__=="__main__":
    import xarray as xr
    import pandas as pd
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',type=str,help='File to open: integrated emission (.nc)',required=True)
    parser.add_argument('--plot_2D',type=str,choices=["yes","no"],help='Plot 2D surface?',default="yes",required=True)

    args = parser.parse_args()
    ds = xr.open_dataset(args.filename)
    print "\n\n * DATASET * "
    print ds
    print " \n\n "
    
    # For +/- stuff
    cmap=sns.diverging_palette(h_neg=250, h_pos=380, s=99, l=45, n=250, center='light',as_cmap=True)
    # For sequential stuff:
    #cmap=sns.light_palette((210, 90, 60), input="husl",as_cmap=True)

    print " J values: ", ds['Iems'].J.values
    Jrange = ds['Iems'].J.values
    Srange = ds['Iems'].S.values
    Nrange = ds['Iems'].N.values
    print " S Values: ", ds['Iems'].S.values
    print " N Values: ", ds['Iems'].N.values

    #sys.exit()
    #X,Y = np.meshgrid(np.asarray(ds['data'].S),np.asarray(ds['data'].J))
    #X,Y = np.meshgrid(np.asarray(ds['data'].S),np.asarray(ds['data'].N))
    #X,Y = np.meshgrid(np.asarray((ds['data'].J)),np.asarray(ds['data'].N))
    #data = np.asarray(ds['data'].loc[{'J':Jrange[-1]}])#Jrange[-30]}])
    #iems_levels=[0.005,0.01,0.02, 0.03, 0.05, 0.08]#[0.2,0.4,0.6,0.8]
    
    plot_2D = {'yes':True, 'no':False}[args.plot_2D]
    plot_ipr = True
    plot_Iems = False
    if (plot_2D):
        fig = prepare_plot()
        iems = ds['Iems'].loc[{'S':Srange[3]}]
        iems_electronic = ds['Iems'].loc[{'S':Srange[0]}]
        iems = iems - iems_electronic
        #iems = iems.where(iems.J>-0.1,drop=True)
        print "Plotting the 2D colormesh with data = ", iems
        x = iems.J
        y = iems.N
        X, Y = np.meshgrid(np.asarray(x),np.asarray(y))
        data = np.asarray(iems)
        iems_levels = np.linspace(np.min(data),np.max(data),20)
        nrange_levels = np.linspace(np.min(Nrange),np.max(Nrange),20)
        ax = fig.gca(projection='3d')
        surf = ax.plot_surface(X/omega_vib, Y, data.T,rstride=1, cstride=1, cmap=cmap,
                               linewidth=0, antialiased=True, alpha=0.7)
        cset = ax.contour(X/omega_vib,Y,data.T, zdir='z', offset=np.min(data), levels=iems_levels, cmap=cmap)#cm.coolwarm)
        plt.xlabel("Coulomb coupling, J/$\omega_{vib}$")
        #plt.xlabel("Huang-Rhys factor")
        plt.ylabel("Number of monomers")
        ax = plt.gca()
        plt.tight_layout()
        plt.show()
        sys.exit()
    

    # iems_electronic = ds['data'].loc[{'S':Srange[0]}]
    # iems_electronic = iems_electronic.where(iems_electronic.J>-0.1,drop=True)
    # iems_electronic = iems_electronic.loc[{'N':3}]
    # plt.plot(iems_electronic.J.values/omega_vib,iems_electronic.values,linewidth=1.0,marker='.',label="S=0.00")

    if (plot_ipr):
        for N_current in Nrange:
            fig = prepare_plot(np.size(Srange))
            #c_1p = ds['ipr_total']#.loc['N':N_current]
            c_1p = (ds['pr_1p'])#.loc['N':N_current]
            for S_idx, Srange_current in enumerate(Srange):
                data = c_1p.loc[{'S':Srange_current}]
                plt.plot(data.J.values/omega_vib,data.values,c=sns.color_palette()[S_idx],linewidth=0.5)
            plt.xlabel("$V/\omega_{vib}$")
            plt.ylabel(r"PR(1p)")
            plt.tight_layout()
            filename = "pr_1p_plot_"+str(data['N'].values)
            plt.savefig(filename+".png",dpi=600)
            plt.savefig(filename+".svg")
            plt.savefig(filename+".eps")
            plt.savefig(filename+".pdf",dpi=600)
            
    if (plot_Iems):
        for N_current in Nrange:
            fig = prepare_plot(np.size(Srange[1:]))
            lines = []
            label_list = []
            print "S range is ", Srange
            iems_electronic = ds['Iems'].loc[{'S':Srange[0]}]
            iems_electronic = iems_electronic.where(iems_electronic.J>=-omega_vib,drop=True)
            iems_electronic = iems_electronic.where(iems_electronic.J<=omega_vib,drop=True)
            iems_electronic = iems_electronic.loc[{'N':N_current}]
            l, = plt.plot(iems_electronic.J.values/omega_vib,iems_electronic.values,'k--',linewidth=0.5)
            lines.append(l)
            label_list.append("Electronic")
            for S_idx,Srange_current in enumerate(Srange[1:]):#[Srange[0],Srange[3],Srange[-1]]:#Srange[1:]:
                iems = ds['Iems'].loc[{'S':Srange_current}]
                iems_electronic = ds['Iems'].loc[{'S':Srange[0]}]
                iems = iems - iems_electronic
                iems = iems.where(iems.J>=-omega_vib,drop=True)
                iems = iems.where(iems.J<=omega_vib,drop=True)
                #iems = iems.where(iems.J<0.4*1500,drop=True)
                iems_N = iems.loc[{'N':N_current}]
                print "Plotting the 1D data ", iems_N
                #l, = plt.plot(iems_N.J.values/omega_vib,iems_N.values,linewidth=1.0,marker='.',markersize=4)#,label="%2.1f"%(Srange_current))
                l, = plt.plot(iems_N.J.values/omega_vib,iems_N.values,c=sns.color_palette()[S_idx],linewidth=0.5)#,label="%2.1f"%(Srange_current))
                lines.append(l)
                label_list.append("S=%3.2f"%(Srange_current))
                plt.text(0.5,0.5,r"$N=%i$"%N_current)
            plt.xlabel("$V/\omega_{vib}$")
            plt.ylabel(r"$F_{vib}$, a.u.")
            #plt.ylim([-3.6,0.6])
            plt.xlim([-1,1])
            legend1 = plt.legend([l for l in lines[0:4]],label_list[0:4],loc='center left')
            legend2 = plt.legend([l for l in lines[5:]],label_list[5:],loc='upper right')
            plt.gca().add_artist(legend1)
            plt.tight_layout()
            filename = "iems_plot_"+str(iems_N['N'].values)
            plt.savefig(filename+".png",dpi=600)
            plt.savefig(filename+".svg")
            plt.savefig(filename+".eps")
            plt.savefig(filename+".pdf",dpi=600)
            print " * * * \n DONE \n * * * \n"

