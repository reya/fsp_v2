module types

  implicit none 
  integer, parameter :: dp=kind(1.0d00)
  integer, parameter :: k1=selected_int_kind(2) ! 1-byte integer
  integer, parameter :: k4=selected_int_kind(8) ! 4-byte integer
  integer, parameter :: k8=selected_int_kind(10) ! 8-byte integer
  real(kind=dp), parameter :: pi = 4*atan(1.0_dp)
  ! N-particle state is composed from these states: we use 1-byte int here
  type particle_type
     integer(kind=k1) :: idx                 ! Index of monomer
     integer(kind=k1), allocatable :: vib(:) ! Vibrational excitations are here
  end type particle_type

  ! basis is an array of that type
  type basis_function_type
     type(particle_type), allocatable :: particle(:) ! N-particle components
     integer(kind=k1)                 :: np          ! N-particle number
  end type basis_function_type

  type runtime_lib_type
     integer :: Nmodes, Nmers, Nparticle, Ntemp, number_of_eig
     real(kind=dp), allocatable :: dipole(:,:)    ! Last index -> monomer
     real(kind=dp), allocatable :: position(:,:)  ! Last index -> monomer
     real(kind=dp), allocatable :: couplings(:,:) ! between pairs of monomers
     real(kind=dp), allocatable :: modes(:)  
     real(kind=dp), allocatable :: HRs(:)
     real(kind=dp), allocatable :: E00(:)
     real(kind=dp), allocatable :: Temp(:)
     real(kind=dp)              :: tiny=1.0d-12   ! Working precision to drop small quantities
     integer, allocatable       :: Ng(:), Ne(:)
     logical                    :: compute_eigenstates
     logical                    :: compute_absorption
     logical                    :: compute_emission
     logical                    :: only_nearest_neighbors
     logical                    :: full_output=.FALSE.
     logical                    :: implicit_solver ! Means that shell function will be given instead of hamiltonian
  end type runtime_lib_type

  type transition_type
     real(kind=dp) :: intensity
     real(kind=dp) :: population
     real(kind=dp), dimension(3) :: dipole
     real(kind=dp) :: E
  end type transition_type

  type dynamic_array_type
     real(kind=dp), allocatable  :: data(:)
     integer, allocatable        :: idx(:), idy(:)
     integer :: size
     integer :: position
     integer :: increment
     logical :: is_initialized=.FALSE.
     logical :: checked_allocation=.FALSE.
   contains
     procedure, pass :: add => dat_add_element
     procedure, pass :: set => dat_set_element
     procedure, pass :: initialize => dat_initialize_size
     procedure :: clear => dat_clear
     procedure :: finalize  => dat_finalize_size
  end type dynamic_array_type
  

  
  public :: dp, k1, pi
  public :: basis_function_type
  public :: runtime_lib_type
  public :: transition_type
  public :: dynamic_array_type 
  private
contains


  subroutine dat_finalize_size(this)
    class(dynamic_array_type), intent(inout) :: this
    real(kind=dp), allocatable, dimension(:) :: new_data
    integer, allocatable, dimension(:)       :: new_idx, new_idy
    integer :: ierr
    if (allocated(new_data)) deallocate(new_data)
    if (allocated(new_idx)) deallocate(new_idx)
    if (allocated(new_idy)) deallocate(new_idy)
    allocate(new_data(this%position),stat=ierr);  call chkmerr(ierr,info="[dat]: finalization allocation")
    new_data(1:this%position) = this%data(1:this%position)
    call move_alloc(new_data,this%data)
    allocate(new_idx(this%position),stat=ierr);  call chkmerr(ierr,info="[dat]: finalization idx allocation")
    new_idx(1:this%position) = this%idx(1:this%position)
    call move_alloc(new_idx,this%idx)
    allocate(new_idy(this%position),stat=ierr);  call chkmerr(ierr,info="[dat]: finalization idy allocation")
    new_idy(1:this%position) = this%idy(1:this%position)
    call move_alloc(new_idy,this%idy)
    this%size = this%position
  end subroutine dat_finalize_size
     
  subroutine dat_initialize_size(this,initial_size,increment)
    class(dynamic_array_type), intent(inout) :: this
    integer, intent(in) :: initial_size, increment
    this%increment = increment
    this%size = initial_size
    this%position = 0
    allocate(this%data(this%size))
    allocate(this%idx(this%size))
    allocate(this%idy(this%size))
    this%is_initialized = .TRUE.
  end subroutine dat_initialize_size

  subroutine dat_clear(this)
    class(dynamic_array_type), intent(inout) :: this
    this%increment = 0
    this%size = 0
    deallocate(this%data)
    deallocate(this%idx)
    deallocate(this%idy)
    this%is_initialized = .FALSE.
  end subroutine dat_clear
  
  subroutine dat_add_element(this, X, ix, iy)
    class(dynamic_array_type), intent(inout) :: this
    integer, intent(in)  :: ix, iy
    real(kind=dp),intent(in) :: X
    if (this%is_initialized) then
       call this%set(this%position+1,X,ix,iy)
    else
       write(*,*) "[dat_add_element]: type is not initialized"
       stop
    end if
  end subroutine dat_add_element
  
  subroutine dat_set_element(this,i,X,ix,iy)
    class(dynamic_array_type), intent(inout) :: this
    integer, intent(in) :: i, ix, iy
    real(kind=dp),intent(in) :: X
    real(kind=dp), dimension(:), allocatable :: new_data
    integer, dimension(:), allocatable :: new_idx, new_idy
    integer :: ierr

    if (.not.(this%checked_allocation)) then
       if (&
            (.not.(allocated(this%data)))&
            .or.&
            (.not.(allocated(this%idx)))&
            .or.&
            (.not.(allocated(this%idy))) &
            ) then
          write(*,*) "[hamiltonain_type_set_element]: error - not allocated"
          stop
       end if
    else
       this%checked_allocation=.TRUE.
    end if
    
    
    if (i<=this%size) then
       this%data(i) = X
       this%idx(i)  = ix
       this%idy(i)  = iy
       this%position = i
    else
       if (allocated(new_data)) deallocate(new_data)
       if (allocated(new_idx)) deallocate(new_idx)
       if (allocated(new_idy)) deallocate(new_idy)
       allocate(new_data(this%size+this%increment),stat=ierr);  call chkmerr(ierr,info="[dat_type]: new data allocation")
       new_data(1:this%size) = this%data(:)
       new_data(this%size+1:this%size+this%increment) = 0.0d0
       call move_alloc(new_data,this%data)
       allocate(new_idx(this%size+this%increment),stat=ierr);  call chkmerr(ierr,info="[dat_type]: new idx allocation")
       new_idx(1:this%size) = this%idx(:)
       new_idx(this%size+1:this%size+this%increment) = 0
       call move_alloc(new_idx,this%idx)
       allocate(new_idy(this%size+this%increment),stat=ierr);  call chkmerr(ierr,info="[dat_type]: new idy allocation")
       new_idy(1:this%size) = this%idy(:)
       new_idy(this%size+1:this%size+this%increment) = 0
       call move_alloc(new_idy,this%idy)
       this%size = this%size + this%increment
       this%data(i) = X
       this%idx(i)  = ix
       this%idy(i)  = iy
       this%position = i
    end if
    
  end subroutine dat_set_element

  subroutine chkmerr(ierr, info)
    integer, intent(in) :: ierr
    character(len=*), intent(in), optional :: info
    if (ierr.ne.0) then
       write(*,"(A, I4)") " ERROR: memory allocation failed with the code ierr=", ierr
       if (present(info)) then
          write(*,*) " Info: ", info
       end if
       stop
    end if
  end subroutine chkmerr

  
end module types
