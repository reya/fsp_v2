module rtlib
  use types, dpk=> dp
  use utils
  use io
  implicit none 

  type(runtime_lib_type), protected :: rt
  logical, protected :: isInitialized=.FALSE.
  public :: rt, initialize_rt, set_number_of_eig
  
  private
  
contains

  subroutine set_number_of_eig(number_in)
    integer, intent(in) :: number_in
    rt%number_of_eig = number_in
  end subroutine set_number_of_eig
  
  subroutine initialize_rt()
    character(len=256)  :: filename_initial
    character(:), allocatable :: filename_input, filename
    integer :: ierr, i, j, k
    integer :: boolint ! # Well, boolint is a nonsense. I need it to set up logicals in rt
    ! We read input lines in buffer and label vars.
    integer :: ios = 0, line = 0, funit=10, pos=0 ! Note that funit=10 is arbitrary. It could be any unit you want.
    character(len=256) :: buffer, label    

    if (isInitialized) then
       write(*,*) " ERROR: rt already initalized? "
       stop
    end if
    
    ! Get the input file name as a first command-line argument.
    call get_command_argument(1, filename_initial)
    filename_input = filename_initial(1:LEN_TRIM(filename_initial))

    write(*,*) " ======================================================== "
    write(*,*) "                        INPUT DATA                        "
    write(*,*) " "
    ! write(*,"(A,A)") "  * Reading input from the file: ", filename_input
    ! open(funit, file=filename_input)
    ! line=0
    ! do while (ios == 0) ! while file still has lines to read
    !    read(funit, '(A)', iostat=ios) buffer ! we read the line and put it in the buffer
    !    if (ios == 0) then  ! And if ios == 0, there is a line. If it's not, file has ended.
    !       line = line + 1   
    !       pos = scan(buffer, ' ') ! find where a space first occurs
    !       label = buffer(1:pos)   ! split the buffer into the label, which is before a space
    !       buffer = buffer(pos+1:) ! and into the value, which is after the space. 
    !       select case (label)     ! Now let's iterate over all possible labels
    !       case ('job_input')
    !          filename = buffer(1:LEN_TRIM(buffer))
    !          read(buffer, *, iostat=ios) filename
    !          print "(A,A)", '    HDF5 Input Filename = ', filename
    !       case default
    !          print *, "  * * * Skipping line in the input: " , line
    !       end select
    !    end if
    ! end do
    
    filename = filename_input
    
    print "(A,A)", '    HDF5 Input Filename = ', filename
    
    call readHDF5int(rt%Nmodes,"/Nmodes",filename)
    write(*,"(A,I4)") "    Nmodes = ", rt%Nmodes
    allocate(rt%Ng(rt%Nmodes),stat=ierr); call chkmerr(ierr,info="rt%Ng")
    call readHDF5dataset1D_INT(data=rt%Ng,size_in=rt%Nmodes,location="/Ng",filename=filename)
    write(*,"(A,10I4)") "    Ng     = ", rt%Ng
    allocate(rt%Ne(rt%Nmodes),stat=ierr); call chkmerr(ierr,info="rt%Ne")
    call readHDF5dataset1D_INT(data=rt%Ne,size_in=rt%Nmodes,location="/Ne",filename=filename)
    write(*,"(A,10I4)") "    Ne     = ", rt%Ne

    call readHDF5int(rt%Nmers,"/Nmers",filename)
    write(*,"(A,I4)") "    Nmers  = ", rt%Nmers
    call readHDF5int(rt%Ntemp,"/ntemp",filename)
    write(*,"(A,I4)") "    Ntemp  = ", rt%Ntemp
    call readHDF5int(rt%Nparticle,"/Nparticle",filename)
    write(*,"(A,I4)") "    Nparticle  = ", rt%Nparticle
    
    boolint = -1
    call readHDF5int(boolint,"/compute_eigenstates",filename)
    write(*,"(A,I4)") "    Compute_Eigenstates  = ", boolint
    if (boolint==1) then
       rt%compute_eigenstates = .TRUE.
    else if (boolint==0) then
       rt%compute_eigenstates = .FALSE.
    else
       write(*,*) " [RTLIB] Wrong value for compute_eigenstates. Exiting. "
    end if


    boolint = -1
    call readHDF5int(boolint,"/full_output",filename)
    write(*,"(A,I4)") "    full_output  = ", boolint
    if (boolint==1) then
       rt%full_output = .TRUE.
    else if (boolint==0) then
       rt%full_output = .FALSE.
    else
       write(*,*) " [RTLIB] Wrong value for full_output. Exiting. "
    end if


    ! ### Hardcode implicit solver to be FALSE for now. ###
    rt%implicit_solver = .FALSE.
    ! boolint = -1
    ! call readHDF5int(boolint,"/implicit_solver",filename)
    ! write(*,"(A,I4)") "    implicit_solver  = ", boolint
    ! if (boolint==1) then
    !    rt%implicit_solver = .TRUE.
    ! else if (boolint==0) then
    !    rt%implicit_solver = .FALSE.
    ! else
    !    write(*,*) " [RTLIB] Wrong value for implicit_solver. Exiting. "
    ! end if
    
    call readHDF5int(rt%number_of_eig,"/number_of_eig",filename)
    if (rt%number_of_eig<0) then
       write(*,"(A)") "    Number_of_eig  = ALL " 
    else
       write(*,"(A,I4)") "    Number_of_eig  = ", rt%number_of_eig
    end if

    boolint = -1
    call readHDF5int(boolint,"/compute_absorption",filename)
    if (boolint==1) then
       rt%compute_absorption = .TRUE.
    else if (boolint==0) then
       rt%compute_absorption = .FALSE.
    else
       write(*,*) " [RTLIB] Wrong value for compute_absorption. Exiting. "
       stop
    end if
    write(*,"(A,I4)") "    Compute_Absorption  = ", boolint
    boolint = -1
    call readHDF5int(boolint,"/compute_emission",filename)
    write(*,"(A,I4)") "    Compute_Emission  = ", boolint
    if (boolint==1) then
       rt%compute_emission = .TRUE.
    else if (boolint==0) then
       rt%compute_emission = .FALSE.
    else
       write(*,*) " [RTLIB] Wrong value for compute_emission. Exiting. "
       stop
    end if
    boolint = -1
    call readHDF5int(boolint,"/only_nearest_neighbors",filename)
    write(*,"(A,I4)") "    Only_nearest_neighbors  = ", boolint
    if (boolint==1) then
       rt%only_nearest_neighbors = .TRUE.
    else if (boolint==0) then
       rt%only_nearest_neighbors = .FALSE.
    else
       write(*,*) " [RTLIB] Wrong value for only_nearest_neighbors. Exiting. "
       stop
    end if

    
    if (rt%Nparticle>rt%Nmers) then
       write(*,*) " ERROR: requested Nparticle basis set is meaningless for Nmers = ", rt%Nmers
       stop
    end if
    if (rt%Nparticle>3) then
       write(*,*) " ERROR: requested Nparticle basis set is too big (not implemented yet, 3-particle at most)"
       stop
    end if
    if (rt%Nparticle<1) then
       write(*,*) " ERROR: need Nparticle=1 at least"
       stop
    end if


    write(*,"(A)") " "
    allocate(rt%dipole(3,rt%Nmers),stat=ierr); call chkmerr(ierr,info="dipole")
    allocate(rt%position(3,rt%Nmers),stat=ierr); call chkmerr(ierr,info="position")
    allocate(rt%couplings(rt%Nmers,rt%Nmers),stat=ierr); call chkmerr(ierr,info="couplings")  
          
    call readHDF5dataset2D(rt%dipole,[3,rt%Nmers],"/magnitude",filename)
    call readHDF5dataset2D(rt%position,[3,rt%Nmers],"/positions",filename)
    rt%position = ang_to_bohr(rt%position)
    write(*,"(A)") "            * Positions, a.u. *    "
    do i=1,rt%Nmers
       write(*,"(A,I3,A,3D20.6)") " Idx: ", i, "  =  ", rt%position(:,i)
    end do
    write(*,"(A)") "        * Transition dipoles, a.u. *    "
    do i=1,rt%Nmers
       write(*,"(A,I3,A,3D20.6)") " Idx: ", i, "  =  ", rt%dipole(:,i)
    end do
    do i=1,rt%Nmers
       do j=1,rt%Nmers
          if (rt%only_nearest_neighbors) then
             if (abs(i-j)==1) then
                rt%couplings(i,j) = dipole_coupling(i,j)
             else
                rt%couplings(i,j) = 0.0d0
             end if
          else
             rt%couplings(i,j) = dipole_coupling(i,j)
          end if
       end do
    end do
    write(*,"(A)") "        * Dipole-dipole couplings, a.u. *    "
    do i=1,rt%Nmers
       write(*,"(A)",advance="no") " "
       do j=1,rt%Nmers
          write(*,"(F20.8)",advance="no") rt%couplings(i,j)
       end do
       write(*,"(A)") " "
    end do

    write(*,"(A)") "        * Dipole-dipole couplings, rcm *    "
    do i=1,rt%Nmers
       write(*,"(A)",advance="no") " "
       do j=1,rt%Nmers
          write(*,"(F20.8)",advance="no") ha_to_rcm(rt%couplings(i,j))
       end do
       write(*,"(A)") " "
    end do

    write(*,"(A)") "          * Modes (rcm) and HRs *    "
    allocate(rt%modes(rt%Nmodes),stat=ierr); call chkmerr(ierr,info="modes")
    allocate(rt%HRs(rt%Nmodes),stat=ierr); call chkmerr(ierr,info="HRs")
    call readHDF5dataset1D(rt%modes,rt%Nmodes,"/modes",filename)
    call readHDF5dataset1D(rt%HRs,rt%Nmodes,"/HRs",filename)
    write(*,"(A)",advance="no") "    Modes  = "
    do i=1,rt%Nmodes
       write(*,"(F12.6)",advance="no") rt%modes(i)
    end do
    write(*,"(A)") " "
    write(*,"(A)",advance="no") "    HRs    = "
    do i=1,rt%Nmodes
       write(*,"(F12.6)",advance="no") rt%HRs(i)
    end do
    write(*,"(A)") " "
    rt%modes = rcm_to_ha(rt%modes)
    
    allocate(rt%E00(rt%Nmers),stat=ierr); call chkmerr(ierr,info="E00")
    call readHDF5dataset1D(rt%E00,rt%Nmers,"/E0",filename)
    write(*,"(A)") " "
    write(*,"(A)",advance="no") "    E00    = "
    do i=1,rt%Nmers
       write(*,"(F12.6)",advance="no") rt%E00(i)
    end do
    write(*,"(A)") " "
    rt%E00 = ev_to_ha(rt%E00)

    allocate(rt%Temp(rt%Ntemp),stat=ierr); call chkmerr(ierr,info="Temp")
    call readHDF5dataset1D(rt%Temp,rt%Ntemp,"/T",filename)
    write(*,"(A)") "            * Temperatures *     "
    write(*,"(A)",advance="no") " "
    do i=1,rt%Ntemp
       write(*,"(F12.4)",advance="no") rt%Temp(i)
    end do
    write(*,"(A)") " "
    write(*,*) " ======================================================== "
    isInitialized=.TRUE.
  end subroutine initialize_rt


  ! # Compute dipole-dipole coupling between monomers A and B
  pure real(kind=dpk) function dipole_coupling(A,B)
    use blas95
    use f95_precision
    integer, intent(in) :: A, B
    real(kind=dpk), dimension(3) :: rAB
    if (A==B) then
       dipole_coupling = 0.0d00
    else
       rAB(:) = rt%position(:,B) - rt%position(:,A)
       dipole_coupling =(4.0d00*pi/(nrm2(rAB)**3))*&
            (&
            dot(rt%dipole(:,A),rt%dipole(:,B)) - &
            3.0d00*(dot(rt%dipole(:,A),rAB) * dot(rt%dipole(:,B),rAB))/nrm2(rAB)**2)
            
    end if
  end function dipole_coupling


  
end module rtlib
