module utils
  use ieee_arithmetic
  use types
  implicit none

  public :: chkmerr, chkhdferr, chknum
  public :: bohr_to_ang, ang_to_bohr
  public :: ha_to_ev, ev_to_ha, ha_to_rcm, rcm_to_ha
  public :: fcOverlapIntegral, laguerre_polynomial, fac, k1delta
  private
  
contains

    
  pure integer(kind=k1) function k1delta(i,j)
    integer(kind=k1), intent(in) :: i, j
    if (i==j) then
       k1delta = 1_k1
    else
       k1delta = 0_k1
    end if
  end function k1delta




  pure elemental integer(kind=8) function fac(x)
    integer(kind=k1), intent(in) :: x
    integer(kind=k1) :: i
    fac = 1
    if (x==0) then
       return
    else
       do i=0,x-1
          fac = fac*(x-i)
       end do
    end if
  end function fac
  

  pure recursive real(kind=dp) function laguerre_polynomial(k,a,x)
    real(kind=dp),  intent(in) :: x
    integer(kind=k1), intent(in) :: k, a
    if (k==0) then
       laguerre_polynomial = 1.0d00
    else if (k==1) then
       laguerre_polynomial = 1.0d00 + a - x
    else if (k>1) then
       laguerre_polynomial = (1.0d00/(k))*(&
            (2_k1*(k-1)+1_k1+a-x)*laguerre_polynomial(k-1_k1,a,x)-(k-1_k1+a)*laguerre_polynomial(k-2_k1,a,x))
    end if
    
  end function laguerre_polynomial
  


    !> **fcOverlapIntegral** function computes the Franck-Condon (FC) amplitude
  !! for transitions between vibGround and vibExcited harmonic
  !! oscillator states. FC amplitude is computed according to eq. 2.105
  !! from V. May, O. Kuhn "Charge and Energy Transfer Dynamics in
  !! Molecular Systems", 2011.
  !! @param vibExcited index of the excited state
  !! @param vibGround index of the ground state
  !! @param HR Huang-Rhys factor for the transition
  !! 
  !! @return Value of the Franck-Condon amplitude: \f$<vibExcited|vibGround>\f$

  pure real(kind=dp) elemental function fcOverlapIntegral(vibGround,vibExcited,HR)
    integer(kind=k1), intent(in) :: vibExcited, vibGround
    real(kind=dp), intent(in) :: HR
    real(kind=dp) :: HR_in
    integer(kind=k1) :: M, N
    real(kind=dp) :: dg, fctmp

    fcOverlapIntegral= 0.0d00
    ! if (vibExcited==0) then
    !    fcOverlapIntegral = sqrt(1.0/(1.0+HR))
    ! else if (vibExcited==1) then
    !    fcOverlapIntegral = -sqrt(HR/(1.0+HR))
    ! end if
    
    !> Simplified formula for FC computation
    !! when vibGround == 0.
    !!

    ! if (vibGround==0) then
    !    fcOverlapIntegral = (-1.0)**vibExcited * exp(-HR/2.0)*(HR**(vibExcited/2.0)/(fac(vibExcited)**0.5))
    !    !(-1.0)**vibExcited * exp(-HR/2.0)*(HR**(vibExcited/2.0)/(fac(vibExcited)**0.5))
    !    return
    ! end if

    
    !> Complete formula for FC amplitude computation
    !! based on the ref. from above.
    if (vibGround<=vibExcited) then
       fcOverlapIntegral = sqrt(fac(vibGround)/(1.0d0*fac(vibExcited)))*exp(-HR/2.0d00)*(-sqrt(HR))**(vibExcited-vibGround)*&
            laguerre_polynomial(vibGround,vibExcited-vibGround,HR)
    else
       fcOverlapIntegral = (-1.0d00)**(vibExcited-vibGround)*&
            sqrt(fac(vibExcited)/(1.0d0*fac(vibGround)))*exp(-HR/2.0d00)*(-sqrt(HR))**(vibGround-vibExcited)*&
            laguerre_polynomial(vibExcited,vibGround-vibExcited,HR)
!       fcOverlapIntegral = (-1.0d00)**(vibExcited-vibGround)*fcOverlapIntegral
    end if
    
    
    ! do M=0, vibGround
    !    do N=0, vibExcited
    !       ! fctmp may as well be a NaN, so we first compute it, and then check it.
    !       if (k1delta(vibExcited-N,vibGround-M)==1) then
    !          fctmp = (((-1.0d00)**N * ((2.0*HR_in)**0.5d00)**(M+N))/(1.0d00*fac(M)*fac(N)))&
    !               *((1.0d00*fac(vibExcited)*fac(vibGround))/(1.0d00*fac(vibExcited-N)*fac(vibGround-M)))**0.5d00
    !       else
    !          fctmp = 0.0d00
    !          continue
    !       end if
    !       if (ieee_is_normal(fctmp)) then
    !          fcOverlapIntegral = fcOverlapIntegral + fctmp
    !       else ! If we're getting a NaN, we're not adding it to the sum, just assume that it's too small to be added.
    !          continue
    !       end if
    !    end do
    ! end do
    ! fcOverlapIntegral = fcOverlapIntegral * exp(-1.0d00*HR_in)
!    return
  end function fcOverlapIntegral

  
  pure elemental real(kind=dp) function ha_to_rcm(x)
    real(kind=dp), intent(in) :: x
    ha_to_rcm = x*219474.631370515D00
  end function ha_to_rcm

  pure elemental real(kind=dp) function rcm_to_ha(x)
    real(kind=dp), intent(in) :: x
    rcm_to_ha = x/219474.631370515D00
  end function rcm_to_ha



  pure elemental real(kind=dp) function bohr_to_ang(x)
    implicit none
    real(kind=dp), intent(in) :: x
    bohr_to_ang = x*0.5291772109217
  end function bohr_to_ang

  pure elemental real(kind=dp) function ang_to_bohr(x)
    implicit none
    real(kind=dp), intent(in) :: x
    ang_to_bohr = x/0.5291772109217
  end function ang_to_bohr


  pure elemental real(kind=dp) function ha_to_ev(x)
    implicit none
    real(kind=dp), intent(in) :: x
    ha_to_ev = x*27.2113850560
  end function ha_to_ev

  pure elemental real(kind=dp) function ev_to_ha(x)
    implicit none
    real(kind=dp), intent(in) :: x
    ev_to_ha = x/27.2113850560
  end function ev_to_ha


  
  subroutine chkmerr(ierr, info)
    integer, intent(in) :: ierr
    character(len=*), intent(in), optional :: info
    if (ierr.ne.0) then
       write(*,"(A, I4)") " ERROR: memory allocation failed with the code ierr=", ierr
       if (present(info)) then
          write(*,*) " Info: ", info
       end if
       stop
    end if
  end subroutine chkmerr

  subroutine chkhdferr(ierr, info)
    integer, intent(in) :: ierr
    character(len=*), intent(in), optional :: info
    if ((ierr).ne.0) then
       write(*,*) "ERROR: call to HDF5 lib failed with the code ", ierr
       if (present(info)) then
          write(*,*) " Info: ", info
       end if
       
       stop
    end if
  end subroutine chkhdferr

  subroutine chknum(x,info)
    real(kind=dp), intent(in) :: x
    character(len=*), intent(in), optional :: info
    if (.not.(ieee_is_normal(x))) then
       if (ieee_class(x)==ieee_positive_denormal) then
          if (x>1.0d-10) then
             write(*,*) "ERROR: floating point exception. ", x
             if (present(info)) then
                write(*,*) " Info: ", info
             end if
             stop
          end if
       else if (ieee_class(x)==ieee_negative_denormal) then
          if (x<-1.0d-10) then
             write(*,*) "ERROR: floating point exception. ", x
             if (present(info)) then
                write(*,*) " Info: ", info
             end if
             stop
          end if
       else
          write(*,*) "ERROR: floating point exception ",x
          if (present(info)) then
             write(*,*) " Info: ", info
          end if
          stop
       end if
    end if
  end subroutine chknum

end module utils
