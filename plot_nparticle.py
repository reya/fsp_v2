#coding: utf8
###############################################
import sys
import argparse
### Plotting tools ###
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
### HDF5 I/O ###
import h5py
### NumPy module ###
import numpy as np

### External Fortran module to speed-up calculations ###
from lorentzian_profile import lorentzian_profile
###############################################



### Helper function to convert centimeters to inches ###
def cm_to_in(x):
    return x*0.393701


def plot_stuff(filename,spectrum_index,xmin,xmax):
    f = h5py.File(filename,'r')
    sns.set_style("white")
    matplotlib.rcParams.update({"font.size":10})
    ####################################
    ### Physial size of the figure   ###
    ### Set up something meaningful  ###
    ###                              ###
    fig = plt.figure(figsize=[cm_to_in(15),cm_to_in(8)])
    ###                              ###
    ####################################
    
    #sns.set_palette(sns.color_palette('husl'))
    try:
        c_1p = f.get('/c_1p')
        c_1p = np.array(c_1p,dtype=np.float64)
    except:
        print " ERROR: Can't get c_1p"
    try:
        c_2p = f.get('/c_2p')
        c_2p = np.array(c_2p,dtype=np.float64)
    except:
        print " ERROR: Can't get c_2p"
    try:
        c_3p = f.get('/c_3p')
        c_3p = np.array(c_3p,dtype=np.float64)
    except:
        print " ERROR: Can't get c_3p"
    
    try:
        Erange = f.get('/w')
        Erange = np.array(Erange,dtype=np.float64)
    except:
        print " ERROR: Can't get Erange"
        
    #########################################################
    ### IMPORTANT PART: PUT E00 and omega_vib as they are ###
    ###                 in order to get correct graphs    ###
    E00 = 24196.63; omega_vib = 1500;
    ###                                                   ###
    #########################################################
    
    # Subtract E00 and divide by omega_vib
    Erange = (Erange*219474.631370515-E00)/omega_vib

    ### Plot the graph ###
    #    plt.plot(Erange,c_1p,linewidth=1.5,label='c_1p') #,label=str(int(key_splitted[1])))
    plt.plot(Erange,c_1p)
    plt.plot(Erange,c_2p)
    try:
        plt.plot(Erange,c_3p)
    except:
        pass
        # Set limits
    # xlim_min = 0.0
    # for xi, yi in zip(Erange,y):
    #     if yi>0.01:
    #         xlim_min=xi - 0.5
    #         break
    # xlim_max = np.max(Erange)+0.5
    # if (xmin is not None):
    #     xlim_min = xmin
    # if (xmax is not None):
    #     xlim_max = xmax
        
    # plt.xlim([xlim_min,xlim_max])
    # plt.ylim([0,1.05])
    # Set labels
    plt.xlabel(r"$(\omega-\Omega_{ge})/\omega_{vib}$")
    plt.ylabel(u"Something, a.u.")

    
JOB_NAME="TETRAMER_emission"
#JOB_NAME="TRIMER_emission"
if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',help="HDF5 file to read")
    parser.add_argument('--title',help="Plot title")
    parser.add_argument('--output',help="Output file prefix")
    parser.add_argument('--idx',help="Number of spectrum",type=int)
    parser.add_argument('--xmin',help="X limit (min)",type=float,default=None)
    parser.add_argument('--xmax',help="X limit (max)",type=float,default=None)
    args = parser.parse_args()
    plot_stuff(filename=args.filename,spectrum_index=args.idx,xmin=args.xmin,xmax=args.xmax)
    #plot_stuff(filename="emission_TRIMER.h5")
    #########################
    ### Do you want grid? ###
    ### Uncomment this:   ###
    plt.grid()
    ###                   ###
    #########################

    ############################
    ### Write down the title ###
    ###                      ###
    #plt.title("Trimer G7E7. "+r"$J=2000$ cm$^{-1}$, $\omega_{vib}=1500$ cm$^{-1}$, $FWHM=250$ cm$^{-1}$ ", y=1.02,size=10)
    plt.title(args.title, y=1.02,size=10)
    ###                      ###
    ############################

    # Set up legend
    plt.tight_layout()
    plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
    # Save a .png figure with DPI=600
    plt.savefig(args.output+".png",format="png",dpi=600)
    # Save an .eps figure
    plt.savefig(args.output+".eps",format="eps")
    plt.savefig("output.png",format="png",dpi=300)
    
        
    
    
