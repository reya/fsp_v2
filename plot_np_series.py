#coding: utf8
###############################################
import sys
import os
import argparse
### Plotting tools ###
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
### HDF5 I/O ###
import h5py
### NumPy module ###
import numpy as np


### External Fortran module to speed-up calculations ###
from lorentzian_profile import lorentzian_profile
###############################################



### Helper function to convert centimeters to inches ###
def cm_to_in(x):
    return x*0.393701


def walk_through_files(path):
    for subdir, dirs, files in os.walk(path):
        for file in files:
            yield os.path.join(subdir, file)

def prepare_plot():
    sns.set_style("ticks",{'xtick.major.size': 5.0,
                           'xtick.direction': u'inout',
                           'ytick.direction': u'inout',
                           'zorder':1,
                           'ytick.major.size': 5.0})
    matplotlib.rcParams.update({"font.size":10})
#    ax = plt.gca()
#    plt.setp(ax.xticks, zorder=100)
    ####################################
    ### Physial size of the figure   ###
    ### Set up something meaningful  ###
    ###                              ###
    #fig = plt.figure(figsize=[cm_to_in(15),cm_to_in(8)])
    fig = plt.figure(figsize=[cm_to_in(12),cm_to_in(8)])
    ###                              ###
    ####################################


def plot_from_file(filename,xmin,xmax):
    f = h5py.File(filename,'r')
    #sns.set_palette(sns.color_palette('husl'))
    try:
        c_1p = f.get('/c_1p')
        c_1p = np.array(c_1p,dtype=np.float64)
    except:
        print " ERROR: Can't get c_1p"
    try:
        c_2p = f.get('/c_2p')
        c_2p = np.array(c_2p,dtype=np.float64)
    except:
        print " ERROR: Can't get c_2p"
    try:
        c_3p = f.get('/c_3p')
        c_3p = np.array(c_3p,dtype=np.float64)
    except:
        print " ERROR: Can't get c_3p"
    
    try:
        Erange = f.get('/w')
        Erange = np.array(Erange,dtype=np.float64)
    except:
        print " ERROR: Can't get Erange"
        
    #########################################################
    ### IMPORTANT PART: PUT E00 and omega_vib as they are ###
    ###                 in order to get correct graphs    ###
    E00 = 24196.63; omega_vib = 1500;
    ###                                                   ###
    #########################################################
    
    # Subtract E00 and divide by omega_vib
    Erange = (Erange*219474.631370515-E00)/omega_vib
    
    ### Plot the graph ###
    #    plt.plot(Erange,c_1p,linewidth=1.5,label='c_1p') #,label=str(int(key_splitted[1])))
    # Get first 10% 2-p occurence
    try:
        for ci,ei in zip(c_2p,Erange):
            if (ci>=0.05):
                print " First 2p>=5% occurence at ",ei," c_2p = ",ci
                list_2p.append((filename,ci,ei))
                break
    except:
        print "ERROR: Failed to get 2p contribution. Exiting."
        sys.exit(-1)
        
    # Get first 10% 3-p occurence
    try:
        for ci,ei in zip(c_3p,Erange):
            if (ci>=0.05):
                print " First 3p>=5% occurence at ",ei," c_3p = ",ci
                list_3p.append((filename,ci,ei))
                break
    except:
        print "ERROR: Failed to get 3p contribution. Exiting."
        
    plt.plot(Erange,c_1p,label="1-p")
#    plt.fill_between(Erange, 0, c_1p)
    
    # change the edge color (bluish and transparentish) and thickness
    try:
        plt.plot(Erange,c_2p,label="2-p")
        
#        plt.fill_between(Erange, 0, c_2p)
    except:
        print "No 2-p components"
    try:
        plt.plot(Erange,c_3p,label="3-p")
#        plt.fill_between(Erange, 0, c_3p)
    except:
        print "No 3-p components"

        # Set limits
    # xlim_min = 0.0
    # for xi, yi in zip(Erange,y):
    #     if yi>0.01:
    #         xlim_min=xi - 0.5
    #         break
    # xlim_max = np.max(Erange)+0.5
    # if (xmin is not None):
    #     xlim_min = xmin
    # if (xmax is not None):
    #     xlim_max = xmax
        
    # plt.xlim([xlim_min,xlim_max])
    # plt.ylim([0,1.05])
    # Set labels
    #    plt.ylim([1.0e-6,1.0])
    if (xmin is None):
        xmin = min(Erange)-0.5
    if (xmax is None):
        xmax = max(Erange)+0.5
    plt.xlim([xmin,xmax])

    plt.xlabel(r"$(\omega-\Omega_{ge})/\omega_{vib}$")
    plt.ylabel(u"N-particle contributions, a.u.")


list_3p = []
list_2p = []

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',help="HDF5 file to read")
    parser.add_argument('--title',help="Plot title")
    parser.add_argument('--output',help="Output file prefix")
    parser.add_argument('--xmin',help="X limit (min)",type=float,default=None)
    parser.add_argument('--xmax',help="X limit (max)",type=float,default=None)
    parser.add_argument('--path',help="Path to walk through")
    legend_parser = parser.add_mutually_exclusive_group(required=False)
    legend_parser.add_argument('--legend',help="Plot legend",dest="legend",action='store_true')
    legend_parser.add_argument('--no-legend',help="Don't plot legend",dest="legend",action='store_false')
    parser.set_defaults(legend=False)
    xlabel_parser = parser.add_mutually_exclusive_group(required=False)
    xlabel_parser.add_argument('--xlabel',help="Plot xlabel",dest="xlabel",action='store_true')
    xlabel_parser.add_argument('--no-xlabel',help="Don't plot xlabel",dest="xlabel",action='store_false')
    parser.set_defaults(xlabel=False)
    ylabel_parser = parser.add_mutually_exclusive_group(required=False)
    ylabel_parser.add_argument('--ylabel',help="Plot ylabel",dest="ylabel",action='store_true')
    ylabel_parser.add_argument('--no-ylabel',help="Don't plot ylabel",dest="ylabel",action='store_false')
    parser.set_defaults(ylabel=False)

    args = parser.parse_args()
    gen = walk_through_files(path=args.path)
    for el in gen:
        if args.filename in el:
            print " * File: ", el
            output_fname_prefix = el.replace("/","_")
            prepare_plot()
            plot_from_file(filename=el,xmin=args.xmin,xmax=args.xmax)
            plt.tight_layout()
            if args.legend:
                plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
            if args.xlabel:
                plt.xlabel(u"Energy, (E-E$_{00})/\omega_{vib}$")
            if args.ylabel:
                plt.ylabel(u"Intensity, a.u.")

            # Save a .png figure with DPI=600
            plt.savefig(output_fname_prefix+args.output+".png",format="png",dpi=600)
            # Save an .eps figure
            plt.savefig(output_fname_prefix+args.output+".eps",format="eps")

    for el in sorted(list_2p):
        print "[2p], %s,    %12.6f, %12.6f "%el
        
    for el in sorted(list_3p):
        print "[3p], %s,    %12.6f, %12.6f "%el
    
    # plot_stuff(filename=args.filename,xmin=args.xmin,xmax=args.xmax)
    # #plot_stuff(filename="emission_TRIMER.h5")
    # #########################
    # ### Do you want grid? ###
    # ### Uncomment this:   ###
    # plt.grid()
    # ###                   ###
    # #########################

    # ############################
    # ### Write down the title ###
    # ###                      ###
    # #plt.title("Trimer G7E7. "+r"$J=2000$ cm$^{-1}$, $\omega_{vib}=1500$ cm$^{-1}$, $FWHM=250$ cm$^{-1}$ ", y=1.02,size=10)
    # plt.title(args.title, y=1.02,size=10)
    # ###                      ###
    # ############################

    # # Set up legend
    # plt.tight_layout()
    # plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
    # # Save a .png figure with DPI=600
    # plt.savefig(args.output+".png",format="png",dpi=600)
    # # Save an .eps figure
    # plt.savefig(args.output+".eps",format="eps")
    # plt.savefig("output.png",format="png",dpi=300)
    
        
    
    
