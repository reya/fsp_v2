#coding: utf8
###############################################
import sys
import os
import argparse
### Plotting tools ###
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.ticker import MaxNLocator
import matplotlib.colors as col
   
import seaborn as sns
### HDF5 I/O ###
import h5py
### NumPy module ###
import numpy as np

### External Fortran module to speed-up calculations ###
from lorentzian_profile import lorentzian_profile
###############################################


def ha_to_rcm(x):
    return x*219474.631370515

def rcm_to_ha(x):
    return x/219474.631370515

def ha_to_ev(x):
    return x*27.2113850560

def ev_to_ha(x):
    return x/27.2113850560

def ev_to_rcm(x):
    return ha_to_rcm(ev_to_ha(x))

def rcm_to_ev(x):
    return ha_to_ev(rcm_to_ha(x))

### Helper function to convert centimeters to inches ###
def cm_to_in(x):
    return x*0.393701

### Generator returning walks inside the path ###
def walk_through_files(path):
    for subdir, dirs, files in os.walk(path):
        for file in files:
            yield os.path.join(subdir, file)

def prepare_plot():
    sns.set_style("ticks",{'xtick.major.size': 5.0,
                           'xtick.direction': u'inout',
                           'ytick.direction': u'inout',
                           'zorder':1,
                           'ytick.major.size': 5.0})
    matplotlib.rcParams.update({"font.size":10})
#    ax = plt.gca()
#    plt.setp(ax.xticks, zorder=100)
    ####################################
    ### Physial size of the figure   ###
    ### Set up something meaningful  ###
    ###                              ###
    #fig = plt.figure(figsize=[cm_to_in(15),cm_to_in(8)])
    fig = plt.figure(figsize=[cm_to_in(10),cm_to_in(10)])
    ###                              ###
    ####################################

###############################################################

def read_from_file(filename,take_iw):
    f = h5py.File(filename,'r')
    for key in f.keys():
        if "ISUM" in key:
            key_fine = False
            if (take_iw==1):
                if ("ISUM_w" in key):
                    key_fine=True
#                    pass
            else:
                if ("ISUM_w" not in key):
                    key_fine=True
                    #pass
            if (key_fine):
                try:
                    print " * Reading key: ", key
                    ISUM = f.get(key)
                    ISUM = np.array(ISUM,dtype=np.float64)
                except:
                    pass        #print " * Skipping key: ", key

    return ISUM

#########################################################    
E00 = 3; omega_vib = 1500;
#########################################################


#JOB_NAME="TRIMER_emission"
T = [1,3.00502512562814,5.01005025125628,7.01507537688442,9.02010050251256,11.0251256281407,13.0301507537688,15.0351758793970,17.0402010050251,19.0452261306533,21.0502512562814,23.0552763819095,25.0603015075377,27.0653266331658,29.0703517587940,31.0753768844221,33.0804020100503,35.0854271356784,37.0904522613065,39.0954773869347,41.1005025125628,43.1055276381910,45.1105527638191,47.1155778894472,49.1206030150754,51.1256281407035,53.1306532663317,55.1356783919598,57.1407035175879,59.1457286432161,61.1507537688442,63.1557788944724,65.1608040201005,67.1658291457286,69.1708542713568,71.1758793969849,73.1809045226131,75.1859296482412,77.1909547738694,79.1959798994975,81.2010050251256,83.2060301507538,85.2110552763819,87.2160804020101,89.2211055276382,91.2261306532663,93.2311557788945,95.2361809045226,97.2412060301508,99.2462311557789,101.251256281407,103.256281407035,105.261306532663,107.266331658291,109.271356783920,111.276381909548,113.281407035176,115.286432160804,117.291457286432,119.296482412060,121.301507537688,123.306532663317,125.311557788945,127.316582914573,129.321608040201,131.326633165829,133.331658291457,135.336683417085,137.341708542714,139.346733668342,141.351758793970,143.356783919598,145.361809045226,147.366834170854,149.371859296482,151.376884422111,153.381909547739,155.386934673367,157.391959798995,159.396984924623,161.402010050251,163.407035175879,165.412060301508,167.417085427136,169.422110552764,171.427135678392,173.432160804020,175.437185929648,177.442211055276,179.447236180905,181.452261306533,183.457286432161,185.462311557789,187.467336683417,189.472361809045,191.477386934673,193.482412060302,195.487437185930,197.492462311558,199.497487437186,201.502512562814,203.507537688442,205.512562814070,207.517587939699,209.522613065327,211.527638190955,213.532663316583,215.537688442211,217.542713567839,219.547738693467,221.552763819095,223.557788944724,225.562814070352,227.567839195980,229.572864321608,231.577889447236,233.582914572864,235.587939698492,237.592964824121,239.597989949749,241.603015075377,243.608040201005,245.613065326633,247.618090452261,249.623115577889,251.628140703518,253.633165829146,255.638190954774,257.643216080402,259.648241206030,261.653266331658,263.658291457286,265.663316582915,267.668341708543,269.673366834171,271.678391959799,273.683417085427,275.688442211055,277.693467336683,279.698492462312,281.703517587940,283.708542713568,285.713567839196,287.718592964824,289.723618090452,291.728643216080,293.733668341709,295.738693467337,297.743718592965,299.748743718593,301.753768844221,303.758793969849,305.763819095477,307.768844221106,309.773869346734,311.778894472362,313.783919597990,315.788944723618,317.793969849246,319.798994974874,321.804020100503,323.809045226131,325.814070351759,327.819095477387,329.824120603015,331.829145728643,333.834170854271,335.839195979900,337.844221105528,339.849246231156,341.854271356784,343.859296482412,345.864321608040,347.869346733668,349.874371859297,351.879396984925,353.884422110553,355.889447236181,357.894472361809,359.899497487437,361.904522613065,363.909547738693,365.914572864322,367.919597989950,369.924623115578,371.929648241206,373.934673366834,375.939698492462,377.944723618090,379.949748743719,381.954773869347,383.959798994975,385.964824120603,387.969849246231,389.974874371859,391.979899497487,393.984924623116,395.989949748744,397.994974874372,400]

J=[-3000,-2948.71794871795,-2897.43589743590,-2846.15384615385,-2794.87179487180,-2743.58974358974,-2692.30769230769,-2641.02564102564,-2589.74358974359,-2538.46153846154,-2487.17948717949,-2435.89743589744,-2384.61538461538,-2333.33333333333,-2282.05128205128,-2230.76923076923,-2179.48717948718,-2128.20512820513,-2076.92307692308,-2025.64102564103,-1974.35897435897,-1923.07692307692,-1871.79487179487,-1820.51282051282,-1769.23076923077,-1717.94871794872,-1666.66666666667,-1615.38461538462,-1564.10256410256,-1512.82051282051,-1461.53846153846,-1410.25641025641,-1358.97435897436,-1307.69230769231,-1256.41025641026,-1205.12820512821,-1153.84615384615,-1102.56410256410,-1051.28205128205,-1000,-900,-852.105263157895,-804.210526315790,-756.315789473684,-708.421052631579,-660.526315789474,-612.631578947368,-564.736842105263,-516.842105263158,-468.947368421053,-421.052631578947,-373.157894736842,-325.263157894737,-277.368421052632,-229.473684210526,-181.578947368421,-133.684210526316,-85.7894736842105,-37.8947368421053,10,0,10,56.8421052631579,103.684210526316,150.526315789474,197.368421052632,244.210526315789,291.052631578947,337.894736842105,384.736842105263,431.578947368421,478.421052631579,525.263157894737,572.105263157895,618.947368421053,665.789473684211,712.631578947368,759.473684210526,806.315789473684,853.157894736842,900,1000,1051.28205128205,1102.56410256410,1153.84615384615,1205.12820512821,1256.41025641026,1307.69230769231,1358.97435897436,1410.25641025641,1461.53846153846,1512.82051282051,1564.10256410256,1615.38461538462,1666.66666666667,1717.94871794872,1769.23076923077,1820.51282051282,1871.79487179487,1923.07692307692,1974.35897435897,2025.64102564103,2076.92307692308,2128.20512820513,2179.48717948718,2230.76923076923,2282.05128205128,2333.33333333333,2384.61538461538,2435.89743589744,2487.17948717949,2538.46153846154,2589.74358974359,2641.02564102564,2692.30769230769,2743.58974358974,2794.87179487180,2846.15384615385,2897.43589743590,2948.71794871795,3000]

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',help="HDF5 file to read")
    parser.add_argument('--path',help="Path to walk through")
    parser.add_argument('--title',help="Plot title",default=None)
    parser.add_argument('--output',help="Output file prefix")
    legend_parser = parser.add_mutually_exclusive_group(required=False)
    legend_parser.add_argument('--legend',help="Plot legend",dest="legend",action='store_true')
    legend_parser.add_argument('--no-legend',help="Don't plot legend",dest="legend",action='store_false')
    parser.set_defaults(legend=False)
    xlabel_parser = parser.add_mutually_exclusive_group(required=False)
    xlabel_parser.add_argument('--xlabel',help="Plot xlabel",dest="xlabel",action='store_true')
    xlabel_parser.add_argument('--no-xlabel',help="Don't plot xlabel",dest="xlabel",action='store_false')
    parser.set_defaults(xlabel=False)
    ylabel_parser = parser.add_mutually_exclusive_group(required=False)
    ylabel_parser.add_argument('--ylabel',help="Plot ylabel",dest="ylabel",action='store_true')
    ylabel_parser.add_argument('--no-ylabel',help="Don't plot ylabel",dest="ylabel",action='store_false')
    parser.set_defaults(ylabel=False)

    parser.add_argument('--xmin',help="X limit (min)",type=float,default=None)
    parser.add_argument('--xmax',help="X limit (max)",type=float,default=None)
    parser.add_argument('--ymin',help="Y limit (min)",type=float,default=None)
    parser.add_argument('--ymax',help="Y limit (max)",type=float,default=None)
    parser.add_argument('--use_iw',help="Use frequency multiplier",type=int,default=1)
    parser.add_argument('--num_points',help="Number of T points",type=int)
    feature_parser = parser.add_mutually_exclusive_group(required=False)
    feature_parser.add_argument('--plot_yfine', dest='plot_yfine', action='store_true')
    feature_parser.add_argument('--no-plot-yfine', dest='plot_yfine', action='store_false')
    parser.set_defaults(plot_yfine=True)
    # contour_parser = parser.add_mutually_exclusive_group(required=False)
    # contour_parser.add_argument('--contour', dest='contour', action='store_true')
    # contour_parser.add_argument('--pcolormesh', dest='contour', action='store_false')
    # parser.set_defaults(contour=False)
    args = parser.parse_args()
    gen = walk_through_files(path=args.path)
    assert(args.num_points==np.size(T))
    print np.size(J)#, np.size(sorted(gen))
    #assert(np.size(sorted(gen))==np.size(J))
    prepare_plot()
    I = np.zeros([np.size(J),np.size(T)])
    counter = 0
    for el in sorted(gen):
        if args.filename in el:
            print " * File: ", el
            #output_fname_prefix = el.replace("/","_")
            counter = counter + 1
            Ii = read_from_file(filename=el,
                           take_iw=args.use_iw)
            #print " I at ", counter, " = ", Ii
            I[counter-1,:] = Ii
                
            # if args.legend:
            #     plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
            # if args.xlabel:
            #     plt.xlabel(u"Energy, (E-E$_{00})/\omega_{vib}$")
            # if args.ylabel:
            #     plt.ylabel(u"Intensity, a.u.")
            # if (args.title is not None):
            #     plt.title(args.title)

            # plt.tight_layout()
            # # Save a .png figure with DPI=600
            # plt.savefig(output_fname_prefix+args.output+".png",format="png",dpi=600)
            # # Save an .eps figure
            # plt.savefig(output_fname_prefix+args.output+".eps",format="eps")
    if (counter==np.size(J)):
        print " * Count for J works * "
    else:
        print " * ERROR: Count for J failed * "

       #  orange ~380
    cmap=col.ListedColormap(sns.diverging_palette(3,10,n=250))
    cmap=sns.diverging_palette(h_neg=250, h_pos=380, s=99, l=45, n=250, center='light',as_cmap=True)
    X,Y = np.meshgrid(T,J)
    #plt.pcolormesh(X,Y,A,cmap="rainbow",shading='gouraud')

    plt.pcolormesh(Y,X,I,cmap=cmap,shading='gouraud')
    plt.colorbar()
#    if (args.contour):
#        plt.contour(Y,X,I,colors='k',linesi)
    plt.xlabel("Coulomb coupling, cm$^{-1}$")
    plt.ylabel("Temperature, K")
    ax = plt.gca()
    ax.get_xaxis().get_major_formatter().set_useOffset(False)
    #plt.xlim([8980,9005])
    #plt.ylim([7480,7530])
    plt.tight_layout()
#    print " * Saving SVG... "
#    plt.savefig("test2d.svg",format="svg")
    #plt.savefig("2dCuO.eps")
    plt.savefig(args.output+"_2D_JT.png",dpi=600)
    plt.savefig(args.output+"_2D_JT.pdf",dpi=600)
    #    plt.savefig(args.output+"_2D_JT.",dpi=600)
    print "I in the H-agg region: ", "J=",J[-1]," T=",T[-1]," ISUM=",I[-1,-1]
    print " * * * \n DONE \n * * * \n"
    #plot_stuff(filename="emission_TRIMER.h5")
    #########################
    ### Do you want grid? ###
    ### Uncomment this:   ###
    #plt.grid()
    ###                   ###
    #########################

    ############################
    ### Write down the title ###
    ###                      ###
    #plt.title("Trimer G7E7. "+r"$J=2000$ cm$^{-1}$, $\omega_{vib}=1500$ cm$^{-1}$, $FWHM=250$ cm$^{-1}$ ", y=1.02,size=10)
    #plt.title(args.title, y=1.02,size=10)
    ###                      ###
    ############################

    # Set up legend
    # plt.tight_layout()
    # plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
    # # Save a .png figure with DPI=600
    # plt.savefig(args.output+".png",format="png",dpi=600)
    # # Save an .eps figure
    # plt.savefig(args.output+".eps",format="eps")
    # plt.savefig("output.png",format="png",dpi=300)
    
        
    
    
