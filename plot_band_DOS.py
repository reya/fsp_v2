#coding: utf8
###############################################
import sys
import os
import argparse
### Plotting tools ###
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns
### HDF5 I/O ###
import h5py
### NumPy module ###
import numpy as np

from scipy.interpolate import interp1d

### Helper function to convert centimeters to inches ###
def cm_to_in(x):
    return x*0.393701


def walk_through_files(path):
    for subdir, dirs, files in os.walk(path):
        for file in files:
            yield os.path.join(subdir, file)

def prepare_plot():
    sns.set_style("ticks",{'xtick.major.size': 5.0,
                           'xtick.direction': u'inout',
                           'ytick.direction': u'inout',
                           'zorder':1,
                           'ytick.major.size': 5.0})
    matplotlib.rcParams.update({"font.size":10})
#    ax = plt.gca()
#    plt.setp(ax.xticks, zorder=100)
    ####################################
    ### Physial size of the figure   ###
    ### Set up something meaningful  ###
    ###                              ###
    #fig = plt.figure(figsize=[cm_to_in(15),cm_to_in(8)])
    fig = plt.figure(figsize=[cm_to_in(10),cm_to_in(10)])
    ###                              ###
    ####################################


def read_eigenstates(filename,dnorm):
    f = h5py.File(filename,'r')
    #sns.set_palette(sns.color_palette('husl'))
    try:
        if (dnorm):
            w = f.get('/dnorm')
            w = np.array(w,dtype=np.float64)
        else:
            w = f.get('/w')
            w = np.array(w,dtype=np.float64)
    except:
        print " ERROR: Can't get w"
        
    # Subtract E00 and divide by omega_vib
    #Erange = (w*219474.631370515-E00)/omega_vib
    if (dnorm):
        Erange = w
    else:
        Erange = (w*219474.631370515-E00)/omega_vib
    energies.append(Erange)


#########################################################
### IMPORTANT PART: PUT E00 and omega_vib as they are ###
###                 in order to get correct graphs    ###
E00 = 24196.63; omega_vib = 1500.0;
###                                                   ###
#########################################################
energies = []
couplings = np.array([-3000,-2948.71794871795,-2897.43589743590,-2846.15384615385,-2794.87179487180,-2743.58974358974,-2692.30769230769,-2641.02564102564,-2589.74358974359,-2538.46153846154,-2487.17948717949,-2435.89743589744,-2384.61538461538,-2333.33333333333,-2282.05128205128,-2230.76923076923,-2179.48717948718,-2128.20512820513,-2076.92307692308,-2025.64102564103,-1974.35897435897,-1923.07692307692,-1871.79487179487,-1820.51282051282,-1769.23076923077,-1717.94871794872,-1666.66666666667,-1615.38461538462,-1564.10256410256,-1512.82051282051,-1461.53846153846,-1410.25641025641,-1358.97435897436,-1307.69230769231,-1256.41025641026,-1205.12820512821,-1153.84615384615,-1102.56410256410,-1051.28205128205,-1000,-900,-852.105263157895,-804.210526315790,-756.315789473684,-708.421052631579,-660.526315789474,-612.631578947368,-564.736842105263,-516.842105263158,-468.947368421053,-421.052631578947,-373.157894736842,-325.263157894737,-277.368421052632,-229.473684210526,-181.578947368421,-133.684210526316,-85.7894736842105,-37.8947368421053,10,0,10,56.8421052631579,103.684210526316,150.526315789474,197.368421052632,244.210526315789,291.052631578947,337.894736842105,384.736842105263,431.578947368421,478.421052631579,525.263157894737,572.105263157895,618.947368421053,665.789473684211,712.631578947368,759.473684210526,806.315789473684,853.157894736842,900,1000,1051.28205128205,1102.56410256410,1153.84615384615,1205.12820512821,1256.41025641026,1307.69230769231,1358.97435897436,1410.25641025641,1461.53846153846,1512.82051282051,1564.10256410256,1615.38461538462,1666.66666666667,1717.94871794872,1769.23076923077,1820.51282051282,1871.79487179487,1923.07692307692,1974.35897435897,2025.64102564103,2076.92307692308,2128.20512820513,2179.48717948718,2230.76923076923,2282.05128205128,2333.33333333333,2384.61538461538,2435.89743589744,2487.17948717949,2538.46153846154,2589.74358974359,2641.02564102564,2692.30769230769,2743.58974358974,2794.87179487180,2846.15384615385,2897.43589743590,2948.71794871795,3000])/omega_vib


if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',help="HDF5 file to read")
    parser.add_argument('--title',help="Plot title")
    parser.add_argument('--output',help="Output file prefix")
    parser.add_argument('--xmin',help="X limit (min)",type=float,default=None)
    parser.add_argument('--xmax',help="X limit (max)",type=float,default=None)
    parser.add_argument('--ymin',help="Y limit (min)",type=float,default=None)
    parser.add_argument('--ymax',help="Y limit (max)",type=float,default=None)

    parser.add_argument('--path',help="Path to walk through")
    legend_parser = parser.add_mutually_exclusive_group(required=False)
    legend_parser.add_argument('--legend',help="Plot legend",dest="legend",action='store_true')
    legend_parser.add_argument('--no-legend',help="Don't plot legend",dest="legend",action='store_false')
    parser.set_defaults(legend=False)
    xlabel_parser = parser.add_mutually_exclusive_group(required=False)
    xlabel_parser.add_argument('--xlabel',help="Plot xlabel",dest="xlabel",action='store_true')
    xlabel_parser.add_argument('--no-xlabel',help="Don't plot xlabel",dest="xlabel",action='store_false')
    parser.set_defaults(xlabel=False)
    ylabel_parser = parser.add_mutually_exclusive_group(required=False)
    ylabel_parser.add_argument('--ylabel',help="Plot ylabel",dest="ylabel",action='store_true')
    ylabel_parser.add_argument('--no-ylabel',help="Don't plot ylabel",dest="ylabel",action='store_false')
    parser.set_defaults(ylabel=False)
    dnorm_parser = parser.add_mutually_exclusive_group(required=False)
    dnorm_parser.add_argument('--dnorm',help='Plot dipole norm instead of energies',dest='dnorm',action='store_true')
    dnorm_parser.add_argument('--no-dnorm',help="Don't plot dipole norm instead of energies",dest='dnorm',action='store_false')
    parser.set_defaults(dnorm=False)
    args = parser.parse_args()
    gen = sorted(walk_through_files(path=args.path))
    prepare_plot()
    num_of_couplings = 0
    for el in gen:
        if args.filename in el:
            print " * File: ", el
            output_fname_prefix = el.replace("/","_")
            read_eigenstates(filename=el,dnorm=args.dnorm)
            num_of_couplings = num_of_couplings + 1
            #plot_from_file(filename=el,xmin=args.xmin,xmax=args.xmax)
            #plt.tight_layout()
    energies = np.array(energies)
    print energies
    print "problem size: ",np.size(energies,1)
    for i in range(np.size(energies,1)):
        x = couplings
        y = energies[:,i]

    
    if args.xlabel:
        plt.xlabel(u"Coulomb coupling, $J/\omega_{vib}$")
        #plt.xlabel(u"Coulomb coupling, $cm^{-1}$")
    if args.ylabel:
        #plt.ylabel(u"Energy, $(E-E_{00})/\omega_{vib}$")
        if (args.dnorm):
            plt.ylabel(u"Transition dipole magnitude, a.u.")
        else:
            plt.ylabel(u"Energy, $(E-E_{00})/\omega_{vib}$")
    # Save a .png figure with DPI=600
    if (args.xmin is not None) and (args.xmax is not None):
        plt.xlim([args.xmin,args.xmax])
    if (args.ymin is not None) and (args.ymax is not None):
        plt.ylim([args.ymin,args.ymax])

    plt.tight_layout()
    plt.savefig(output_fname_prefix+args.output+".png",format="png",dpi=600)
    # Save an .eps figure
    plt.savefig(output_fname_prefix+args.output+".eps",format="eps")

    # plot_stuff(filename=args.filename,xmin=args.xmin,xmax=args.xmax)
    # #plot_stuff(filename="emission_TRIMER.h5")
    # #########################
    # ### Do you want grid? ###
    # ### Uncomment this:   ###
    # plt.grid()
    # ###                   ###
    # #########################

    # ############################
    # ### Write down the title ###
    # ###                      ###
    # #plt.title("Trimer G7E7. "+r"$J=2000$ cm$^{-1}$, $\omega_{vib}=1500$ cm$^{-1}$, $FWHM=250$ cm$^{-1}$ ", y=1.02,size=10)
    # plt.title(args.title, y=1.02,size=10)
    # ###                      ###
    # ############################

    # # Set up legend
    # plt.tight_layout()
    # plt.legend(loc='best',ncol=1) # ncols gives number of columns for the legend
    # # Save a .png figure with DPI=600
    # plt.savefig(args.output+".png",format="png",dpi=600)
    # # Save an .eps figure
    # plt.savefig(args.output+".eps",format="eps")
    # plt.savefig("output.png",format="png",dpi=300)
    
        
    
    
