module absems
  use types
  use rtlib
  use basis_set
  use utils
  use eigensolver
  use hamiltonian, only: fcOverlapIntegral
  use io
  use omp_lib
  implicit none

  real(kind=dp), parameter ::  kB=3.16681e-6 ! Boltzmann constant in Eh/K

  public :: compute_absorption, compute_emission
  public :: prepare_gs_basis
  private



contains


  subroutine prepare_gs_basis()
    integer :: ground_state_basis_size
    call initialize_gs_basis(gs_basis_size=ground_state_basis_size,compute=.FALSE., print_output=.FALSE.)
    call initialize_gs_basis(gs_basis_size=ground_state_basis_size,compute=.TRUE., print_output=.FALSE.)
  end subroutine prepare_gs_basis


  subroutine compute_absorption()
    real(kind=dp), dimension(gs_basis_set_size) :: gs_energies
    real(kind=dp), dimension(:), allocatable :: intensity
    real(kind=dp) :: intensity_current
    type(dynamic_array_type) :: intensity_dynamic, energy_dynamic
    real(kind=dp), dimension(rt%Ntemp) :: mu_gs, isum_w
    real(kind=dp) :: time_start, time_stop, dipole_sum
    real(kind=dp), dimension(:), allocatable :: overlap_current
    type(transition_type), dimension(:), allocatable :: absorption
    integer :: i, j, k, im, ip, itercount, igs, iems, itemp, ierr, idxcount, total_size
    integer, allocatable, dimension(:) :: from, to
    logical :: file_new
    character(*), parameter :: fname_abs_output="absorption.h5"
    character(*), parameter :: fname_ems_output="emission.h5"
    character(20) :: fname_f
    character(7) :: dsetname

    ! # First, we need to compute energies of the initial states
    gs_energies(:) = 0.0d00 ! Global ground state is the first item
    do i=2,gs_basis_set_size
       do ip=1,gs_basis(i)%np
          do im=1,rt%Nmodes
             ! Since all monomers have the same set of modes, it's fine to sum over vib(im)
             gs_energies(i) = gs_energies(i) + gs_basis(i)%particle(ip)%vib(im)*rt%modes(im)
          end do
       end do
    end do
    write(*,"(A,I4)") " * Computing GS energies for basis set size = ", gs_basis_set_size
    ! do i=1,gs_basis_set_size
    !    write(*,"(A,I4,A,F16.8)") " [GS Energy] ",i, " = ", gs_energies(i)
    ! end do

    ! # Second, we compute chemical potentials for different temperatures
    write (*,"(A)") " -------------------------------------------------- "
    write (*,"(A)") "          Ground state chemical potentials"
    write (*,"(A)") " -------------------------------------------------- "
    do i=1,rt%Ntemp
       mu_gs(i) = compute_chemical_potential(energies=gs_energies,T=rt%Temp(i))
       write(*,"(A,F8.3,A,F20.10)") "  T = ",rt%Temp(i), "  :  ", mu_gs(i)
    end do
    write (*,"(A)") " -------------------------------------------------- "

    total_size = gs_basis_set_size*rt%number_of_eig
    allocate(from(total_size),stat=ierr); call chkmerr(ierr,info="abs:from")
    allocate(to(total_size),stat=ierr); call chkmerr(ierr,info="abs:to")
    allocate(intensity(total_size),stat=ierr); call chkmerr(ierr,info="abs:intensity")
    allocate(overlap_current(basis_set_size),stat=ierr); call chkmerr(ierr,info="abs:overlap_current")
    ! # Third, we compute absorption spectrum and save it somehow. I need a linked list, probalby.

    if (allocated(absorption)) deallocate(absorption)
    allocate(absorption(total_size),stat=ierr); call chkmerr(ierr,info="abs:absorption")
    itercount = 1
!    call cpu_time(time_start)
    time_start = omp_get_wtime()
    idxcount = 0
    intensity(:) = 0.0d0
    from(:) = 0
    to(:)   = 0
    do igs=1,gs_basis_set_size
       do iems=1,rt%number_of_eig
          idxcount = idxcount + 1
          absorption(idxcount)%dipole(:) = 0.0d00
          from(idxcount) = igs
          to(idxcount)   = iems
          !$OMP PARALLEL
          !$OMP DO PRIVATE(k)
          do k=1,basis_set_size
             overlap_current(k) = overlap_ge(igs,k)
             if (abs(overlap_current(k))<1.0d-10) then
                overlap_current(k)=0.0d0
             end if
          end do
          !$OMP END DO
          !$OMP END PARALLEL

          do i=1,3
             dipole_sum = 0.0d0
             !$OMP PARALLEL DO REDUCTION(+:dipole_sum) PRIVATE(k)
             !do concurrent (k=1:basis_set_size)
             do k=1,basis_set_size
                dipole_sum = dipole_sum + &
                     rt%dipole(i,basis(k)%particle(1)%idx) &
                     * eigenvectors(k,iems) * overlap_current(k)
             end do
             !$OMP END PARALLEL DO
             absorption(idxcount)%dipole(i) = dipole_sum
          end do
          absorption(idxcount)%E = eigenvalues(iems)-gs_energies(igs)
       end do
       
       if (((1.0*igs)/gs_basis_set_size)>=0.1*itercount) then
          itercount = itercount + 1
          time_stop = omp_get_wtime() !call cpu_time(time_stop)

          write(*,"(A,F6.2,A,A,F16.4,A)") " Done: ", (100.0*igs)/gs_basis_set_size, "%", "   time  =  ", &
               time_stop-time_start, " s "
          time_start = omp_get_wtime() !call cpu_time(time_start)
       end if
    end do

    if (rt%full_output) then
       call writeHDF5dataset1D_new(data=ha_to_rcm(absorption(:)%E),&
            size_in=total_size,location="/E",filename="absorption.h5")

       call writeHDF5dataset1D_int(data=from,&
            size_in=total_size,location="/from",filename="absorption.h5")

       call writeHDF5dataset1D_int(data=to,&
            size_in=total_size,location="/to",filename="absorption.h5")

       call writeHDF5dataset1D(data=absorption(:)%dipole(1),&
            size_in=total_size,location="/dx",filename="absorption.h5")
       call writeHDF5dataset1D(data=absorption(:)%dipole(2),&
            size_in=total_size,location="/dy",filename="absorption.h5")
       call writeHDF5dataset1D(data=absorption(:)%dipole(3),&
            size_in=total_size,location="/dz",filename="absorption.h5")

    end if
    
    isum_w(:) = 0.0d0
    file_new = .TRUE.
    do itemp=1,rt%Ntemp
       
       intensity(:) = 0.0d0
       absorption(:)%population = 0.0d0
       call intensity_dynamic%initialize(initial_size=total_size, increment = 1000)
       call energy_dynamic%initialize(initial_size=total_size, increment = 1000)
       do idxcount=1,total_size
          absorption(idxcount)%population = bosefun(gs_energies(from(idxcount)),mu_gs(itemp),rt%Temp(itemp))
          intensity_current = absorption(idxcount)%E*absorption(idxcount)%population*sum(absorption(idxcount)%dipole(:)**2)
          if (intensity_current>rt%tiny) then
             call intensity_dynamic%add(intensity_current, from(idxcount), to(idxcount))
             call energy_dynamic%add(absorption(idxcount)%E, from(idxcount), to(idxcount))
          end if
       end do
       call intensity_dynamic%finalize()
       call energy_dynamic%finalize()
       write(fname_f,"(A4,I5.5)") "/I_w",itemp
       if (.not.(rt%full_output).and.(file_new)) then
          call writeHDF5dataset1D_new(data=intensity_dynamic%data,&
               size_in=intensity_dynamic%size,location=fname_f,filename="absorption.h5")
          file_new = .FALSE.
       else
          call writeHDF5dataset1D(data=intensity_dynamic%data,&
               size_in=intensity_dynamic%size,location=fname_f,filename="absorption.h5")
       end if
       
       write(fname_f,"(A4,I5.5)") "/E_w",itemp
       call writeHDF5dataset1D(data=energy_dynamic%data,&
            size_in=energy_dynamic%size,location=fname_f,filename="absorption.h5")
       isum_w(itemp) = sum(intensity_dynamic%data)
       write(fname_f,"(A9,I5.5)") "/from_I_w",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idx,&
            size_in=intensity_dynamic%size,location=fname_f,filename="absorption.h5")
       write(fname_f,"(A7,I5.5)") "/to_I_w",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idy,&
            size_in=intensity_dynamic%size,location=fname_f,filename="absorption.h5")
       call intensity_dynamic%clear()
       call energy_dynamic%clear()
    end do
    call writeHDF5dataset1D(data=isum_w,&
         size_in=rt%Ntemp,location="/ISUM_w",filename="absorption.h5")
    ! ### Dipole moment norm ###
    call intensity_dynamic%initialize(initial_size=total_size, increment = 1000)
    call energy_dynamic%initialize(initial_size=total_size, increment = 1000)
    do idxcount=1,total_size
       intensity_current = sum(absorption(idxcount)%dipole(:)**2)
       if ((intensity_current>rt%tiny).or.(rt%full_output)) then
          call intensity_dynamic%add(intensity_current, from(idxcount), to(idxcount))
          call energy_dynamic%add(absorption(idxcount)%E, from(idxcount), to(idxcount))
       end if
    end do
    call intensity_dynamic%finalize()
    call energy_dynamic%finalize()
    call writeHDF5dataset1D(data=intensity_dynamic%data,&
         size_in=intensity_dynamic%size,location="/dnorm",filename="absorption.h5")
    call writeHDF5dataset1D(data=energy_dynamic%data,&
         size_in=energy_dynamic%size,location="/E_dnorm",filename="absorption.h5")
    call writeHDF5dataset1D_int(data=intensity_dynamic%idx,&
            size_in=intensity_dynamic%size,location="/from_dnorm",filename="absorption.h5")
    call writeHDF5dataset1D_int(data=intensity_dynamic%idy,&
         size_in=intensity_dynamic%size,location="/to_dnorm",filename="absorption.h5")
    call intensity_dynamic%clear()
    call energy_dynamic%clear()

    ! ##########################
    
    ! ** Intensity without the energy multiplier ** 
    isum_w(:) = 0.0d0
    do itemp=1,rt%Ntemp
       intensity(:) = 0.0d0
       absorption(:)%population = 0.0d0
       call intensity_dynamic%initialize(initial_size=total_size, increment = 1000)
       call energy_dynamic%initialize(initial_size=total_size, increment = 1000)
       do idxcount=1,total_size
          absorption(idxcount)%population = bosefun(gs_energies(from(idxcount)),mu_gs(itemp),rt%Temp(itemp))
          intensity_current = absorption(idxcount)%population*sum(absorption(idxcount)%dipole(:)**2)
          if (intensity_current>rt%tiny) then
             call intensity_dynamic%add(intensity_current, from(idxcount), to(idxcount))
             call energy_dynamic%add(absorption(idxcount)%E,from(idxcount), to(idxcount))
          end if
       end do
       call intensity_dynamic%finalize()
       call energy_dynamic%finalize()
       write(fname_f,"(A2,I5.5)") "/I",itemp
       call writeHDF5dataset1D(data=intensity_dynamic%data,&
            size_in=intensity_dynamic%size,location=fname_f,filename="absorption.h5")
       write(fname_f,"(A2,I5.5)") "/E",itemp
       call writeHDF5dataset1D(data=energy_dynamic%data,&
            size_in=energy_dynamic%size,location=fname_f,filename="absorption.h5")
       isum_w(itemp) = sum(intensity_dynamic%data)
       write(fname_f,"(A7,I5.5)") "/from_I",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idx,&
            size_in=intensity_dynamic%size,location=fname_f,filename="absorption.h5")
       write(fname_f,"(A5,I5.5)") "/to_I",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idy,&
            size_in=intensity_dynamic%size,location=fname_f,filename="absorption.h5")
       call intensity_dynamic%clear()
       call energy_dynamic%clear()
    end do
    call writeHDF5dataset1D(data=isum_w,&
         size_in=rt%Ntemp,location="/ISUM",filename="absorption.h5")
    
  end subroutine compute_absorption

  subroutine compute_emission()
    real(kind=dp), dimension(gs_basis_set_size) :: gs_energies
    real(kind=dp), dimension(:), allocatable :: intensity
    real(kind=dp) :: intensity_current
    type(dynamic_array_type) :: intensity_dynamic, energy_dynamic
    real(kind=dp), dimension(rt%Ntemp) :: mu_es, isum_w
    real(kind=dp) :: time_start, time_stop, dipole_sum
    type(transition_type), dimension(:), allocatable :: emission
    integer :: i, j, k, im, ip, itercount, igs, iems, itemp, ierr, idxcount, total_size
    integer, allocatable, dimension(:) :: from, to
    real(kind=dp), dimension(:), allocatable :: overlap_current
    logical :: file_new
    character(*), parameter :: fname_abs_output="absorption.h5"
    character(*), parameter :: fname_ems_output="emission.h5"
    character(20) :: fname_f
    character(7) :: dsetname

    ! # First, we need to compute energies of the final states
    gs_energies(:) = 0.0d00 ! Global ground state is the first item
    do i=2,gs_basis_set_size
       do ip=1,gs_basis(i)%np
          do im=1,rt%Nmodes
             ! Since all monomers have the same set of modes, it's fine to sum over vib(im)
             gs_energies(i) = gs_energies(i) + gs_basis(i)%particle(ip)%vib(im)*rt%modes(im)
          end do
       end do
    end do
    write(*,"(A,I4)") " * Computing GS energies for basis set size = ", gs_basis_set_size
    ! do i=1,gs_basis_set_size
    !    write(*,"(A,I4,A,F16.8)") " [GS Energy] ",i, " = ", gs_energies(i)
    ! end do

    ! # Second, we compute chemical potentials for different temperatures
    write (*,"(A)") " -------------------------------------------------- "
    write (*,"(A)") "       One-exciton state chemical potentials"
    write (*,"(A)") " -------------------------------------------------- "
    do i=1,rt%Ntemp
       mu_es(i) = compute_chemical_potential(energies=eigenvalues,T=rt%Temp(i))
       write(*,"(A,F8.3,A,F20.10)") "  T = ",rt%Temp(i), "  :  ", mu_es(i)
    end do
    write (*,"(A)") " -------------------------------------------------- "

    total_size = gs_basis_set_size*rt%number_of_eig
    allocate(from(total_size),stat=ierr); call chkmerr(ierr,info="ems:from")
    allocate(to(total_size),stat=ierr); call chkmerr(ierr,info="ems:to")
    allocate(intensity(total_size),stat=ierr); call chkmerr(ierr,info="ems:intensity")
    allocate(overlap_current(basis_set_size),stat=ierr); call chkmerr(ierr,info="abs:overlap_current")

    ! # Third, we compute emission spectrum and save it somehow. I need a linked list, probalby.

    if (allocated(emission)) deallocate(emission)
    allocate(emission(total_size),stat=ierr); call chkmerr(ierr,info="ems:emission")
    itercount = 1
    time_start = omp_get_wtime() !call cpu_time(time_start)
    idxcount = 0
    intensity(:) = 0.0d0
    from(:) = 0
    to(:)   = 0
    
    do iems=1,rt%number_of_eig
       do igs=1,gs_basis_set_size
          idxcount = idxcount + 1
          emission(idxcount)%dipole(:) = 0.0d00
          from(idxcount) = iems 
          to(idxcount)   = igs
          !$OMP PARALLEL DO PRIVATE(k)
          do k=1,basis_set_size
             overlap_current(k) = overlap_ge(igs,k)
             if (abs(overlap_current(k))<1.0d-10) then
                overlap_current(k)=0.0d0
             end if
          end do
          !$OMP END PARALLEL DO

          
          do i=1,3
             dipole_sum = 0.0d0
             !$OMP PARALLEL DO REDUCTION(+:dipole_sum) PRIVATE(k)
             do k=1,basis_set_size
                dipole_sum = dipole_sum  + &
                     rt%dipole(i,basis(k)%particle(1)%idx) &
                     * eigenvectors(k,iems) * overlap_current(k)
             end do
             !$OMP END PARALLEL DO
             emission(idxcount)%dipole(i)=dipole_sum
          end do
          
          emission(idxcount)%E = eigenvalues(iems)-gs_energies(igs)
       end do
       if (((1.0*iems)/rt%number_of_eig)>=0.1*itercount) then
          itercount = itercount + 1
          time_stop = omp_get_wtime() !call cpu_time(time_stop)

          write(*,"(A,F6.2,A,A,F16.4,A)") " Done: ", (100.0*iems)/rt%number_of_eig, "%", "   time  =  ", &
               time_stop-time_start, " s "
          time_start = omp_get_wtime() !call cpu_time(time_start)
       end if
    end do

    if (rt%full_output)then
       call writeHDF5dataset1D_new(data=ha_to_rcm(emission(:)%E),&
            size_in=total_size,location="/E",filename="emission.h5")

       call writeHDF5dataset1D_int(data=from,&
            size_in=total_size,location="/from",filename="emission.h5")

       call writeHDF5dataset1D_int(data=to,&
            size_in=total_size,location="/to",filename="emission.h5")

       call writeHDF5dataset1D(data=emission(:)%dipole(1),&
            size_in=total_size,location="/dx",filename="emission.h5")
       call writeHDF5dataset1D(data=emission(:)%dipole(2),&
            size_in=total_size,location="/dy",filename="emission.h5")
       call writeHDF5dataset1D(data=emission(:)%dipole(3),&
            size_in=total_size,location="/dz",filename="emission.h5")
    end if
    
    isum_w(:) = 0.0d0
    file_new = .TRUE.
    do itemp=1,rt%Ntemp
       intensity(:) = 0.0d0
       emission(:)%population = 0.0d0
       call intensity_dynamic%initialize(initial_size=total_size, increment = 1000)
       call energy_dynamic%initialize(initial_size=total_size, increment = 1000)
       do idxcount=1,total_size
          emission(idxcount)%population = bosefun(eigenvalues(from(idxcount)),mu_es(itemp),rt%Temp(itemp))
          intensity_current = emission(idxcount)%E**3*emission(idxcount)%population*sum(emission(idxcount)%dipole(:)**2)
          if (intensity_current>rt%tiny) then
             call intensity_dynamic%add(intensity_current, from(idxcount), to(idxcount))
             call energy_dynamic%add(emission(idxcount)%E, from(idxcount), to(idxcount))
          end if
       end do
       call intensity_dynamic%finalize()
       call energy_dynamic%finalize()
       write(fname_f,"(A4,I5.5)") "/I_w",itemp
       if (.not.(rt%full_output).and.(file_new)) then
          call writeHDF5dataset1D_new(data=intensity_dynamic%data,&
               size_in=intensity_dynamic%size,location=fname_f,filename="emission.h5")
          file_new = .FALSE.
       else
          call writeHDF5dataset1D(data=intensity_dynamic%data,&
               size_in=intensity_dynamic%size,location=fname_f,filename="emission.h5")
       end if
       write(fname_f,"(A4,I5.5)") "/E_w",itemp
       call writeHDF5dataset1D(data=energy_dynamic%data,&
            size_in=energy_dynamic%size,location=fname_f,filename="emission.h5")
       isum_w(itemp) = sum(intensity_dynamic%data)
       write(fname_f,"(A9,I5.5)") "/from_I_w",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idx,&
            size_in=intensity_dynamic%size,location=fname_f,filename="emission.h5")
       write(fname_f,"(A7,I5.5)") "/to_I_w",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idy,&
            size_in=intensity_dynamic%size,location=fname_f,filename="emission.h5")
       call intensity_dynamic%clear()
       call energy_dynamic%clear()
    end do
    call writeHDF5dataset1D(data=isum_w,&
         size_in=rt%Ntemp,location="/ISUM_w",filename="emission.h5")
    ! ### Dipole moment norm ###
    call intensity_dynamic%initialize(initial_size=total_size, increment = 1000)
    call energy_dynamic%initialize(initial_size=total_size, increment = 1000)
    do idxcount=1,total_size
       intensity_current = sum(emission(idxcount)%dipole(:)**2)
       if ((intensity_current>rt%tiny).or.(rt%full_output)) then
          call intensity_dynamic%add(intensity_current, from(idxcount), to(idxcount))
          call energy_dynamic%add(emission(idxcount)%E, from(idxcount), to(idxcount))
       end if
    end do
    call intensity_dynamic%finalize()
    call energy_dynamic%finalize()
    call writeHDF5dataset1D(data=intensity_dynamic%data,&
         size_in=intensity_dynamic%size,location="/dnorm",filename="emission.h5")
    call writeHDF5dataset1D(data=energy_dynamic%data,&
         size_in=energy_dynamic%size,location="/E_dnorm",filename="emission.h5")
    call writeHDF5dataset1D_int(data=intensity_dynamic%idx,&
            size_in=intensity_dynamic%size,location="/from_dnorm",filename="emission.h5")
    call writeHDF5dataset1D_int(data=intensity_dynamic%idy,&
         size_in=intensity_dynamic%size,location="/to_dnorm",filename="emission.h5")
    call intensity_dynamic%clear()
    call energy_dynamic%clear()
    ! ##########################
    
    ! ** Intensity without the energy multiplier ** 
    isum_w(:) = 0.0d0
    do itemp=1,rt%Ntemp
       intensity(:) = 0.0d0
       emission(:)%population = 0.0d0
       call intensity_dynamic%initialize(initial_size=total_size, increment = 1000)
       call energy_dynamic%initialize(initial_size=total_size, increment = 1000)
       do idxcount=1,total_size
          emission(idxcount)%population = bosefun(eigenvalues(from(idxcount)),mu_es(itemp),rt%Temp(itemp))
          intensity_current = emission(idxcount)%population*sum(emission(idxcount)%dipole(:)**2)
          if (intensity_current>rt%tiny) then
             call intensity_dynamic%add(intensity_current, from(idxcount), to(idxcount))
             call energy_dynamic%add(emission(idxcount)%E,from(idxcount), to(idxcount))
          end if
       end do
       call intensity_dynamic%finalize()
       call energy_dynamic%finalize()
       write(fname_f,"(A2,I5.5)") "/I",itemp
       call writeHDF5dataset1D(data=intensity_dynamic%data,&
            size_in=intensity_dynamic%size,location=fname_f,filename="emission.h5")
       write(fname_f,"(A2,I5.5)") "/E",itemp
       call writeHDF5dataset1D(data=energy_dynamic%data,&
            size_in=energy_dynamic%size,location=fname_f,filename="emission.h5")
       isum_w(itemp) = sum(intensity_dynamic%data)
       write(fname_f,"(A7,I5.5)") "/from_I",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idx,&
            size_in=intensity_dynamic%size,location=fname_f,filename="emission.h5")
       write(fname_f,"(A5,I5.5)") "/to_I",itemp
       call writeHDF5dataset1D_int(data=intensity_dynamic%idy,&
            size_in=intensity_dynamic%size,location=fname_f,filename="emission.h5")
       call intensity_dynamic%clear()
       call energy_dynamic%clear()
    end do
    call writeHDF5dataset1D(data=isum_w,&
         size_in=rt%Ntemp,location="/ISUM",filename="emission.h5")

  end subroutine compute_emission

  !! Checked logic of the function at Jun 6, 2016 20:18 
  pure elemental real(kind=dp) function overlap_ge(gidx, eidx)
    integer, intent(in) :: gidx, eidx
    integer :: im
    integer(kind=k1) :: iA, iB, iC, iP, iQ, iR
    logical :: fine

    overlap_ge = 0.0d00

    ! First case: Global ground state. Overlap .ne. 0 only for 1p exc.st. components.
    if (gidx==1) then
       ! If excited state is 1-p state
       if (basis(eidx)%np==1) then
          overlap_ge = 1.0d00
          do im=1,rt%Nmodes
             overlap_ge = overlap_ge*&
                  fcOverlapIntegral(0_k1,basis(eidx)%particle(1)%vib(im),rt%HRs(im))
          end do
       else
          overlap_ge = 0.0d00
       end if
    else
       ! Second case: both ground and excited states are 1-p states.
       if ( (basis(eidx)%np==1).and.(gs_basis(gidx)%np==1) ) then
          ! If exc state and ground state are on the same monomer
          if ( (basis(eidx)%particle(1)%idx==gs_basis(gidx)%particle(1)%idx) ) then
             overlap_ge = 1.0d00
             do im=1,rt%Nmodes
                overlap_ge = overlap_ge*&
                     fcOverlapIntegral(gs_basis(gidx)%particle(1)%vib(im),&
                     basis(eidx)%particle(1)%vib(im),rt%HRs(im))
             end do
          end if
          ! Third case: excited state is 2-p, ground state is 1-p. 
       else if  ( (basis(eidx)%np==2).and.(gs_basis(gidx)%np==1) ) then
          ! Overlap is non-zero if exc state is on a different monomer, and gs excitations are equal
          if ( (basis(eidx)%particle(1)%idx.ne.gs_basis(gidx)%particle(1)%idx) ) then
             if ( (basis(eidx)%particle(2)%idx.eq.gs_basis(gidx)%particle(1)%idx) .and. &
                  all(basis(eidx)%particle(2)%vib(:).eq.gs_basis(gidx)%particle(1)%vib(:)) ) then
                overlap_ge = 1.0d00
                do im=1,rt%Nmodes
                   overlap_ge = overlap_ge*&
                        fcOverlapIntegral(0_k1,&
                        basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                end do
             end if
          end if
          ! Fourth case: both gs and es are 2-p states.
       else if  ( (basis(eidx)%np==2).and.(gs_basis(gidx)%np==2) ) then
          ! If gs and es are on the same monomer, and 2nd particle components are equal
          if ( (basis(eidx)%particle(1)%idx.eq.gs_basis(gidx)%particle(1)%idx) ) then
             if ( (basis(eidx)%particle(2)%idx.eq.gs_basis(gidx)%particle(2)%idx)  .and. &
                  all(basis(eidx)%particle(2)%vib(:).eq.gs_basis(gidx)%particle(2)%vib(:)) ) then
                overlap_ge = 1.0d00
                do im=1,rt%Nmodes
                   overlap_ge = overlap_ge*&
                        fcOverlapIntegral(gs_basis(gidx)%particle(1)%vib(im),&
                        basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                end do
             end if
             ! The same condition, but indicies are swapped
          else if ( (basis(eidx)%particle(1)%idx.eq.gs_basis(gidx)%particle(2)%idx) ) then
             if ( (basis(eidx)%particle(2)%idx.eq.gs_basis(gidx)%particle(1)%idx)  .and. &
                  all(basis(eidx)%particle(2)%vib(:).eq.gs_basis(gidx)%particle(1)%vib(:)) ) then
                do im=1,rt%Nmodes
                   overlap_ge = overlap_ge*&
                        fcOverlapIntegral(gs_basis(gidx)%particle(2)%vib(im),&
                        basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                end do
             end if
          end if
          ! ### 3p-3p overlap ###
       else if ( (basis(eidx)%np==3).and.(gs_basis(gidx)%np==3) ) then
          ! ### 6 cases for possible contractions ###
          iA = basis(eidx)%particle(3)%idx
          iB = basis(eidx)%particle(2)%idx
          iC = basis(eidx)%particle(1)%idx
          iP = gs_basis(gidx)%particle(3)%idx
          iQ = gs_basis(gidx)%particle(2)%idx
          iR = gs_basis(gidx)%particle(1)%idx
          ! # iC == iP case
          if (iC==iP) then
             if ((iA==iR).and.(iB==iQ)) then
                if ( (all(basis(eidx)%particle(3)%vib==gs_basis(gidx)%particle(1)%vib))&
                     .and.&
                     (all(basis(eidx)%particle(2)%vib==gs_basis(gidx)%particle(2)%vib))) then
                   overlap_ge = 1.0d00
                   do im=1,rt%Nmodes
                      overlap_ge = overlap_ge*&
                           fcOverlapIntegral(gs_basis(gidx)%particle(3)%vib(im),&
                           basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                   end do
                end if
             else if ((iA==iQ).and.(iB==iR)) then
                if ( (all(basis(eidx)%particle(3)%vib==gs_basis(gidx)%particle(2)%vib))&
                     .and.&
                     (all(basis(eidx)%particle(2)%vib==gs_basis(gidx)%particle(1)%vib))) then
                   overlap_ge = 1.0d00
                   do im=1,rt%Nmodes
                      overlap_ge = overlap_ge*&
                           fcOverlapIntegral(gs_basis(gidx)%particle(3)%vib(im),&
                           basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                   end do
                end if
             end if

          else if (iC==iR) then
             if ((iA==iQ).and.(iB==iP)) then
                if ( (all(basis(eidx)%particle(3)%vib==gs_basis(gidx)%particle(2)%vib))&
                     .and.&
                     (all(basis(eidx)%particle(2)%vib==gs_basis(gidx)%particle(3)%vib))) then
                   overlap_ge = 1.0d00
                   do im=1,rt%Nmodes
                      overlap_ge = overlap_ge*&
                           fcOverlapIntegral(gs_basis(gidx)%particle(1)%vib(im),&
                           basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                   end do
                end if
             else if ((iA==iP).and.(iB==iQ)) then
                if ( (all(basis(eidx)%particle(3)%vib==gs_basis(gidx)%particle(3)%vib))&
                     .and.&
                     (all(basis(eidx)%particle(2)%vib==gs_basis(gidx)%particle(2)%vib))) then
                   overlap_ge = 1.0d00
                   do im=1,rt%Nmodes
                      overlap_ge = overlap_ge*&
                           fcOverlapIntegral(gs_basis(gidx)%particle(1)%vib(im),&
                           basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                   end do
                end if
             end if
             
          else if (iC==iQ) then
             if ((iA==iR).and.(iB==iP)) then
                if ( (all(basis(eidx)%particle(3)%vib==gs_basis(gidx)%particle(1)%vib))&
                     .and.&
                     (all(basis(eidx)%particle(2)%vib==gs_basis(gidx)%particle(3)%vib))) then
                   overlap_ge = 1.0d00
                   do im=1,rt%Nmodes
                      overlap_ge = overlap_ge*&
                           fcOverlapIntegral(gs_basis(gidx)%particle(2)%vib(im),&
                           basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                   end do
                end if
             else if ((iA==iP).and.(iB==iR)) then
                if ( (all(basis(eidx)%particle(3)%vib==gs_basis(gidx)%particle(3)%vib))&
                     .and.&
                     (all(basis(eidx)%particle(2)%vib==gs_basis(gidx)%particle(1)%vib))) then
                   overlap_ge = 1.0d00
                   do im=1,rt%Nmodes
                      overlap_ge = overlap_ge*&
                           fcOverlapIntegral(gs_basis(gidx)%particle(2)%vib(im),&
                           basis(eidx)%particle(1)%vib(im),rt%HRs(im))
                   end do
                end if
             end if
          end if
       end if
    end if

  end function overlap_ge

  real(kind=dp) function compute_chemical_potential(energies,T)
    real(kind=dp), intent(in) :: energies(:)
    real(kind=dp), intent(in) :: T
    real(kind=dp) :: sum1, sum2, mu, dmu, dsum, eps, grad
    integer :: i, j
    compute_chemical_potential = chemicalPotentialBose(energies,T)
  end function compute_chemical_potential

  real(kind=dp) function chemicalPotentialBose(w,T)
    use blas95
    use f95_precision
    use lapack95
    implicit none 
    integer :: dim
    real(kind=dp), intent(in) :: w(:)
    real(kind=dp), intent(in) :: T
    real(kind=dp) :: mu1, mu0, ec, ep, mua,mub,mux, ea, eb, ex, tol, btol
    real(kind=dp) :: boseval
    integer :: counter, sgn, i
    logical :: badrun

    dim = size(w)
    ! Prepare initial guess
    !  print *, " * * Chemical potential calculations * * "
    badrun = .FALSE.
    if (T<1.0) then
       btol = 1.0d-10
    elseif ((T>=1.0).and.(T<10.0)) then
       btol = 1.0d-11
    elseif ((T>=10.0).and.(T<30.0)) then
       btol = 1.0d-12
    else
       tol = 1.0d-12
       btol = tol
    end if

    if (w(iamin(w))==0.0d0) then
       mu0=w(iamin(w))-0.05
    else
       mu0 = w(iamin(w))*0.95
    end if
    !  print *, "Energies are: ", w(:)
    !  print *, "Min: ", w(iamin(w))

    !  print *, " * Using bisection solver"
    !     mu0 = w(iamin(w))*0.999!0.99997975
    if (w(iamin(w))==0.0d0) then
       mua=w(iamin(w))-0.05
       mub=w(iamin(w))
    else
       mua=w(iamin(w))*0.99
       mub=w(iamin(w))
    end if

    !  10 print *, mua, mub
10  boseval = sum( [( bosefun(w(i),mua,T), i=1,dim)] )
    ea = boseval-1
    !  write(*,"(A,F12.8)") "Error A = ",ea
    boseval = sum( [( bosefun(w(i),mub,T), i=1,dim)] )
    eb = boseval-1
    !  write(*,"(A,F12.8)") "Error B = ",eb

    if ((ea>0).or.(eb<0)) then
       print *, " * * * bisection method failed, ea>0 or eb<0 * * * "
       if (T>100) then
          if (badrun) then
             stop
          end if
          badrun=.TRUE.
          print *, " * trying to start with smaller mua"
          mua=w(iamin(w))-0.1
          goto 10
       end if

       print *, " Exiting "
       stop
    end if

    boseval = sum( [( bosefun(w(i),mua,T), i=1,dim)] )
    ep = boseval-1
    ! write(*,"(A,F12.8)") "Error A = ",ep
    boseval = sum( [( bosefun(w(i),mub,T), i=1,dim)] )
    ep = boseval-1
    ! write(*,"(A,F12.8)") "Error B = ",ep
    do
       mux = (mua+mub)/2.0
       boseval = sum( [( bosefun(w(i),mux,T), i=1,dim)] )
       ep = boseval-1
       !     write(*,"(A,F12.8)") "Error X = ",ep
       if (abs(ep)<btol) then
          chemicalPotentialBose = mux
          !        write(*,"(A,F12.8)") "Error X = ",ep
          return
       end if
       if (ep>0) then
          mub = mux
       else
          mua = mux
       end if
    end do



    ! print *, " * Using line search "
    ! write(*,"(A,F12.8)") "Starting at mu0 = ",mu0

    ! boseval = sum( [( bosefun(w(i),mu0,T), i=1,dim)] )
    ! ep = boseval-1
    ! write(*,"(A,F12.8)") "Error = ",ep
    ! sgn=+1
    ! counter=0
    ! !stop

    ! do 
    !    mu1 = mu0 + sgn*(w(iamin(w))-mu0)/2.0
    !    boseval = sum([(bosefun(w(i),mu1,T), i=1,dim)])
    !    ec = boseval - 1.0
    !    if (abs(ec)>abs(ep)) then
    !       sgn = -1.0*sgn
    !    end if


    !    mu0 = mu1
    !    counter = counter+1
    !    write(*,"(A,F14.10)") "  * ", ec
    !    ep=ec
    !    if (T<20) then
    !       if (abs(ec)<1.0e-1) then
    !          chemicalPotentialBose = mu1
    !          return

    !       end if
    !    else 
    !       if (abs(ec)<0.5) then
    !          exit
    !       end if
    !    end if

    ! end do

    ! Start Newton-Raphson iterations
    counter = 0
    ep=1
    mu0 = mux
    print *, " * NR steps * "
    do
       mu1 = mu0 - (sum([(bosefun(w(i),mu0,T), i=1,dim)])-1)/sum([(dbosefun(w(i),mu0,T), i=1,dim)])
       ec = sum([(bosefun(w(i),mu0,T), i=1,dim)])-1
       write(*,"(A,F16.13)") "Error = ", ec
       if (abs(ec)<tol.or.abs(ec-ep)<1.0d-20) then
          exit
       end if
       mu0=mu1
       if (isnan(mu1)) then
          write(*,*) "  * * * Not a number detected while computing the chemical potential. Stopping the program! * * *"
          stop
       end if
       ep=ec
    end do
    chemicalPotentialBose = mu1
    print *, " * NR converged * "
    print *, " * * done with chemical potential calculation * * "
  end function chemicalPotentialBose


  elemental pure integer function sgn(x)
    real(kind=dp), intent(in) :: x
    if (x>=0.0d0) then
       sgn = +1
    else
       sgn = -1
    end if
  end function sgn

  elemental real(kind=dp) function bosefun(E,mu,T)
    real(kind=dp), intent(in) :: E
    real(kind=dp), intent(in) :: mu, T ! T in K, mu and E in Hartree
    bosefun = 1.0/(exp((E-mu)/(kB*T))-1.0d0)
  end function bosefun

  elemental real(kind=dp) function boltzmannfun(E,T)
    real(kind=dp), intent(in) :: E
    real(kind=dp), intent(in) :: T ! T in K, mu and E in Hartree
    boltzmannfun = exp(-(E)/(kB*T))
  end function boltzmannfun

  real(kind=dp) function dbosefun(E,mu,T)
    real(kind=dp), intent(in) :: E, mu, T ! T in K, mu and E in Hartree
    dbosefun = (exp((E-mu)/(kB*T)))/(kB*T*(exp((E-mu)/(kB*T))-1.0d0)**2)
  end function dbosefun


end module absems
