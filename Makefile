#BUILD_TYPE = "OPT"
BUILD_TYPE = "DEBUG"
ifeq ($(BUILD_TYPE),"OPT")
PETSC=/opt/petsc_3.7.3_production
SLEPC=/opt/slepc_3.7.2-production
else
PETSC=/opt/petsc_3.7.3
SLEPC=/opt/slepc_3.7.2
endif

#PETSC=/opt/petsc_3.7.3/
#SLEPC=/opt/slepc_3.7.2/

HDF5=/opt/hdf5
MPI=/opt/openmpi_intel
MKL=/opt/intel/mkl
LIBRARIES=/opt/sundials/lib:${MPI}/lib:${HDF5}/lib:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib
INCLUDE=/opt/sundials/include:${MPI}/include:${HDF5}/include:/opt/slepc/include:/opt/petsc/include
SUNDIALSLIBS = -lsundials_fkinsol -lsundials_kinsol -lsundials_fcvode -lsundials_fnvecserial -lsundials_cvode -lsundials_nvecserial
#MODULES=main.f90 functions.f90 linalg.f90 printing.f90 testing.f90 subset.f90
SOURCES= utils.f90 types.f90 subset.f90 basis_set.f90 init.f90 io.f90 rtlib.f90 absems.f90 eigensolver.f90 hamiltonian.f90 testing.f90 ./eigsolver/eigensolver_slepc.f90 
#CSOURCES=slepc_eigsolver.c io_hdf.c
OBJECTS=$(SOURCES:.f90=.o)
EXECUTABLE=main.x
EXEC_SOURCE=main.f90
EXEC_OBJ=$(EXEC_SOURCE:.f90=.o)

### + ilp64
#FCFLAGS = -L${PETSC}/lib -L${SLEPC}/lib -L${MPI}/lib -lpetsc -lslepc -lmpi_usempif08 -lmpi_usempi_ignore_tkr -lmpi_mpifh -lmpi -I$(MKL)/include -I$(MKL)/include/intel64/ilp64 -I${MPI}/include -L${MPI}/lib -I${HDF5}/include -L${HDF5}/lib -lhdf5_fortran -lhdf5hl_fortran -I${PETSC}/include -I${SLEPC}/include -I${MPI}/include -L/opt/intel/lib/ -L/opt/intel/mkl/lib/intel64 -L. -lpthread -lmkl_blas95_ilp64 -lmkl_lapack95_ilp64 -lmkl_intel_ilp64 -lmkl_core -lmkl_rt -lmkl_intel_thread -L/opt/sundials/lib -I/opt/sundials/include ${SUNDIALSLIBS} -Wl,-rpath,${HDF5}/lib:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib:${MPI}/lib:${HDF5}/lib:${HDF5}/include:/opt/sundials/lib:/opt/sundials/include:${PETSC}/lib:${SLEPC}/lib

FCFLAGS = -L${PETSC}/lib -L${SLEPC}/lib -L${MPI}/lib -I${PETSC}/include -I${SLEPC}/include -I${MPI}/include -I${HDF5}/include -L${HDF5}/lib  -lpetsc -lslepc -lmpi_usempif08 -lhdf5_fortran -lhdf5hl_fortran -lmkl_intel_thread -lmkl_blas95_ilp64 -lmkl_lapack95_ilp64 -lmkl_intel_ilp64 -Wl,-rpath,${HDF5}/lib:${MPI}/lib:${HDF5}/lib:${HDF5}/include:${PETSC}/lib:${SLEPC}/lib:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib

#FCFLAGS = -L${MPI}/lib -I${PETSC}/include  -I${MPI}/include -I${HDF5}/include -L${HDF5}/lib -lmpi_usempif08 -lhdf5_fortran -lhdf5hl_fortran libarpack_LINUX64.a -lmkl_sequential -lmkl_rt -lmkl_blas95_lp64 -Wl,-rpath,${HDF5}/lib:${MPI}/lib:${HDF5}/lib:${HDF5}/include:/opt/intel/mkl/lib/intel64:/opt/intel/mkl/lib


FCDEBUG = ${MPI}/bin/mpifort -fpp -mkl -heap-arrays -traceback -fp-stack-check  -O0 -g -check noarg_temp_created -check all -fp-model strict
FCOPT = ${MPI}/bin/mpifort -openmp -fpp -mkl -traceback -heap-arrays -O3 -ipo -qopt-report= -xHost -fp-model strict
FCPLAIN = ${MPI}/bin/mpifort -fpp -mkl -O0 -g -check  noarg_temp_created -check all

ifeq ($(BUILD_TYPE),"OPT")
FC = ${FCOPT}
else
FC = ${FCDEBUG}
endif

#FC = ${FCDEBUG}
#FC = ${FCPLAIN}
#FC = ${FCOPTTRBCK}

all: $(SOURCES) ${EXEC_SOURCE} ${OBJECTS} ${EXEC_OBJ} $(EXECUTABLE)

%.o: %.f90
	${FC} -c ${SOURCES} ${EXEC_SOURCE} ${SOURCES_ANALYSIS} ${FCFLAGS}
	cp eigensolver_slepc.o eigsolver/

$(EXECUTABLE): ${OBJECTS} ${EXEC_OBJ}
	${FC} ${OBJECTS} ${EXEC_OBJ} $(FCFLAGS) -o $@

lorentzian:
	f2py -c lorentzian_profile.f90 -m lorentzian_profile
tags:
	etags `find . -name "*.f90"`
clean:
	rm *.x *.o *.mod
