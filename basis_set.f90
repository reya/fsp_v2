module basis_set
  use types
  use utils
  use subset
  use rtlib
  
  
  implicit none 
  type(basis_function_type), protected, allocatable, dimension(:) :: basis, gs_basis
  logical, protected :: isInitialized=.FALSE., gs_isInitialized=.FALSE.
  integer, protected :: basis_set_size, gs_basis_set_size
  public :: basis, basis_set_size
  public :: initialize_basis_set
  public :: initialize_gs_basis
  public :: gs_basis, gs_basis_set_size
  public :: check_basis_completeness
  private
contains

  subroutine initialize_basis_set(basis_size,compute, print_output)
    integer, intent(inout) :: basis_size
    logical, intent(in)    :: compute
    integer :: i, j, k, depth, bcount
    ! Dirty shit comes below.
    integer :: i1, i2, i3, i4, i5, npcount, iexc, ierr
    integer, dimension(rt%Nmodes) :: vibexc1, vibexc2, vibexc3, vibexc4, vibexc5
    ! End of dirty shit for now.
    integer, allocatable, dimension(:) :: nparticles, gs_particles
    integer, allocatable, dimension(:) :: idx, pos, pos_gs, idx_gs
    logical, allocatable, dimension(:) :: more
    logical, optional, intent(in)      :: print_output
    logical :: print_output_in
    logical :: gs_fine
    vibexc1(:) = 0; vibexc2(:) = 0; vibexc3(:) = 0; vibexc4(:) = 0; vibexc5(:) = 0
    if (present(print_output)) then
       print_output_in = print_output
    else
       if (compute) then
          print_output_in = .FALSE.
       else
          print_output_in = .TRUE.
       end if
    end if
    
    write(*,"(A,I2,A)") " Pre-computing size of the ", rt%Nparticle," basis set"
    !# First: run the Nparticle loop for (100), (010), etc.
    allocate(nparticles(rt%Nparticle))
    allocate(more(rt%Nparticle))
    if (rt%Nparticle>1) then
       allocate(gs_particles(rt%Nmers))
       allocate(pos_gs(rt%Nmers))
    end if
    
    nparticles(:) = 0
    if (compute) then
       write(*,*) " Filling components of the basis set: "
       allocate(basis(basis_size))
       basis_set_size = basis_size
       bcount = 0
    else
       basis_size = 0
    end if
    
    do iexc=1,rt%Nmers
       if (print_output_in) then
          write(*,*) "     ================================================= "
          write(*,*) "       One-particle contribution: Excited state on ", iexc
       end if
       ! # Scan Nparticle depth
       ! OK, *dirty shit* comes in.
       ! But we don't need more than 5-paricle basis for sure.


       !write(*,*) "       Excitation on: ", iexc, "  Vibrational exc in GS = 0"
       more(:) = .FALSE.
       do
          call index_next0(rt%Nmodes,maxval(rt%Ne),vibexc1,more(1))
          if (all(vibexc1<=rt%Ne)) then
             if (print_output_in) then
                write(*,*) "    (bs)>  (1) Monomer = ", iexc, " e = ",vibexc1(:)-1
             end if

             if (compute) then
                bcount = bcount + 1
                allocate(basis(bcount)%particle(1),stat=ierr); call chkmerr(ierr)
                basis(bcount)%np = 1
                allocate(basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                basis(bcount)%particle(1)%idx    = iexc
             else
                basis_size = basis_size + 1
             end if
          end if
          if (.not.(more(1))) exit
       end do
       if (print_output_in) then
          write(*,*) " "
       end if
       
       if (rt%Nparticle>1) then
          gs_particles(:) = 0
          do while (sum(gs_particles(:))<rt%Nmers)
             ! (0,0,1,1,0,etc.) where position is the monomer, and 1/0 is the excitation.
             call binary_vector_next(rt%Nmers, gs_particles)
             gs_fine = .TRUE.
             !               write(*,*) " gs_particles = ", gs_particles
             do i=1,rt%Nmers
                if ((gs_particles(i)==1).and.(i==iexc)) then
                   !                      write(*,*) " Exiting the loop with ", gs_particles
                   gs_fine = .FALSE.
                end if
             end do
             if (gs_fine) then
                k=0
                pos_gs(:) = 0
                do i=1,rt%Nmers
                   if (gs_particles(i)==1) then
                      k = k + 1
                      pos_gs(k) = i ! 
                   end if
                end do
                !write(*,*) "     Next: ", gs_particles 
                !write(*,*) "       E: ", iexc, "  G: ", pos_gs(1:k)

                ! ### So, here I put the select block ###
                ! ### To iterate over vibrational exc ###
                npcount = k ! Number of monomers in gs
                more(:) = .FALSE.

                if (npcount<rt%Nparticle) then  ! npcount is only for gs. So it must be less than total Nparticle.
                   select case(npcount)
                   case(1)   ! Two-partcle basis
                      if (print_output_in) then
                         write(*,*) "     ================================================= "
                         write(*,*) "       Two-particle contribution: Excited state on ", iexc
                      end if
                      do
                         call index_next0(rt%Nmodes,maxval(rt%Ne),vibexc1,more(1))
                         if (all(vibexc1<=rt%Ne)) then
                            if (print_output_in) then
                               write(*,*) "    >> (2) Monomer = ", iexc, " e = ",vibexc1(:)-1
                            end if

                            do
                               call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc2,more(2))
                               if (all(vibexc2<=rt%Ng)) then
                                  if (sum(vibexc2-1)>=1) then
                                     if (print_output_in) then
                                        write(*,*) "     (bs)> (2) Monomer = ", pos_gs(1), " g = ",vibexc2(:)-1
                                     end if

                                     if (compute) then
                                        bcount = bcount + 1
                                        allocate(basis(bcount)%particle(2),stat=ierr); call chkmerr(ierr)
                                        basis(bcount)%np = 2
                                        allocate(basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                        allocate(basis(bcount)%particle(2)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                        basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                                        basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
                                        basis(bcount)%particle(1)%idx    = iexc
                                        basis(bcount)%particle(2)%idx    = pos_gs(1)
                                     else
                                        basis_size = basis_size + 1
                                     end if
                                  end if
                               end if
                               if (.not.more(2)) exit
                            end do
                            if (print_output_in) then
                               write(*,*) " "
                            end if
                         end if

                         if (.not.more(1)) exit
                      end do
                   case(2)   ! Three-partcle basis
                      if (print_output_in) then
                         write(*,*) "     ================================================= "
                         write(*,*) "       Three-particle contribution: Excited state on ", iexc
                      end if
                      do
                         call index_next0(rt%Nmodes,maxval(rt%Ne),vibexc1,more(1))
                         if (all(vibexc1<=rt%Ne)) then
                            if (print_output_in) then
                               write(*,*) "    >> (3) Monomer = ", iexc, " e = ",vibexc1(:)-1
                            end if

                            do
                               call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc2,more(2))
                               if (all(vibexc2<=rt%Ng)) then
                                  if (sum(vibexc2-1)>=1) then
                                     if (print_output_in) then
                                        write(*,*) "     (bs)> (3) Monomer = ", pos_gs(1), " g = ",vibexc2(:)-1
                                     end if
                                     do
                                        call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc3,more(3))
                                        if (all(vibexc3<=rt%Ng)) then
                                           if (sum(vibexc3-1)>=1) then
                                              if (print_output_in) then
                                                 write(*,*) "     (bs)> (3) Monomer = ", pos_gs(2), " g = ",vibexc3(:)-1
                                              end if
                                              if (compute) then
                                                 bcount = bcount + 1
                                                 allocate(basis(bcount)%particle(3),stat=ierr); call chkmerr(ierr)
                                                 basis(bcount)%np = 3
                                                 allocate(basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                                 allocate(basis(bcount)%particle(2)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                                 allocate(basis(bcount)%particle(3)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                                 basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                                                 basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
                                                 basis(bcount)%particle(3)%vib(:) = vibexc3(:) - 1
                                                 basis(bcount)%particle(1)%idx    = iexc
                                                 basis(bcount)%particle(2)%idx    = pos_gs(1)
                                                 basis(bcount)%particle(3)%idx    = pos_gs(2)
                                              else
                                                 basis_size = basis_size + 1
                                              end if
                                           end if
                                        end if
                                        if (.not.more(3)) exit
                                     end do
                                  end if
                               end if
                               if (.not.more(2)) exit
                            end do
                            if (print_output_in) then
                               write(*,*) " "
                            end if
                         end if

                         if (.not.more(1)) exit
                      end do
                   case default
                      write(*,*) " [BASIS_SET] Error: Unexpected number of GS particles"
                      write(*,*) "                    npcount = ",npcount

                   end select
                   ! ### END OF SODOMY ###
                   if (print_output_in) then
                      write(*,*) "  "
                   end if

                end if
             end if
          end do
          if (print_output_in) then
             write(*,*) "  * * *  "
          end if
       end if
       if (allocated(pos)) deallocate(pos)
    end do
    if (print_output_in) then
       write(*,*) " FINALLY: size of the basis set is ", basis_size
       
    end if
    if (compute) then
       isInitialized=.TRUE.
       write(*,*) " BASIS SET INITIALIZED"
    end if
  end subroutine initialize_basis_set


  
  subroutine initialize_gs_basis(gs_basis_size,compute, print_output)
    integer, intent(inout) :: gs_basis_size
    logical, intent(in)    :: compute
    integer :: i, j, k, depth, bcount
    ! Dirty shit comes below.
    integer :: i1, i2, i3, i4, i5, npcount, iexc, ierr, idx_count
    integer, dimension(rt%Nmodes) :: vibexc1, vibexc2, vibexc3, vibexc4, vibexc5
    integer, dimension(rt%Nmers)  :: monomers_gs
    ! End of dirty shit for now.
    integer, allocatable, dimension(:) :: nparticles, gs_particles
    integer, allocatable, dimension(:) :: idx, pos, pos_gs, idx_gs
    logical, allocatable, dimension(:) :: more
    logical, optional, intent(in)      :: print_output
    logical :: print_output_in, more_monomers, more_vibexc, more_vibexc2, more_vibexc3
    logical :: gs_fine
    vibexc1(:) = 0; vibexc2(:) = 0; vibexc3(:) = 0; vibexc4(:) = 0; vibexc5(:) = 0
    if (present(print_output)) then
       print_output_in = print_output
    else
       if (compute) then
          print_output_in = .FALSE.
       else
          print_output_in = .TRUE.
       end if
    end if
    
    if (print_output_in) then
       if (compute) then
          write(*,"(A,I2,A)") " Computing elements of the ", rt%Nparticle," ground state basis set"          
       else
          write(*,"(A,I2,A)") " Pre-computing size of the ", rt%Nparticle," ground state basis set"
       end if
       
    end if
    

    
    monomers_gs(:) = 0
    more_monomers = .FALSE.
    bcount = 0
    if (compute) then
       allocate(gs_basis(gs_basis_size),stat=ierr); call chkmerr(ierr, info=" gs_basis ")
    else
       gs_basis_size = 0
    end if
    
    allocate(idx(rt%Nmers))
    idx(:) = 0
    do
       call index_next0(rt%Nmers,2,monomers_gs,more_monomers)
       idx(:)=0
       idx_count = 0
       do iexc=1,rt%Nmers
          if ((monomers_gs(iexc)-1).eq.1) then
             idx_count = idx_count + 1
             idx(idx_count) = iexc
          end if
       end do
       
       if (sum(monomers_gs-1)<=rt%nparticle) then
          if (print_output_in) then
             write(*,"(A,10I2)") " [GS] ", monomers_gs-1
          end if
          
          if (sum(monomers_gs-1).eq.0) then
             ! # Put there a global ground state
             vibexc1(:) = 0
             if (print_output_in) then
                write(*,*) "   Global ground state:  ", vibexc1(:)
             end if
             if (compute) then
                bcount = bcount + 1
                allocate(gs_basis(bcount)%particle(1),stat=ierr); call chkmerr(ierr)
                gs_basis(bcount)%np = 0
                allocate(gs_basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                gs_basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                gs_basis(bcount)%particle(1)%idx    = 0
             else
                gs_basis_size = gs_basis_size + 1
             end if
             
          else if (sum(monomers_gs-1).eq.1) then
             ! # Put there a 1-p ground state
             vibexc1(:) = 0
             more_vibexc = .FALSE.
             do
                call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc1,more_vibexc)
                if (all(vibexc1<=rt%Ng)) then
                   if (sum(vibexc1-1)>=1) then
                      if (print_output_in) then
                         write(*,"(A,I2,A,10I2)") "   [GS:1p] on ", idx(1), " is ",  vibexc1-1
                      end if

                      if (compute) then
                         bcount = bcount + 1
                         allocate(gs_basis(bcount)%particle(1),stat=ierr); call chkmerr(ierr)
                         gs_basis(bcount)%np = 1
                         allocate(gs_basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                         gs_basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                         gs_basis(bcount)%particle(1)%idx    = idx(1)
                      else
                         gs_basis_size = gs_basis_size + 1
                      end if
                   end if
                end if
                if (.not.(more_vibexc)) exit
             end do
          else if (sum(monomers_gs-1).eq.2) then
             ! # Put there a 2-p ground state
             vibexc1(:) = 0
             more_vibexc = .FALSE.
             do
                call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc1,more_vibexc)
                if (all(vibexc1<=rt%Ng)) then
                   if (sum(vibexc1-1)>=1) then
                      vibexc2(:) = 0
                      more_vibexc2 = .FALSE. 
                      do
                         call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc2,more_vibexc2)
                         if (all(vibexc2<=rt%Ng)) then
                            if (sum(vibexc2-1)>=1) then
                               if (print_output_in) then
                                  write(*,"(A,I2,A,10I2)") "   [GS:2p] on ", idx(1), " is ",  vibexc1-1
                                  write(*,"(A,I2,A,10I2)") "           on ", idx(2), " is ", vibexc2-1
                               end if
                               if (compute) then
                                  bcount = bcount + 1
                                  allocate(gs_basis(bcount)%particle(2),stat=ierr); call chkmerr(ierr)
                                  gs_basis(bcount)%np = 2
                                  allocate(gs_basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                  gs_basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                                  gs_basis(bcount)%particle(1)%idx    = idx(1)
                                  allocate(gs_basis(bcount)%particle(2)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                  gs_basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
                                  gs_basis(bcount)%particle(2)%idx    = idx(2)
                               else
                                  gs_basis_size = gs_basis_size + 1
                               end if
                            end if
                         end if
                         if (.not.(more_vibexc2)) exit
                      end do
                   end if
                end if
                if (.not.(more_vibexc)) exit
             end do

          else if (sum(monomers_gs-1).eq.3) then
             ! # Put there a 3-p ground state
             vibexc1(:) = 0
             more_vibexc = .FALSE.
             do
                call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc1,more_vibexc)
                if (all(vibexc1<=rt%Ng)) then
                   if (sum(vibexc1-1)>=1) then
                      vibexc2(:) = 0
                      more_vibexc2 = .FALSE. 
                      do
                         call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc2,more_vibexc2)
                         if (all(vibexc2<=rt%Ng)) then
                            if (sum(vibexc2-1)>=1) then

                               vibexc3(:)=0
                               more_vibexc3 = .FALSE.
                               do
                                  call index_next0(rt%Nmodes,maxval(rt%Ng),vibexc3,more_vibexc3)
                                  if (all(vibexc3<=rt%Ng)) then
                                     if (sum(vibexc3-1)>=1) then
                                        if (print_output_in) then
                                           write(*,"(A,I2,A,10I2)") "   [GS:3p] on ", idx(1), " is ", vibexc1-1
                                           write(*,"(A,I2,A,10I2)") "           on ", idx(2), " is ", vibexc2-1
                                           write(*,"(A,I2,A,10I2)") "           on ", idx(3), " is ", vibexc3-1
                                        end if
                                        if (compute) then
                                           bcount = bcount + 1
                                           allocate(gs_basis(bcount)%particle(3),stat=ierr); call chkmerr(ierr)
                                           gs_basis(bcount)%np = 3
                                           allocate(gs_basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                           gs_basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
                                           gs_basis(bcount)%particle(1)%idx    = idx(1)
                                           allocate(gs_basis(bcount)%particle(2)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                           gs_basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
                                           gs_basis(bcount)%particle(2)%idx    = idx(2)
                                           allocate(gs_basis(bcount)%particle(3)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
                                           gs_basis(bcount)%particle(3)%vib(:) = vibexc3(:) - 1
                                           gs_basis(bcount)%particle(3)%idx    = idx(3)
                                        else
                                           gs_basis_size = gs_basis_size + 1
                                        end if
                                     end if
                                  end if
                                  if (.not.(more_vibexc3)) exit
                               end do
                               
                            end if
                         end if
                         if (.not.(more_vibexc2)) exit
                      end do
                   end if
                end if
                if (.not.(more_vibexc)) exit
             end do

          end if
       end if
       if (.not.(more_monomers)) exit
    end do
    
    !# First: run the Nparticle loop for (100), (010), etc.
    ! allocate(nparticles(rt%Nparticle))
    ! allocate(more(rt%Nparticle))
    ! if (rt%Nparticle>1) then
    !    allocate(gs_particles(rt%Nmers))
    !    allocate(pos_gs(rt%Nmers))
    ! end if
    
    ! nparticles(:) = 0
    ! if (compute) then
    !    write(*,*) " Filling components of the basis set: "
    !    allocate(gs_basis(gs_basis_size))
    !    basis_set_size = gs_basis_size
    !    bcount = 0
    ! else
    !    gs_basis_size = 0
    ! end if
    
    ! do iexc=1,rt%Nmers
    !    if (print_output_in) then
    !       write(*,*) "     ================================================= "
    !       write(*,*) "       One-particle contribution: Excited state on ", iexc
    !    end if
    !    ! # Scan Nparticle depth
    !    ! OK, *dirty shit* comes in.
    !    ! But we don't need more than 5-paricle basis for sure.


    !    !write(*,*) "       Excitation on: ", iexc, "  Vibrational exc in GS = 0"
    !    more(:) = .FALSE.
    !    do
    !       call index_next0(rt%Nmodes,rt%Ne,vibexc1,more(1))
    !       if (print_output_in) then
    !          write(*,*) "    (bs)>  (1) Monomer = ", iexc, " e = ",vibexc1(:)-1
    !       end if
          
    !       if (compute) then
    !          bcount = bcount + 1
    !          allocate(gs_basis(bcount)%particle(1),stat=ierr); call chkmerr(ierr)
    !          gs_basis(bcount)%np = 1
    !          allocate(gs_basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
    !          gs_basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
    !          gs_basis(bcount)%particle(1)%idx    = iexc
    !       else
    !          gs_basis_size = gs_basis_size + 1
    !       end if
          
    !       if (.not.(more(1))) exit
    !    end do
    !    if (print_output_in) then
    !       write(*,*) " "
    !    end if
       
    !    if (rt%Nparticle>1) then
    !       gs_particles(:) = 0
    !       do while (sum(gs_particles(:))<rt%Nmers)
    !          call binary_vector_next(rt%Nmers, gs_particles)
    !          gs_fine = .TRUE.
    !          !               write(*,*) " gs_particles = ", gs_particles
    !          do i=1,rt%Nmers
    !             if ((gs_particles(i)==1).and.(i==iexc)) then
    !                !                      write(*,*) " Exiting the loop with ", gs_particles
    !                gs_fine = .FALSE.
    !             end if
    !          end do
    !          if (gs_fine) then
    !             k=0
    !             pos_gs(:) = 0
    !             do i=1,rt%Nmers
    !                if (gs_particles(i)==1) then
    !                   k = k + 1
    !                   pos_gs(k) = i ! 
    !                end if
    !             end do
    !             !write(*,*) "     Next: ", gs_particles 
    !             !write(*,*) "       E: ", iexc, "  G: ", pos_gs(1:k)

    !             ! ### So, here I put the select block ###
    !             ! ### To iterate over vibrational exc ###
    !             npcount = k ! Number of monomers in gs
    !             more(:) = .FALSE.

    !             if (npcount<rt%Nparticle) then
    !                select case(npcount)
    !                case(1)   ! Two-partcle basis
    !                   if (print_output_in) then
    !                      write(*,*) "     ================================================= "
    !                      write(*,*) "       Two-particle contribution: Excited state on ", iexc
    !                   end if
    !                   do
    !                      call index_next0(rt%Nmodes,rt%Ne,vibexc1,more(1))
    !                      if (print_output_in) then
    !                         write(*,*) "    >> (2) Monomer = ", iexc, " e = ",vibexc1(:)-1
    !                      end if
                         
    !                      do
    !                         call index_next0(rt%Nmodes,rt%Ng,vibexc2,more(2))
    !                         if (sum(vibexc2-1)>=1) then
    !                            if (print_output_in) then
    !                               write(*,*) "     (bs)> (2) Monomer = ", pos_gs(1), " g = ",vibexc2(:)-1
    !                            end if
                               
    !                            if (compute) then
    !                               bcount = bcount + 1
    !                               allocate(gs_basis(bcount)%particle(2),stat=ierr); call chkmerr(ierr)
    !                               gs_basis(bcount)%np = 2
    !                               allocate(gs_basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
    !                               allocate(gs_basis(bcount)%particle(2)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
    !                               gs_basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
    !                               gs_basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
    !                               gs_basis(bcount)%particle(1)%idx    = iexc
    !                               gs_basis(bcount)%particle(2)%idx    = pos_gs(1)
    !                            else
    !                               gs_basis_size = gs_basis_size + 1
    !                            end if
    !                         end if
    !                         if (.not.more(2)) exit
    !                      end do
    !                      if (print_output_in) then
    !                         write(*,*) " "
    !                      end if
                         
    !                      if (.not.more(1)) exit
    !                   end do
    !                case(2)   ! Three-partcle basis
    !                   if (print_output_in) then
    !                      write(*,*) "     ================================================= "
    !                      write(*,*) "       Three-particle contribution: Excited state on ", iexc
    !                   end if
    !                   do
    !                      call index_next0(rt%Nmodes,rt%Ne,vibexc1,more(1))
    !                      if (print_output_in) then
    !                         write(*,*) "    >> (3) Monomer = ", iexc, " e = ",vibexc1(:)-1
    !                      end if
                         
    !                      do
    !                         call index_next0(rt%Nmodes,rt%Ng,vibexc2,more(2))
    !                         if (sum(vibexc2-1)>=1) then
    !                            if (print_output_in) then
    !                               write(*,*) "     > (3) Monomer = ", pos_gs(1), " g = ",vibexc2(:)-1
    !                            end if
    !                            do 
    !                               call index_next0(rt%Nmodes,rt%Ng,vibexc3,more(3))
    !                               if (sum(vibexc3-1)>=1) then
    !                                  if (print_output_in) then
    !                                     write(*,*) "        (bs)> (3) Monomer = ", pos_gs(2), " g = ",vibexc3(:)-1
    !                                  end if
    !                                  if (compute) then
    !                                     bcount = bcount + 1
    !                                     allocate(gs_basis(bcount)%particle(3),stat=ierr); call chkmerr(ierr)
    !                                     gs_basis(bcount)%np = 3
    !                                     allocate(gs_basis(bcount)%particle(1)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
    !                                     allocate(gs_basis(bcount)%particle(2)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
    !                                     allocate(gs_basis(bcount)%particle(3)%vib(rt%Nmodes),stat=ierr); call chkmerr(ierr)
    !                                     gs_basis(bcount)%particle(1)%vib(:) = vibexc1(:) - 1
    !                                     gs_basis(bcount)%particle(2)%vib(:) = vibexc2(:) - 1
    !                                     gs_basis(bcount)%particle(3)%vib(:) = vibexc3(:) - 1
    !                                     gs_basis(bcount)%particle(1)%idx    = iexc
    !                                     gs_basis(bcount)%particle(2)%idx    = pos_gs(1)
    !                                     gs_basis(bcount)%particle(2)%idx    = pos_gs(2)
    !                                  else
    !                                     gs_basis_size = gs_basis_size + 1
    !                                  end if
    !                               end if
    !                               if (.not.more(3)) exit
    !                            end do
    !                         end if
    !                         if (.not.more(2)) exit
    !                      end do
    !                      if (print_output_in) then
    !                         write(*,*) " "
    !                      end if
                         
    !                      if (.not.more(1)) exit
    !                   end do
    !                end select
    !                ! ### END OF SODOMY ###
    !                if (print_output_in) then
    !                   write(*,*) "  "
    !                end if
                   
    !             end if
    !          end if
    !       end do
    !       if (print_output_in) then
    !          write(*,*) "  * * *  "
    !       end if
    !    end if
    !    if (allocated(pos)) deallocate(pos)
    ! end do
    if (print_output_in) then
       write(*,*) " FINALLY: size of the GS basis set is ", gs_basis_size

    end if
    if (compute) then
       gs_isInitialized=.TRUE.
       write(*,*) " GROUND STATE BASIS SET INITIALIZED"
       gs_basis_set_size = gs_basis_size
    end if
  end subroutine initialize_gs_basis

  subroutine check_basis_completeness()
    integer(kind=k1) :: i, j, im, last_i
    real :: fcnorm
    write(*,*) " =============================="
    write(*,*) "  Basis set completeness check "
    write(*,*) " =============================="
    do im=1,rt%Nmodes
       write(*,*) "  " 
       write(*,*) " MODE = ", im, "; energy = ",rt%modes(im)
       last_i = rt%Ng(im)
       do i=1,rt%Ng(im)
          fcnorm = 0.0d00
          
          write(*,*)     "   *  From: ",i-1_k1
          do j=1,rt%Ne(im)
             fcnorm = fcnorm + fcOverlapIntegral(i-1_k1,j-1_k1,rt%HRs(im))**2
             ! write(*,"(A,I2,I2,A,D20.6,D20.6)") "     ", i-1, j-1, " :: ", fcOverlapIntegral(i-1_k1,j-1_k1,rt%HRs(im)), fcOverlapIntegral(i-1_k1,j-1_k1,rt%HRs(im))**2
          end do

          write(*,"(A,F12.8)") "   Prob. norm = ", fcnorm
          write(*,*) " " 
          if (fcnorm<0.95) then
             last_i = i-1_k1-1_k1
          end if
       end do
       write (*,*) " Basis is complete up to ", last_i, " vib. exc. in GS"
    end do
    
       
    write(*,*) " =============================="
  end subroutine check_basis_completeness
  
end module basis_set
